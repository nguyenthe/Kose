package com.jillstuart.beauty.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.fragment.BaseWebViewNoToolBarFragment;

public class TabHomeBasicWebFragment extends BaseWebViewNoToolBarFragment {


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		isBasic = true;
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		String urlString = UrlConstant.URL_TAB_HOME;
		loadUrl(urlString);
	}

}
