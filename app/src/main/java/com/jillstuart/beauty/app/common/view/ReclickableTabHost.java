package com.jillstuart.beauty.app.common.view;

import android.content.Context;
import android.support.v4.app.FragmentTabHost;
import android.util.AttributeSet;

public class ReclickableTabHost extends FragmentTabHost {

	private OnReclickableCurrentTabClickListener onCurrentTabClickListener;

	public ReclickableTabHost(Context context) {
		super(context);
	}

	public ReclickableTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ReclickableTabHost(Context context, OnReclickableCurrentTabClickListener listener) {
		super(context);
		this.onCurrentTabClickListener = listener;
	}

	@Override
	public void setCurrentTab(int index) {
		try {
			if (index == getCurrentTab()) {
				if (this.onCurrentTabClickListener != null) {
					this.onCurrentTabClickListener.OnCurrentTabClick(index);
				}
			} else {
				super.setCurrentTab(index);
			}
		} catch (Exception e) {
		}

	}

	/**
	 * Listener
	 */
	public interface OnReclickableCurrentTabClickListener {
		void OnCurrentTabClick(int index);
	}

	// public OnCurrentTabClickListener getOnCurrentTabClickListener() {
	// return onCurrentTabClickListener;
	// }

	public void setOnReclickableCurrentTabClickListener(
			OnReclickableCurrentTabClickListener onCurrentTabClickListener) {
		this.onCurrentTabClickListener = onCurrentTabClickListener;
	}

}
