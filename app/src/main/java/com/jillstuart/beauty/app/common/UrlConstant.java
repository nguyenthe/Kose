package com.jillstuart.beauty.app.common;

public class UrlConstant {
	
	//判断用、base URL
	public static final String APP_BASE_URL = "http://www.jillstuart-beauty.com";
	
	//ホーム画面Basic idとpassword
//	public static final String APP_BASIC_ID = "jillapp";
//	public static final String APP_BASIC_PW = "jillapp123";
//	public static final String APP_BASIC_BASE_URL = "http://jill3.orangecraft.co.jp";
	
	public static final String APP_BASIC_ID = "kose";
	public static final String APP_BASIC_PW = "Vtp.VB++";
	public static final String APP_BASIC_BASE_URL = "http://www.jillstuart-beauty.com";
	
	
	//タブバー部分のURL
	//ホーム画面Basic
	public static final String URL_TAB_HOME = APP_BASIC_BASE_URL + "/ja-jp/app/home.html";
	public static final String URL_TAB_SHOPLIST = "http://www.jillstuart-beauty.com/ja-jp/shop";
	public static final String URL_TAB_SHOPLIST_CSS = "http://www.jillstuart-beauty.com/ja-jp/app2/app.css";
	public static final String CSS_JS = "var style = document.createElement('style');style.type = 'text/css';var cssContent = document.createTextNode('%1s');style.appendChild(cssContent);document.body.appendChild(style);";
	public static final String URL_TAB_SHOPLIST_COUPON_DEFALUT = URL_TAB_SHOPLIST;
	
	//そのた部分のURL
	public static final String URL_OTHER_JILL = "http://www.jillstuart-beauty.com";
	public static final String URL_OTHER_FACEBOOK = "https://www.facebook.com/jillstuartbeauty";
	public static final String URL_OTHER_FACEBOOK_USER = "170410599746788";
	public static final String URL_OTHER_INSTAGRAM = "http://instagram.com/jillstuartbeauty";
	public static final String URL_OTHER_INSTAGRAM_USER = "jillstuartbeauty";

	//ホワイトリスト
	public static final String URLS_CAN_ACCESS_PASS_WWW = "//www.";
	public static final String URLS_CAN_ACCESS_PASS_HTTP = "https://";
	public static final String URLS_CAN_ACCESS_PASS_BLANK = "about:blank";
	public static final String[] URLS_CAN_ACCESS = { APP_BASE_URL,"https://www.jillstuart.jp"};
	public static final String[] URLS_CAN_ADD_PARA = {APP_BASIC_BASE_URL};
	
	//download
	public static final String URL_DOWNLOAD_COUPON_SHOPS = "http://www.jillstuart-beauty.com/ja-jp/app2/data/store_code.json";
	public static final String URL_DONLAOD_KEY_DATA = "data";
	
	//json
	public static final String URL_DOWNLOAD_VERSION_CHECK_JSON = "https://s3-ap-northeast-1.amazonaws.com/inhouse-app/jill/JillStuartVersionCheckAndroid.json";
	public static final String URL_ANDROID_MARKET= "market://details?id=%1$s";
}
