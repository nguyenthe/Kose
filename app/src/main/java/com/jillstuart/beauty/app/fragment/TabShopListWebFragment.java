package com.jillstuart.beauty.app.fragment;

import android.os.Bundle;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.fragment.BaseWebViewHideFragment;

public class TabShopListWebFragment extends BaseWebViewHideFragment {

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		
		String urlString = null;
		
		if (getArguments() != null) {
			urlString = getArguments().getString(Constant.KEY_FG_URL);
		}
		
		urlString = urlString == null? UrlConstant.URL_TAB_SHOPLIST : urlString;
		
		if (urlString!= null && urlString.startsWith(UrlConstant.APP_BASE_URL) == true) {
			isAddCss = true;
		}else {
			isAddCss = false;
		}
		loadUrl(urlString);
	
	}
}
