package com.jillstuart.beauty.app.common.activity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.jillstuart.beauty.app.application.MainApplication;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.helper.AnimationHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.common.listener.LeoViewListener;
import com.jillstuart.beauty.app.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressWarnings("deprecation")
@SuppressLint("InflateParams")
public abstract class BaseActionBarActivity extends ActionBarActivity implements
		OnBackStackChangedListener {

	protected LayoutInflater mInflater;
	protected ActionBar mActionBar;
	protected View mActionBarCustom;

	private WindowManager mWindowManager;
	private WindowManager.LayoutParams mOverlayProgressLayoutParams;
	private View mOverlayProgress;
	
	private ConnectivityManager mConnMgr;

	public LeoListener backListener;
	
	public BaseActionBarActivity() {
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		detectException();
		
		mInflater = LayoutInflater.from(this);

		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(R.layout.activity_actionbar_title);
		TextView titleTextView = (TextView) mActionBar.getCustomView()
				.findViewById(R.id.action_bar_title);
		Typeface font = Typeface.createFromAsset(getAssets(), Constant.SYS_FONT_CUSTOM_FILE);
		titleTextView.setTypeface(font);
		
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowHomeEnabled(true);
		mActionBar.setDisplayUseLogoEnabled(true);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setIcon(R.drawable.transparent);
		mActionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_action_bar));
		mConnMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		mOverlayProgressLayoutParams = new WindowManager.LayoutParams(
				
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_TOAST,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
				PixelFormat.TRANSPARENT);
		mOverlayProgress = mInflater.inflate(R.layout.overlay_progress, null,
				false);
		
		// ProgressBar bar =(ProgressBar)
		// mOverlayProgress.findViewById(R.id.proccessCircle);
		// bar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.sys_loading_above_color),
		// PorterDuff.Mode.SRC_IN);

		getSupportFragmentManager().addOnBackStackChangedListener(this);

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onPause() {
		super.onPause();

		dismissOverlayProgress();
	}

	public void hideActionBar() {
		mActionBar.hide();
	}

	public void showActionBar() {
		mActionBar.show();
	}

	public boolean isActionBarShowing() {
		return mActionBar.isShowing();
	}
	
	public void setActionBarTitle(String title) {

		View titleView = mActionBar.getCustomView();
		TextView titleTextView = (TextView) titleView
				.findViewById(R.id.action_bar_title);
		ImageView logo = (ImageView) titleView
				.findViewById(R.id.action_bar_logo);
		if (getString(R.string.LBL_TB_HOME).equals(title)) {
			// show logo,hidden text
			titleTextView.setVisibility(View.GONE);
			logo.setVisibility(View.VISIBLE);

		} else {
			titleTextView.setVisibility(View.VISIBLE);
			logo.setVisibility(View.GONE);
			titleTextView.setText(title);
		}

	}

	public CharSequence getActionBarTitle(){
		View titleView = mActionBar.getCustomView();
		TextView titleTextView = (TextView) titleView
				.findViewById(R.id.action_bar_title);
		if (titleTextView.getVisibility() == View.VISIBLE) {
			return titleTextView.getText();
		}
		
		return null;
	}
	
	public boolean isConnectedOrConnecting() {
		NetworkInfo ni = mConnMgr.getActiveNetworkInfo();
		return ni != null && ni.isConnectedOrConnecting();
	}

	public void showErrorNoNetworkConnection() {
		showErrorMessage(null,getString(R.string.MSG_ERR_NET_CONNECT_ERR));
	}

	public void showOverlayProgress() {
		showOverlayProgress(false);
	}

	private void showOverlayProgress(boolean retry) {
		try {
			mWindowManager.addView(mOverlayProgress,
					mOverlayProgressLayoutParams);
			View view = mOverlayProgress.findViewById(R.id.proccessCircle);
			AnimationHelper.startRotateAnimation(view);

		} catch (IllegalStateException e) {
			dismissOverlayProgress();
			mOverlayProgress = mInflater.inflate(R.layout.overlay_progress,
					null, false);
			if (!retry)
				showOverlayProgress(true);
		}
	}

	public void dismissOverlayProgress() {
		try {
			View view = mOverlayProgress.findViewById(R.id.proccessCircle);
			AnimationHelper.stopAnimation(view);
			mWindowManager.removeView(mOverlayProgress);
		} catch (IllegalArgumentException e) {
		}
	}

	public void showErrorMessage(String title, String message) {
		//エラーメッセージ表示する際、ローディングなし
		dismissOverlayProgress();
		
		DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, this, title, message, null);
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		
		if (backListener != null) {
			backListener.onEnd();
		}
		
		// 現在のFragment
		Fragment fragment = getSupportFragmentManager().findFragmentById(
				R.id.container);

		try {
			Method m = fragment.getClass().getMethod("dispatchKeyEvent",
					KeyEvent.class);
			boolean ret = (Boolean) m.invoke(fragment, e);
			if (ret) {
				return ret;
			}

		} catch (NoSuchMethodException e1) {

		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		}

		return super.dispatchKeyEvent(e);
	}

	@Override
	public void onBackStackChanged() {
		
		if (backListener != null) {
			backListener.onEnd();
		}
		
		// 現在のFragment
		Fragment fragment = getSupportFragmentManager().findFragmentById(
				R.id.container);
		
		try {
			Method m = fragment.getClass().getMethod("onBackStackChanged");
			m.invoke(fragment);
			
		} catch (NoSuchMethodException e1) {

		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		}
	}
	
	public int getActionBarHeight() {
		return mActionBar.getHeight();
	}

	public void monitorActionBarAnimation(final LeoViewListener listener) {
		try {
			// Get the Animator used internally
			final Class<?> actionBarImpl = mActionBar.getClass();
			final Field currentAnimField = actionBarImpl
					.getDeclaredField("mCurrentShowAnim");

			// Monitor the animation
			final Animator currentAnim = (Animator) currentAnimField
					.get(mActionBar);
			currentAnim.addListener(new AnimatorListenerAdapter() {

				@Override
				public void onAnimationEnd(Animator animation) {
					if (listener != null) {
						listener.onComplete();
					}

					currentAnim.removeAllListeners();
				}

			});
		} catch (final Exception ignored) {
			// Nothing to do
		}
	}
	
	// *********** exception ************
	protected void detectException() {

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				((MainApplication)getApplication())
						.handleUncaughtException(thread, e);
			}
		});
	}
}