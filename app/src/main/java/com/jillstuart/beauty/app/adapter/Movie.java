package com.jillstuart.beauty.app.adapter;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Movie {
    private String title, image;

    public Movie() {
    }

    public Movie(String title, String image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
