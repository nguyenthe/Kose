package com.jillstuart.beauty.app.mypage.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;

public class MyPageLoginPreviewFragment extends MyPageBaseFragment {
	private UserRegistInfo info;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		info = getArguments().getParcelable(Constant.KEY_FG_DATA);
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_login_preview,
				viewgroup, false);

		initViews(view);

		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	private void initViews(View view) {

		int btnTitleId = isLogined ? R.string.LBL_BTN_SUBMIT
				: R.string.LBL_BTN_NEXT;
		String btnTitle = mActivity.getString(btnTitleId);

		String title = Constant.STRING_EMPTY;
		String content = Constant.STRING_EMPTY;

		if (info.getType() == Page_Type.MyPage_Login_Card.getValue()) {
			title = mActivity.getString(R.string.LBL_MP_PR_TITLE_CARD);
			content = info.getCardNo();
		} else if (info.getType() == Page_Type.MyPage_Login_Ec.getValue()) {
			title = mActivity.getString(R.string.LBL_MP_PR_TITLE_EC);
			content = String.format("%1$s\n%2$s", info.getEcEmail(),
					info.getEcNo());
		}

		// タイトル
		TextView titleTextView = (TextView) view
				.findViewById(R.id.mypage_login_preview_title);
		titleTextView.setText(title);

		TextView textTextView = (TextView) view
				.findViewById(R.id.mypage_login_preview_text);
		textTextView.setText(String.format(
				mActivity.getString(R.string.LBL_MP_PR_TEXT), btnTitle));

		// コンテンツ
		TextView contentTextView = (TextView) view
				.findViewById(R.id.mypage_login_preview_content);
		contentTextView.setText(content);

		// ボタン
		Button btnNextButton = (Button) view
				.findViewById(R.id.mypage_login_preview_btn_next);
		btnNextButton.setText(btnTitle);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});

	}

	private void btnNextClick() {
		if (isLogined) {
			Data_Type dataType = null;
			if (info.getType() == Page_Type.MyPage_Login_Card.getValue()) {
				dataType = Data_Type.API_Post_Add_Card;
			} else if (info.getType() == Page_Type.MyPage_Login_Ec.getValue()) {
				dataType = Data_Type.API_Post_Add_Ec;
			}
			final Data_Type type = dataType;

			JillDataControll.requestData(type, info, mActivity,
					new LeoDataAPIListener() {
						@Override
						public void onRequstDataComplete(UserInfo data) {
							afterApi(
									data,
									getString(R.string.MSG_SHOW_MP_IN2_SUCCESS_TITLE),
									getString(R.string.MSG_SHOW_MP_IN2_SUCCESS_TEXT),
									true);
						}

					});
		} else {
			MyPageLoginInputRecoveryFragment fragment = new MyPageLoginInputRecoveryFragment();

			Bundle bd = new Bundle();
			bd.putParcelable(Constant.KEY_FG_DATA, info);
			fragment.setArguments(bd);

			openFragment(fragment);
		}
	}
}
