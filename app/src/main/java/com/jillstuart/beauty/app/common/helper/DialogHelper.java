package com.jillstuart.beauty.app.common.helper;

import java.util.List;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.listener.LeoAdapterViewOnItemClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogConfirmListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogDismissListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogListSelectCompleteListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogTextInputCompleteListener;
import com.jillstuart.beauty.app.common.view.DialogBorderBack;
import com.jillstuart.beauty.app.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class DialogHelper {

	private static AlertDialog mAlertDialog;
	private static TextView mTitleTextView;
	private static TextView mMsgTextView;
	private static Button cancelButton;
	private static Button okButton;
	private static boolean isPositiveButton;

	// パラメーター
	private static int mAlertViewType;
	private static String mTitle;
	private static String mMsg;
	private static String mBtnOkTitle;
	private static String mBtnCancelTitle;
	private static Object mDefalutValue;
	private static LeoDialogCompleteListener mCompleteListener;
	private static Activity mActivity;

	// select list alertview
	private static int listSelectIndex;
	private static ListView mListView;
	private static ArrayAdapter<String> mListAdapter;
	private static List<String> mListData;

	public static void showAlertView(Alert_Type alertViewType,
			Activity activity, String title, String text,
			LeoDialogCompleteListener listener) {
		showAlertView(alertViewType, activity, title, text,null,null,listener);
	}

	public static void showAlertView(Alert_Type alertViewType,
			Activity activity, String title, String text, String okBtnString,
			String cancelBtnString, LeoDialogCompleteListener listener) {

		mAlertViewType = alertViewType.getValue();

		mActivity = activity;
		mTitle = title;
		mMsg = text;
		mCompleteListener = listener;
		
		mBtnOkTitle = okBtnString;
		mBtnCancelTitle = cancelBtnString;
		
		createAlertView();
	}

	public static void showAlertViewWithData(Alert_Type alertViewType,
			Activity activity, String title, String text,List<String> listData, Object defalutValue,
			LeoDialogCompleteListener listener) {
		mAlertViewType = alertViewType.getValue();

		mActivity = activity;
		mTitle = title;
		mMsg = text;
		mListData = listData;
		mDefalutValue = defalutValue;
		mCompleteListener = listener;

		createAlertView();
	}

	/**
	 * AlertView
	 * 
	 * @param isShowDialog
	 */
	public static void createAlertView() {

		if (mAlertDialog != null && mAlertDialog.isShowing() == true) {
			return;
		}

		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mActivity);

		// View 設定
		alertDialogBuilder.setView(createDialogCustomView());
		// ボタン設定
		setDialogBtn();

		// ダイアログを表示
		mAlertDialog = alertDialogBuilder.create();
		mAlertDialog.setCanceledOnTouchOutside(false);
		
		// Keyboardのback など
		mAlertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
						((AlertDialog) dialog).dismiss();
						return true;
					}
				}
				return false;
			}
		});
		// mAlertDialog
		// .setOnCancelListener(new DialogInterface.OnCancelListener() {
		// @Override
		// public void onCancel(DialogInterface dialog) {
		// cancelEvent(dialog);
		// }
		// });
		mAlertDialog
				.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						if (isPositiveButton) {
							okEvent(dialog);
						} else {
							cancelEvent(dialog);
						}
					}
				});
		mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				// 初期化の時、falseになる
				// Keyboardのbackなど用
				isPositiveButton = false;
				showEvent(dialog);
			}
		});

		mAlertDialog.show();
	}

	/**
	 * ボタン　設定
	 * 
	 */
	private static void setDialogBtn() {
		if (mAlertViewType == Alert_Type.Number_Input.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()
				|| mAlertViewType == Alert_Type.Msg_Confirm.getValue()) {
			setDialogTwoBtn();
		} else if (mAlertViewType == Alert_Type.List_Select.getValue()) {
			setDialogBtnListSelectView();
		}
	}

	@SuppressWarnings("deprecation")
	private static void setDialogTwoBtn() {
		if (okButton != null) {
			okButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					isPositiveButton = true;
					mAlertDialog.dismiss();
				}
			});
		}

		if (cancelButton != null) {
			cancelButton
					.setOnClickListener(new View.OnClickListener() {
						public void onClick(View view) {
							isPositiveButton = false;
							mAlertDialog.dismiss();
						}
					});

			// only close.btn background color change
			if (mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
				cancelButton.setBackgroundDrawable(mActivity.getResources()
						.getDrawable(R.drawable.shape_btn_enable));
			}
		}
	}

	private static void setDialogBtnListSelectView() {

		if (okButton != null) {
			okButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					isPositiveButton = true;
					mAlertDialog.dismiss();
				}
			});
		}
	}

	/**
	 * OK イベント
	 * 
	 * @param dialog
	 */
	private static void okEvent(DialogInterface dialog) {
		if (mAlertViewType == Alert_Type.Number_Input.getValue()) {
			okEventInputAlertView(dialog);
		} else if (mAlertViewType == Alert_Type.List_Select.getValue()) {
			okEventListSelectView();
		} else if (mAlertViewType == Alert_Type.Msg_Confirm.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
			okEventMsgView();
		}
	}

	private static void okEventInputAlertView(DialogInterface dialog) {
		if (mListData == null) {
			return;
		}

		EditText dText = (EditText) ((AlertDialog) dialog)
				.findViewById(Constant.TAG_COUPON_NUMBER_INPUT_VIEW);
		String inputNoString = Constant.STRING_EMPTY;
		if (dText.getText() != null) {
			inputNoString = dText.getText().toString().trim();
		}

		// パスワード正しい
		if (mListData.contains(inputNoString)) {
			if (mCompleteListener != null) {
				((LeoDialogTextInputCompleteListener) mCompleteListener)
						.OnDialogTextInputComplete(inputNoString);
			}

		} else {
			// パスワード違え　、　alert 表示し続け
			((AlertDialog) dialog).show();
		}

	}

	private static void okEventListSelectView() {

		if (mCompleteListener != null) {
			((LeoDialogListSelectCompleteListener) mCompleteListener)
					.OnDialogListSelectComplete(listSelectIndex);
		}
	}

	private static void okEventMsgView() {
		if (mCompleteListener == null) {
			return;
		}
		if (mCompleteListener instanceof LeoDialogConfirmListener) {
			((LeoDialogConfirmListener) mCompleteListener).onOkClick();
		} else if (mCompleteListener instanceof LeoDialogDismissListener) {
			((LeoDialogDismissListener) mCompleteListener).OnDismiss();
		}
	}

	/**
	 * キャンセル イベント
	 * 
	 * @param dialog
	 */
	private static void cancelEvent(DialogInterface dialog) {
		if (mAlertViewType == Alert_Type.Number_Input.getValue()) {
			cancelEventInputAlertView(dialog);
		} else if (mAlertViewType == Alert_Type.Msg_Confirm.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
			cancelEventMsgView();
		}
	}

	private static void cancelEventInputAlertView(DialogInterface dialog) {

		EditText editText = (EditText) ((AlertDialog) dialog)
				.findViewById(Constant.TAG_COUPON_NUMBER_INPUT_VIEW);
		
		@SuppressWarnings("static-access")
		InputMethodManager inputMethodManager = (InputMethodManager) mActivity
				.getSystemService(mActivity.INPUT_METHOD_SERVICE);
		inputMethodManager
				.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	private static void cancelEventMsgView() {

		if (mCompleteListener == null) {
			return;
		}

		if (mCompleteListener instanceof LeoDialogConfirmListener) {
			((LeoDialogConfirmListener) mCompleteListener).OnCancelClick();
		} else if (mCompleteListener instanceof LeoDialogDismissListener) {
			((LeoDialogDismissListener) mCompleteListener).OnDismiss();
		}
	}

	/**
	 * 表示　イベント
	 * 
	 * @param dialog
	 */
	private static void showEvent(DialogInterface dialog) {
		if (mAlertViewType == Alert_Type.Number_Input.getValue()) {
			showEventInputAlertView(dialog);
		}
	}

	private static void showEventInputAlertView(DialogInterface dialog) {
		EditText editText = (EditText) ((AlertDialog) dialog)
				.findViewById(Constant.TAG_COUPON_NUMBER_INPUT_VIEW);
		@SuppressWarnings("static-access")
		InputMethodManager inputMethodManager = (InputMethodManager) mActivity
				.getSystemService(mActivity.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(editText, 0);
	}

	/**
	 * View初期化
	 * 
	 * @return
	 */
	private static View createDialogCustomView() {
		if (mAlertViewType == Alert_Type.Number_Input.getValue()) {
			return createNumberInputView();
		} else if (mAlertViewType == Alert_Type.List_Select.getValue()) {
			return createListSelectView();
		} else if (mAlertViewType == Alert_Type.Msg_Confirm.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue()
				|| mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
			return createMsgView();
		}

		return null;
	}

	/**
	 * メッセージ表示
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private static View createMsgView() {

		final LayoutInflater mLayoutInflater = (LayoutInflater) mActivity
				.getSystemService("layout_inflater");

		// view
		LinearLayout cView = (LinearLayout) mLayoutInflater.inflate(
				R.layout.dialog_msg_view, null);
		// title
		mTitleTextView = (TextView) cView.findViewById(R.id.dialog_msg_title);
		if (CheckHelper.isStringEmpty(mTitle)) {
			mTitleTextView.setVisibility(View.GONE);
		} else {
			mTitleTextView.setText(mTitle);
		}

		mMsgTextView = (TextView) cView.findViewById(R.id.dialog_msg_text);

		if (CheckHelper.isStringEmpty(mMsg)) {
			mMsgTextView.setVisibility(View.GONE);
		} else {
			mMsgTextView.setText(mMsg);
		}

		okButton = (Button) cView.findViewById(R.id.dialog_msg_btn_ok);
		if (CheckHelper.isStringEmpty(mBtnOkTitle) == false) {
			okButton.setText(mBtnOkTitle);
		}
		cancelButton = (Button) cView.findViewById(R.id.dialog_msg_btn_cancel);
		if (CheckHelper.isStringEmpty(mBtnCancelTitle) == false) {
			cancelButton.setText(mBtnCancelTitle);
		}
		
		if (mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue()) {
			cancelButton.setVisibility(View.GONE);
			// 中央揃え
			LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) okButton
					.getLayoutParams();
			ll.setMargins(0, 0, 0, 0);
			okButton.setLayoutParams(ll);
		} else if (mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
			okButton.setVisibility(View.GONE);
			// 中央揃え
			LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) cancelButton
					.getLayoutParams();
			ll.setMargins(0, 0, 0, 0);
			cancelButton.setLayoutParams(ll);
		}
		
		//ok/close 一つボタンの場合、背景色灰色
		if (mAlertViewType == Alert_Type.Msg_Show_WithOk.getValue() || mAlertViewType == Alert_Type.Msg_Show_WithClose.getValue()) {
			DialogBorderBack borderBack = (DialogBorderBack)cView.findViewById(R.id.dialog_border_back);
			borderBack.setBackColor(mActivity.getResources().getColor(R.color.sys_color_line_border));
		}
		
		return cView;
	}

	/**
	 * 数字入力
	 * 
	 * @return
	 */
	private static View createNumberInputView() {

		final LayoutInflater mLayoutInflater = (LayoutInflater) mActivity
				.getSystemService("layout_inflater");

		// view
		LinearLayout cView = (LinearLayout) mLayoutInflater.inflate(
				R.layout.dialog_number_input_view, null);
		// title
		TextView titleTextView = (TextView) cView
				.findViewById(R.id.dialog_number_input_title);
			
		titleTextView.setText(mTitle);

		// input
		EditText mEditText = (EditText) cView
				.findViewById(R.id.dialog_number_input_edittext);

		mEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				Constant.INPUT_MAX_LENGTH_NUMBER) });

		mEditText.setId(Constant.TAG_COUPON_NUMBER_INPUT_VIEW);

		cancelButton = (Button) cView
				.findViewById(R.id.dialog_number_input_btn_cancel);
		okButton = (Button) cView.findViewById(R.id.dialog_number_input_btn_ok);

		// TODO dialog key
		// editText.setOnEditorActionListener(new OnEditorActionListener() {
		// public boolean onEditorAction(TextView v,
		// int actionId, KeyEvent event) {
		// isPositiveButton = true;
		// alertDialog.dismiss();
		// return false;
		// }
		// });

		return cView;
	}

	/**
	 * リスト選択
	 * 
	 * @return
	 */
	private static View createListSelectView() {

		final LayoutInflater mLayoutInflater = (LayoutInflater) mActivity
				.getSystemService("layout_inflater");

		// view
		LinearLayout cView = (LinearLayout) mLayoutInflater.inflate(
				R.layout.dialog_list_select_view, null);
		okButton = (Button) cView
				.findViewById(R.id.dialog_list_select_close_button);
		mListView = (ListView) cView
				.findViewById(R.id.dialog_list_select_listView);

		if (mDefalutValue != null) {
			int selectIndex = Integer.valueOf(mDefalutValue.toString());
			listSelectIndex = selectIndex;
		}

		mListAdapter = new ArrayAdapter<String>(mActivity, 0) {

			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = mLayoutInflater.inflate(
							R.layout.dialog_list_select_view_cell, null);
				}

				String titleTxt = (String) getItem(position);
				TextView titleTextView = (TextView) convertView
						.findViewById(R.id.dialog_list_select_view_cell_title);
				titleTextView.setText(titleTxt);

				changeListSelectCell(convertView, listSelectIndex == position);

				return convertView;
			}
		};

		mListView
				.setOnItemClickListener(new LeoAdapterViewOnItemClickListener() {

					@Override
					public void onSingleClick(AdapterView<?> adapteView,
							View view, int position, long id) {
						listSelectIndex = position;
						View cellView = null;
						for (int i = 0; i < mListView.getCount(); i++) {

							cellView = getListViewByPosition(i, mListView);
							changeListSelectCell(cellView, i == position);
						}
					}
				});

		mListView.setAdapter(mListAdapter);
		mListAdapter.addAll(mListData);
		mListView.setSelection(listSelectIndex);
		return cView;
	}

	/**
	 * リスト選択　クリックイベント
	 * 
	 */
	private static void changeListSelectCell(View adapteView, Boolean isSeleted) {
		TextView titleTextView = (TextView) adapteView
				.findViewById(R.id.dialog_list_select_view_cell_title);
		ImageButton cheTextView = (ImageButton) adapteView
				.findViewById(R.id.dialog_list_select_view_cell_check);
		if (titleTextView == null) {
			return;
		}

		if (isSeleted) {
			// titleTextView.setTextColor(mActivity.getResources().getColor(
			// R.color.sys_color));
			titleTextView.setTypeface(Typeface.DEFAULT_BOLD);
			cheTextView.setVisibility(View.VISIBLE);
		} else {
			// titleTextView.setTextColor(mActivity.getResources().getColor(
			// R.color.sys_font_color_text));
			titleTextView.setTypeface(Typeface.DEFAULT);
			cheTextView.setVisibility(View.GONE);
		}
	}

	private static View getListViewByPosition(int pos, ListView listView) {
		final int firstListItemPosition = listView.getFirstVisiblePosition();
		final int lastListItemPosition = firstListItemPosition
				+ listView.getChildCount() - 1;

		if (pos < firstListItemPosition || pos > lastListItemPosition) {
			return listView.getAdapter().getView(pos, null, listView);
		} else {
			final int childIndex = pos - firstListItemPosition;
			return listView.getChildAt(childIndex);
		}

	}

}
