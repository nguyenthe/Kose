package com.jillstuart.beauty.app.common.listener;

import com.jillstuart.beauty.app.common.Constant;

import android.view.View;

/**
 *Single,Double Click
 *
 * history:create by tll 20140409
 **/
public abstract class LeoViewOnClickListener implements View.OnClickListener{

	long lastClickTime = 0;
	
	@Override
	public void onClick(View v) {
		
		long clickTime = System.currentTimeMillis();
		
		//double click
		if (clickTime - lastClickTime > Constant.TIME_DOUBLE_CLICK_DELTA_MSC) {
			//single click
			onSingleClick(v);
		}else{
			
		}
		lastClickTime = clickTime;
	}

    public abstract void onSingleClick(View v);
    //全体double click 要らない
    //public abstract void onDoubleClick(View v);
}
