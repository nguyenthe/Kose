package com.jillstuart.beauty.app.common.helper;

import java.util.List;
import java.util.Map;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;

import co.leonisand.cupido.CupidoKit.CupidoListener;
import co.leonisand.cupido.CupidoNotification;
import co.leonisand.cupido.CupidoNotification.ConversionType;
import co.leonisand.cupido.CupidoNotification.ResponseType;
import co.leonisand.cupido.CupidoObjective;
import co.leonisand.cupido.CupidoStatus;

public class SdkCupidoHelper {

	//public
	//任意
	public static String CUPIDO_KEY_FLG = "from_cupido";
	
	//固定：SDKと関わる
	public static String CUPIDO_KEY_RESPONSE_TYPE = "response_type";
	public static String CUPIDO_KEY_ID = "id";

	public enum Cupido_Data_Key {
		Status("status"), Notification("notification"), Notifications(
				"notifications"), Objectives("objectives");

		private final String value;

		private Cupido_Data_Key(final String newValue) {
			value = newValue;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * ConversionType
	 * 
	 * @param push
	 * @param conversionType
	 */
	public static void setPushConversionType(CupidoNotification push,
			ConversionType conversionType) {
		if (push == null || conversionType == null) {
			return;
		}

		if (conversionType.equals(push.getConversion())) {
			setPushResponseType(push, ResponseType.CONVERSION);
		}

	}

	/**
	 * ResponseType
	 * 
	 * @param push
	 * @param responseType
	 */
	public static void setPushResponseType(CupidoNotification push,
			final ResponseType responseType) {
		push.sendResponse(responseType, new CupidoListener() {
			
			@Override
			public void onFail(String arg0) {
				
			}
			
			@Override
			public void onDone(Map<String, Object> arg0) {
				
			}
		});
	}

	private static void setCupidoTypeOpen(CupidoNotification push) {

		 //プッシュ通知画面がないため、ここでview
		setPushResponseType(push, ResponseType.VIEW);

		setPushConversionType(push, ConversionType.NOTIFICATION_OPENED);

	}

	/**
	 * 通知届いた後処理
	 * 
	 * @param cupidoId
	 * @param listener
	 */
	public static void detectPush(int cupidoId, final LeoDataSingleListener listener) {

		// プッシュ通知取得
		CupidoNotification.notification(cupidoId, new CupidoListener(){

			@Override
			public void onDone(Map<String, Object> result) {

				CupidoStatus status = (CupidoStatus) result
						.get(Cupido_Data_Key.Status.getValue());
				if (status != null && status.getCode() == 0) {

					CupidoNotification push = (CupidoNotification) result
							.get(Cupido_Data_Key.Notification
									.getValue());

					// type 設定
					setCupidoTypeOpen(push);

					// 開く対象
					detectCupidoOpenTargetType(push, listener);

				}
			}

			@Override
			public void onFail(String arg0) {

			}

		});

	}

	private static void detectCupidoOpenTargetType(final CupidoNotification push,
			final LeoDataSingleListener listener) {
		push.objectives(new CupidoListener() {

			@Override
			public void onDone(Map<String, Object> result) {
				@SuppressWarnings("unchecked")
				List<CupidoObjective> objectives = (List<CupidoObjective>) result
						.get(Cupido_Data_Key.Objectives
								.getValue());
				if (objectives.size() > 0) {
					openTab(objectives.get(0), listener);
				}
			}

			@Override
			public void onFail(String arg0) {

			}
		});
	}

	private static void openTab(CupidoObjective objective,
			LeoDataSingleListener listener) {

		String tagName = Constant.STRING_EMPTY;
		switch (objective.getObjectiveType()) {
		case APP:
		case OFFER:
			// 処理なし
			break;
		case COUPON:
			tagName = Constant.TAG_TAB_COUPON;
			break;
		case RECOMMENDATION:
			tagName = Constant.TAG_TAB_NEWS;
			break;
		case NOTIFICATION:
			// 処理なし
			break;
		default:
			break;
		}

		if (listener != null) {
			listener.onRequstDataComplete(
					Request_Status.Success, tagName);
		}
	}
}
