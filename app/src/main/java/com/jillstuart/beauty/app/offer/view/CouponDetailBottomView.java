package com.jillstuart.beauty.app.offer.view;

import co.leonisand.offers.OffersCoupon;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.DateHelper;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponDetailBottomView extends CouponDetailBaseSubView{

	// Unused view
	private TextView mExpire;
	private TextView mShopList;
	private ImageView mShopListBtnImage;
	private TextView mTopDesc;
	private TextView mBottomDesc;
	private String mShopListUrl;

	public CouponDetailBottomView(Context context) {
		super(context);
		initialize();
	}

	public CouponDetailBottomView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public CouponDetailBottomView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs,
				R.styleable.coupon_bottom_view);

		attributes.recycle();

		inflate(getContext(), R.layout.offer_coupon_view_detail_bottom, this);

		mExpire = (TextView) findViewById(R.id.coupon_detail_use_date);

		mShopList = (TextView) findViewById(R.id.coupon_detail_shoplist);
		mShopListBtnImage = (ImageView) findViewById(R.id.coupon_detail_shoplist_img);

		mTopDesc = (TextView) findViewById(R.id.coupon_detail_top_description);
		mBottomDesc = (TextView) findViewById(R.id.coupon_detail_bottom_description);
	}

	public void setData(OffersCoupon coupon) {
		setUnusedBottomView(coupon);
	}

	private void setUnusedBottomView(OffersCoupon coupon) {

		String topDesc = null;
		// ポイントクーポンの場合、ポイント表示
		if (isPointShow()) {
			if (point > 0) {

				topDesc = String.format(
						getContext().getString(R.string.LBL_CP_USE_POINT_PLUS),
						point);
			} else {
				topDesc = String.format(
						getContext().getString(R.string.LBL_CP_USE_POINT_MIUS),
						point * -1);
			}
		} else {
			// 通常クーポンの場合、上段説明文表示
			topDesc = coupon.getTopDescription();
		}

		if (CheckHelper.isStringEmpty(topDesc) == false) {
			mTopDesc.setVisibility(View.VISIBLE);
			mTopDesc.setText(topDesc);
		} else {
			mTopDesc.setVisibility(View.GONE);
		}

		// 利用可能店舗
		if (CheckHelper.isStringEmpty(coupon.getBottomDescription()) == false) {
			mShopListUrl = coupon.getBottomDescription();
		} else {
			mShopListUrl = UrlConstant.URL_TAB_SHOPLIST;
		}

		// 利用条件
		String useCondition = coupon.getUseConditionDescription();
		if (CheckHelper.isStringEmpty(useCondition) == false) {

			mBottomDesc.setVisibility(View.VISIBLE);
			mBottomDesc.setText(useCondition);
		} else {
			mBottomDesc.setVisibility(View.GONE);
		}

		String start = DateHelper.dateFormatOfferDateStringToFormatString(
				coupon.getAvailableFrom(),
				getContext().getString(R.string.format_coupon_use_perid));
		String end = DateHelper.dateFormatOfferDateStringToFormatString(
				coupon.getAvailableTo(),
				getContext().getString(R.string.format_coupon_use_perid));
		String dayStart = DateHelper.dateDayOfWeek(coupon.getAvailableFrom());
		String dayEnd = DateHelper.dateDayOfWeek(coupon.getAvailableTo());
		if (dayStart.length() > 1) {
			dayStart = dayStart.substring(0, 1);
		}
		if (dayEnd.length() > 1) {
			dayEnd = dayEnd.substring(0, 1);
		}
		mExpire.setText(String.format(
				getContext().getString(R.string.LBL_CP_USE_DATE), start,
				dayStart, end, dayEnd));
	}

	public String getShopListUrl() {
		return mShopListUrl;
	}	
	
	public void setBtnShopClickListener(OnClickListener listener) {

		mShopList.setOnClickListener(listener);
		mShopListBtnImage.setOnClickListener(listener);
	}
}
