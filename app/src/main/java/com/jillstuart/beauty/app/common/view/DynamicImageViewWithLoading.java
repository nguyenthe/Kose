package com.jillstuart.beauty.app.common.view;

import com.jillstuart.beauty.app.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class DynamicImageViewWithLoading extends FrameLayout{

	private DynamicImageView imageView;
	private ProgressBar imageProgressBar;
	
	public DynamicImageViewWithLoading(Context context) {
		super(context);
		initialize();
	}

	public DynamicImageViewWithLoading(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public DynamicImageViewWithLoading(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs,
				R.styleable.common_view_image_with_loading);

		attributes.recycle();

		inflate(getContext(), R.layout.common_view_image_with_loading, this);

		imageView = (DynamicImageView)findViewById(R.id.image);
		imageProgressBar = (ProgressBar)findViewById(R.id.image_loading);
		
	}

	public DynamicImageView getImageView() {
		return imageView;
	}

	public ProgressBar getImageProgressBar() {
		return imageProgressBar;
	}

	
}
