package com.jillstuart.beauty.app.common.view;

import com.jillstuart.beauty.app.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DialogBorderBack extends FrameLayout {

	public DialogBorderBack(Context context) {
		super(context);
		initialize();
	}

	public DialogBorderBack(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public DialogBorderBack(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("Recycle")
	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs,
				R.styleable.dialog_border_back);

		int backColorId = attributes.getResourceId(
				R.styleable.dialog_border_back_back_color,
				R.color.sys_background_color); //規定はview規定の背景色

		attributes.recycle();

		inflate(getContext(), R.layout.dialog_border_back, this);
		
		TextView backTextView = (TextView) findViewById(R.id.back);
		backTextView.setBackgroundColor(getContext().getResources().getColor(backColorId));

	}
	
	public void setBackColor(int backColorId){
		TextView backTextView = (TextView) findViewById(R.id.back);
//		backTextView.setBackgroundColor(getContext().getResources().getColor(backColorId));
		backTextView.setBackgroundColor(backColorId);
		
	}
}
