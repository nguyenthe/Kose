package com.jillstuart.beauty.app.common.fragment;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.helper.OpenHepler;
import com.jillstuart.beauty.app.data.JsonDataControll;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;

@SuppressLint("SetJavaScriptEnabled")
public abstract class BaseWebViewFragment extends BaseFragment {

	protected WebView mWebView;
	protected WebSettings mWebSettings;
	protected WebViewClient mWebViewClient;
	protected ImageButton mBackButton;
	protected ImageButton mForwardButton;
	protected ImageButton mReloadButton;
	protected ImageButton mOpenButton;
	protected LinearLayout mBrowse;
	protected boolean isHasBrowseBar;
	protected boolean isBasic;
	protected boolean isAddCss;

	protected String mUrl;

	protected LeoListener openOutSiteDissmissLisenter;
	
	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);

		View view = layoutinflater.inflate(R.layout.webview_with_browser, viewgroup, false);
		mWebView = (WebView) view.findViewById(R.id.webView);
		mBrowse = (LinearLayout) view.findViewById(R.id.web_view_brower);

		if (isHasBrowseBar == false) {
			if (mBrowse != null) {
				mBrowse.setVisibility(View.GONE);
			}
		} else {
			setBowseBar();
		}

		setWebView();
		setWebViewOption();

		return view;
	}

	private void setBowseBar() {

		if (mBrowse == null) {
			return;
		}

		mBackButton = (ImageButton) mBrowse.findViewById(R.id.web_view_back_button);

		mBackButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (canGoBack())
					goBack();
			}
		});
		mBackButton.setEnabled(false);

		mForwardButton = (ImageButton) mBrowse.findViewById(R.id.web_view_forward_button);

		mForwardButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (canGoForward())
					goForward();
			}
		});
		mForwardButton.setEnabled(false);

		mReloadButton = (ImageButton) mBrowse.findViewById(R.id.web_view_reload_button);
		if (mReloadButton != null)
			mReloadButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					reload();
				}
			});

		mOpenButton = (ImageButton) mBrowse.findViewById(R.id.web_view_open_button);
		mOpenButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String stringUrl = getUrl();
				if (stringUrl == null) {
					return;
				}
				Uri data = Uri.parse(getUrl());
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(data);
				try {
					startActivity(intent);
				} catch (ActivityNotFoundException e) {

				}
			}
		});
	}

	protected void loadUrl(String url) {
		if (mWebView != null && mActivity.isConnectedOrConnecting()) {
			mWebView.loadUrl(url);
		} else {
			mActivity.showErrorNoNetworkConnection();
		}
	}

	protected void reload() {
		if (mWebView != null)
			mWebView.reload();
	}

	protected void goBack() {
		if (mWebView != null)
			mWebView.goBack();
	}

	protected void goForward() {
		if (mWebView != null)
			mWebView.goForward();
	}

	protected boolean canGoForward() {
		return mWebView != null && mWebView.canGoForward();
	}

	protected boolean canGoBack() {
		return mWebView != null && mWebView.canGoBack();
	}

	protected String getUrl() {
		return mWebView != null ? mWebView.getUrl() : null;
	}

	public boolean onBackKeyEvent() {
		if (canGoBack()) {
			goBack();
		} else {
			// collapse();
			mFragmentManager.popBackStack();
		}
		return true;
	}

	public boolean dispatchKeyEvent(KeyEvent e) {

		if (KeyEvent.ACTION_DOWN == e.getAction() && KeyEvent.KEYCODE_BACK == e.getKeyCode()) {

			if (canGoBack()) {
				goBack();
				return true;
			}
		}

		return false;
	}

	protected void setWebViewOption() {

		super.setWebViewSetting(mWebView);

		if (isBasic) {
			setWebViewClientBasic();
		} else {
			setWebViewClient();
		}

		mWebView.setWebViewClient(mWebViewClient);
	}

	private void setWebViewClient() {
		mWebViewClient = new ToolBarWebViewClient();
	}

	private void setWebViewClientBasic() {
		mWebViewClient = new ToolBarWebViewClient() {
			@Override
			public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

				handler.proceed(UrlConstant.APP_BASIC_ID, UrlConstant.APP_BASIC_PW);

			}
		};
	}

	private class ToolBarWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			mActivity.showOverlayProgress();
		}

		@Override
		public void onPageFinished(WebView view, String url) {

			addCss();
			
			if (mWebView != null && mBackButton != null) {
				mBackButton.setEnabled(mWebView.canGoBack());
			}
			if (mWebView != null && mForwardButton != null) {
				mForwardButton.setEnabled(mWebView.canGoForward());
			}

			if (!mWebView.getSettings().getLoadsImagesAutomatically()) {
				mWebView.getSettings().setLoadsImagesAutomatically(true);
			}

			mActivity.dismissOverlayProgress();
		};

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			if (KoseHelper.isCanAccessUrlAtApp(url) == false) {
				OpenHepler.openOutSideWebViewConfirm(mActivity, null, url,openOutSiteDissmissLisenter);
				return true;
			}

			if (isBasic) {
				if (KoseHelper.isNeedAddParameterForWebViewLoadUrl(url, mActivity)) {
					String newUrl = KoseHelper.addParameterForWebViewLoadUrl(url);
					loadUrl(newUrl);
					return true;
				}
			}

			return false;
		}
	}

	@SuppressLint("NewApi")
	private void addCss() {

		if (isAddCss == false) {
			return;
		}

		JsonDataControll.getDataJson(Data_Type.Kose_Shop_Css, mActivity, new LeoDataSingleListener() {

			@Override
			public void onRequstDataComplete(Request_Status status, Object data) {
				if (Request_Status.Success != status) {
					return;
				}

				String cssContent = (String) data;
				cssContent = cssContent.replace("\"", "\\\"");
				cssContent = cssContent.replace("\n", "");
				final String css = String.format(UrlConstant.CSS_JS, cssContent);
				
				if (Build.VERSION.SDK_INT >= 19) {
					mWebView.evaluateJavascript(css, new ValueCallback<String>() {
						@Override
						public void onReceiveValue(String value) {
						}
					});
				} else {
					mWebView.post(new Runnable() {
						@Override
						public void run() {
							mWebView.loadUrl("javascript:" + css);
						}
					});
				}

			}

		});

	}

	// abstract
	protected abstract void setWebView();

}
