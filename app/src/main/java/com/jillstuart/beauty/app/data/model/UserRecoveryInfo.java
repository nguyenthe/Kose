package com.jillstuart.beauty.app.data.model;

public class UserRecoveryInfo extends UserInfo{
	
	private String quesCode;
	private String answer;
	private String quesCodeNew;
	private String answerNew;
	public String getQuesCode() {
		return quesCode;
	}
	public void setQuesCode(String quesCode) {
		this.quesCode = quesCode;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getQuesCodeNew() {
		return quesCodeNew;
	}
	public void setQuesCodeNew(String quesCodeNew) {
		this.quesCodeNew = quesCodeNew;
	}
	public String getAnswerNew() {
		return answerNew;
	}
	public void setAnswerNew(String answerNew) {
		this.answerNew = answerNew;
	}
	
	
}
