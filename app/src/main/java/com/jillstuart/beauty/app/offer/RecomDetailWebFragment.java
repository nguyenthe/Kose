package com.jillstuart.beauty.app.offer;

import android.os.Bundle;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.fragment.BaseWebViewHideFragment;
import com.jillstuart.beauty.app.common.listener.LeoListener;


public class RecomDetailWebFragment extends BaseWebViewHideFragment {

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		
		if (getArguments() == null) {
			return;
		}
			String urlString = getArguments().getString(Constant.KEY_FG_URL);
			if (urlString!=null && urlString.startsWith(UrlConstant.APP_BASE_URL) == true) {
				isAddCss = true;
			}else {
				isAddCss = false;
			}
			loadUrl(urlString);
			
			openOutSiteDissmissLisenter = new LeoListener() {
				
				@Override
				public void onEnd() {
					backFragment();
				}
			};
		
	}

}
