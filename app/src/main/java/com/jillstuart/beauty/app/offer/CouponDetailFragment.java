package com.jillstuart.beauty.app.offer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.*;
import android.support.v4.app.FragmentTransaction;
import android.view.*;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.*;
import co.leonisand.offers.*;
import co.leonisand.offers.OffersKit.OffersKitStatusCode;
import co.leonisand.offers.OffersKit.OffersListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.CouponCategroy;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.fragment.BaseFragment;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DateHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.helper.SdkOfferHelper;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogDismissListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogTextInputCompleteListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogConfirmListener;
import com.jillstuart.beauty.app.common.view.LeonisScrollView;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.OffersDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserPointInfo;
import com.jillstuart.beauty.app.fragment.TabShopListWebFragment;
import com.jillstuart.beauty.app.offer.view.CouponDetailBottomView;
import com.jillstuart.beauty.app.offer.view.CouponDetailTopView;
import com.jillstuart.beauty.app.offer.view.CouponDetailUsedView;

import co.leonisand.offers.OffersCouponView;

@SuppressLint("NewApi")
public class CouponDetailFragment extends BaseFragment implements OffersCouponView.OffersCouponViewListener {

	private Context mContext;

	private OffersCoupon mCoupon;
	private int mCoupon_id;

	private boolean isAfterUsed = false;

	// ポイントクーポン用
	private CouponCategroy mCategroy;
	private Integer point;

	// view
	private LeonisScrollView mScrollView;
	private CouponDetailBottomView mBottomView;
	private CouponDetailUsedView mUsedView;
	private CouponDetailTopView mTopView;

	// スタンプ用
	// スタンプ押す範囲表示view
	private LinearLayout mStampWholeBack;
	private View mStampCircle;
	private int mStampCircleHeight = 0;
	// スタンプ押すview
	private OffersCouponView mStampView;
	private FrameLayout mStampDetectLayout;
	private LinearLayout mStampDetectWholeBack;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		mCoupon_id = getArguments().getInt(Constant.KEY_OFFER_COUNPON_DETAIL_ID);
	}

	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);

		mContext = layoutinflater.getContext();

		View view = layoutinflater.inflate(R.layout.offer_coupon_detail, viewgroup, false);

		mScrollView = (LeonisScrollView) view.findViewById(R.id.scrollview);
		mScrollView.setVisibility(View.INVISIBLE);

		// top view
		mTopView = (CouponDetailTopView) view.findViewById(R.id.coupon_detail_topView);

		// bottom view
		mBottomView = (CouponDetailBottomView) view.findViewById(R.id.coupon_detail_bottomView);
		// used view
		mUsedView = (CouponDetailUsedView) view.findViewById(R.id.coupon_detail_usedView);

		// スタンプ用
		initStampViews(view);
		setStampViewCloseBtn(view);
		setStampViewUseBtn(view);

		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		getCoupon();
	}

	public void onResume() {
		super.onResume();

		OffersKit.getInstance().startLocation();
	}

	@SuppressWarnings("static-access")
	public void onPause() {
		super.onPause();

		OffersKit.getInstance().stopLocation();
	}

	@SuppressWarnings("static-access")
	public void onStop() {
		super.onStop();

		// 位置情報取得停止
		OffersKit.getInstance().stopLocation();
	}

	private void getCoupon() {

		mActivity.showOverlayProgress();
		
		Bundle params = new Bundle();
		params.putInt(OffersDataControll.KEY_DATA_PARA_ID, mCoupon_id);
		OffersDataControll.getData(Data_Type.Offer_Coupon_Detail, params, mActivity,
				new LeoDataListener.LeoDataSingleWithParameterListener() {

					@Override
					public void onRequstDataComplete(Request_Status status, Map<String, Object> result) {
						mActivity.dismissOverlayProgress();

						if (status == Request_Status.Success) {
							mCoupon = (OffersCoupon) result.get(OffersDataControll.KEY_DATA_PARA_VALUE);
							
							// ポイントクーポンチェク
							if (checkPointCoupon() == false) {
								return;
							}
							
							// クーポン設定
							setCouponView((OffersStatus)result.get(OffersDataControll.KEY_DATA_PARA_TYPE));

							// edit by tll 20150817
							// 利用ボタンを押した時、スタンプ設定を実行
							// // スタンプ設定
							setStampView();
						
						}

					}
				});

	}

	private void setCouponView(OffersStatus offersstatus) {
		mScrollView.setVisibility(View.VISIBLE);
		mCoupon.setAlreadyRead();

		// top view
		boolean isShowUsedView = isShowUsedView();

		mTopView.setActivity(mActivity);
		mTopView.setCategroy(mCategroy);
		mTopView.setData(mCoupon, isShowUsedView, new LeoListener() {

			@Override
			public void onEnd() {
				boolean isUsed = isShowUsedView();
				if (isUsed) {
					scrollTopBottom();
				}
			}
		});

		mTopView.setActivity(mActivity);
		mTopView.setBtnUseClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnUseClick();
			}
		});

		// bottom
		setBottomView(offersstatus);
		if (isShowUsedView == false) {
			mBottomView.setBtnShopClickListener(new LeoViewOnClickListener() {

				@Override
				public void onSingleClick(View v) {
					openShoplist();
				}
			});
		}

	}

	/**
	 * クーポン利用
	 * 
	 * @param useInfo
	 */

	private void useCoupon(final Object useInfo) {

		mActivity.showOverlayProgress();

		// 通常クーポン
		if ((mCategroy == CouponCategroy.Change || mCategroy == CouponCategroy.Point) == false) {
			doUseCouponInOffer(useInfo);
			return;
		}

		// ポイントクーポン、交換クーポン
		UserPointInfo info = new UserPointInfo();
		info.setPointChange(point);
		// ポイントシステムへ更新
		JillDataControll.requestData(Data_Type.API_Post_ChangePoint, info, mActivity, new LeoDataAPIListener() {

			@Override
			public void onRequstDataComplete(UserInfo data) {
				if (data.isResultSuccess()) {
					doUseCouponInOffer(useInfo);
				} else {

					mActivity.dismissOverlayProgress();

					// if (mCoupon.getCouponTypeCode() ==
					// OffersCouponType.OffersCouponTypeCodeStamp) {

					if (mStampView != null) {
						mStampView.setVisibility(View.GONE);
					}

					// 利用ボタン非活性
					mTopView.setBtnUseEnable(false);

					// }

					// ネットエラーメッセージは共通で表示されるので
					if (JillApiConstant.API_STATUS_ERR_NET_CONNECT.equals(data.getResultCode())) {
						return;
					}

					// ネットエラー以外
					DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null, data.getResultMsg(),
							new LeoDialogDismissListener() {

						@Override
						public void OnDismiss() {
						}

					});

				}
			}

		});

	}

	@SuppressWarnings("unchecked")
	private void doUseCouponInOffer(Object useInfo) {

		if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeStamp) {
			useCouponWithStamp((Map<String, Object>) useInfo);
		} else if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeTap) {
			useCouponWithPassword(null);
		} else if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeIdentification) {
			useCouponWithPassword((String) useInfo);
		}
	}

	private void useCouponWithPassword(String password) {

		isAfterUsed = true;

		mCoupon.apply(password, new OffersListener() {

			public void onDone(Map<String, Object> map) {
				afterUseCoupon(map);
			}

			public void onFail(Integer s1) {
				mActivity.dismissOverlayProgress();
				processMsg(null);
			}
		});
	}

	private void useCouponWithStamp(Map<String, Object> map) {
		mActivity.showOverlayProgress();
		isAfterUsed = true;

		mCoupon.applyWithStamp(map, new OffersListener() {
			public void onDone(Map<String, Object> map) {

				afterUseCoupon(map);
			}

			public void onFail(Integer s1) {
				mActivity.dismissOverlayProgress();
				processMsg(null);
			}
		});
	}

	private void afterUseCoupon(Map<String, Object> map) {

		mActivity.dismissOverlayProgress();

		OffersStatus offersstatus = (OffersStatus) map.get(SdkOfferHelper.Offer_Data_Key.Status.getValue());
		if (offersstatus == null) {
			processMsg(null);
			return;
		}

		if (offersstatus.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusSuccess)) {
			mCoupon = (OffersCoupon) map.get(SdkOfferHelper.Offer_Data_Key.Coupon.getValue());

		}

		// top view reset data
		mTopView.setBtnUseEnable(false);

		setBottomView(offersstatus);

		scrollTopBottom();
	}

	private boolean isShowUsedView() {
		boolean isShowUsedView = mCoupon.isUsed();
		// 利用後毎回表示
		if (isAfterUsed) {
			isShowUsedView = true;
		} else {
			// 何度でも利用できるクーポン、初期化の時表示しない
			isShowUsedView = mTopView.isCouponShowUsed(mCoupon);
		}

		return isShowUsedView;
	}

	private void setBottomView(OffersStatus offersstatus) {

		boolean isShowUsedView = isShowUsedView();

		if (isShowUsedView) {
			mBottomView.setVisibility(View.GONE);
			mUsedView.setVisibility(View.VISIBLE);

			mUsedView.setActivity(mActivity);
			mUsedView.setPoint(point);
			mUsedView.setCategroy(mCategroy);
			mUsedView.setData(mCoupon, offersstatus);

		} else {
			mBottomView.setVisibility(View.VISIBLE);
			mUsedView.setVisibility(View.GONE);

			mBottomView.setPoint(point);
			mBottomView.setCategroy(mCategroy);
			mBottomView.setData(mCoupon);
		}
	}

	private void scrollTopBottom() {

		mScrollView.post(new Runnable() {
			@Override
			public void run() {
				mScrollView.fullScroll(View.FOCUS_DOWN);
			}
		});
	}

	// ******************ボタンクリック処理*********************

	/**
	 * 利用ボタンクリック
	 */
	private void btnUseClick() {
		OffersCouponType openType = null;

		if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeStamp) {
			openType = OffersCouponType.OffersCouponTypeCodeStamp;
		} else if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeTap) {
			openType = OffersCouponType.OffersCouponTypeCodeTap;
		} else if (mCoupon.getCouponTypeCode() == OffersCouponType.OffersCouponTypeCodeIdentification) {
			if (mCategroy == CouponCategroy.Change) {
				openType = OffersCouponType.OffersCouponTypeCodeTap;
			} else {
				openType = OffersCouponType.OffersCouponTypeCodeIdentification;
			}
		}

		if (openType == null) {
			return;
		}

		if (OffersCouponType.OffersCouponTypeCodeStamp == openType) {
			openUseStampView();
		} else if (OffersCouponType.OffersCouponTypeCodeTap == openType) {
			openUseTap();
		} else if (OffersCouponType.OffersCouponTypeCodeIdentification == openType) {
			openUsePassWord();
		}
	}

	/**
	 * 利用可能店舗表示
	 */
	private void openShoplist() {
		TabShopListWebFragment webViewFragment = new TabShopListWebFragment();

		String shopUrl = mBottomView.getShopListUrl();
		if (CheckHelper.isHttpUrl(shopUrl) == false) {
			shopUrl = UrlConstant.URL_TAB_SHOPLIST_COUPON_DEFALUT;
		}

		Bundle bd = new Bundle();
		bd.putString(Constant.KEY_FG_URL, shopUrl);
		webViewFragment.setArguments(bd);

		mFragmentManager.beginTransaction().add(R.id.container, webViewFragment, null)
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
	}

	private void openUsePassWord() {
		DialogHelper.showAlertViewWithData(Alert_Type.Number_Input, mActivity,
				getString(R.string.LBL_CP_USE_PASSWORD_MSG), null, mCoupon.getSecretsList(), null,
				new LeoDialogTextInputCompleteListener() {

					@Override
					public void OnDialogTextInputComplete(String text) {
						useCoupon(text);
					}

				});
	}

	private void openUseTap() {

		// 交換クーポン、店舗選択チェック
		if (CouponCategroy.Change == mCategroy) {
			String shopCode = mTopView.getCouponShopCode();

			//店舗選択していない
			if (CheckHelper.isStringEmpty(shopCode)) {
				DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null,
						getString(R.string.MSG_ERR_CP_USE_CHOOSE_DILIVERY_SHOP), null);

				return;
			}
			
			//店舗コードと管理画面のパスワード設定不一致
			List<String> passwords = mCoupon.getSecretsList();
			if ((passwords != null && passwords.contains(shopCode)) == false) {
				DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null,
						getString(R.string.MSG_ERR_CP_USE_CHOOSE_DILIVERY_SHOP_CODE), null);

				return;
			}
		}

		// 利用しますか？
		DialogHelper.showAlertView(Alert_Type.Msg_Confirm, mActivity, getString(R.string.MSG_ASK_COUPON_TAP_USE), null,
				new LeoDialogConfirmListener() {
					@Override
					public void OnCancelClick() {
					}

					@Override
					public void onOkClick() {
						String password = null;

						// 交換クーポン
						if (CouponCategroy.Change == mCategroy) {
							password = mTopView.getCouponShopCode();
						}

						useCoupon(password);
					}
				});
	}

	// ******************スタンプ処理*********************
	private void openStampUsePassWord(View view) {
		List<String> passwrodList = new ArrayList<String>();
		passwrodList.add(Config.STAMP_USE_PASSWROD);
		DialogHelper.showAlertViewWithData(Alert_Type.Number_Input, mActivity,
				getString(R.string.LBL_CP_USE_STAMP_CODE_MSG), null, passwrodList, null,
				new LeoDialogTextInputCompleteListener() {

					@Override
					public void OnDialogTextInputComplete(String text) {
						readyStamp();
					}

				});

	}

	private void openUseStampView() {
		Point stampViewSize = Common.getCanUseDisplaySize(mActivity, mContext);
		FrameLayout.LayoutParams btnLayoutParams = new FrameLayout.LayoutParams(stampViewSize.x, stampViewSize.y);
		mStampWholeBack.setLayoutParams(btnLayoutParams);
		mStampDetectWholeBack.setLayoutParams(btnLayoutParams);

		setStampCircel();
		mStampWholeBack.setVisibility(View.VISIBLE);
		mStampDetectWholeBack.setVisibility(View.VISIBLE);

		mScrollView.setEnableScrolling(false);
		// scroll to top
		mScrollView.smoothScrollTo(0, 0);
	}

	private void initStampViews(View view) {

		// スタンプ利用画面
		mStampWholeBack = (LinearLayout) view.findViewById(R.id.coupon_detail_stamp_view_wholeback);
		mStampWholeBack.setVisibility(View.GONE);
		mStampCircle = view.findViewById(R.id.coupon_detail_stamp_circle);

		// スタンプ押す範囲
		mStampDetectWholeBack = (LinearLayout) view.findViewById(R.id.coupon_detail_stamp_view_detect_wholeback);
		mStampDetectWholeBack.setVisibility(View.GONE);
		mStampDetectLayout = (FrameLayout) view.findViewById(R.id.coupon_detail_stamp_detect);
		// mStampDetectLayout.getBackground().setAlpha(100);
	}

	private void setStampViewCloseBtn(View view) {
		// 閉じるボタン
		Button closeButton = (Button) view.findViewById(R.id.coupon_detail_stamp_close_btn);
		closeButton.setBackgroundResource(R.drawable.navigationbar_icon_dissapear);
		closeButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				dismissStampView();
			}
		});

	}

	private void setStampViewUseBtn(View view) {
		// クーポン利用ボタン
		Button useButton = (Button) view.findViewById(R.id.coupon_detail_stamp_use_button);

		useButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openStampUsePassWord(v);
			}
		});
	}

	private void setStampCircel() {

		mStampCircle.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {
				if (mStampCircleHeight == 0) {
					mStampCircleHeight = mStampCircle.getHeight();
				}
				setStampViewCircleSize();
				mStampCircle.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					mStampCircle.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				} else {
					mStampCircle.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
			}

		});
	}

	private void setStampViewCircleSize() {

		Point size = SdkOfferHelper.getStampCircleSize(getActivity(), mStampCircleHeight);

		LinearLayout.LayoutParams btnLayoutParams = new LinearLayout.LayoutParams(size.x, size.y);
		// btnLayoutParams.gravity = Gravity.CENTER;
		mStampCircle.setLayoutParams(btnLayoutParams);

		setStampDetectViewPosition();
	}

	private void setStampDetectViewPosition() {

		// スタンプ設定
		// setStampView();

		mStampView.setVisibility(View.VISIBLE);
		mStampDetectLayout.setVisibility(View.VISIBLE);
	}

	/**
	 * スタンプview
	 */
	private void setStampView() {

		if (mCoupon.getCouponTypeCode() != OffersCouponType.OffersCouponTypeCodeStamp) {
			return;
		}

		mStampView = new OffersCouponView(mContext);
		mStampView.debug = false;
		mStampView.setVisibility(View.GONE);
		mStampView.setOffersCouponViewListener(this);

		mStampDetectLayout.addView(mStampView);
		mCoupon.setCouponView(mStampView);

		if (mCoupon.isUsed() == true) {
			mStampDetectWholeBack.setVisibility(View.VISIBLE);
			mStampView.setVisibility(View.VISIBLE);
		}

	}

	private void dismissStampView() {
		mScrollView.setEnableScrolling(true);
		mStampWholeBack.setVisibility(View.GONE);
		mStampDetectWholeBack.setVisibility(View.GONE);
	}

	private void dissppearViewExceptStamp() {
		mScrollView.setEnableScrolling(true);
		mStampWholeBack.setVisibility(View.GONE);

	}

	// ************スタンプ 処理***************
	@Override
	public void beforeStamp(Map<String, Object> map) {

		vibrate();

		Map<String, Object> option = new HashMap<String, Object>();

		resizeAngle(map, option);
		resizeStamp(map, option, true);

		mStampView.setStampedOption(option);
	}

	@Override
	public void afterStamp(Map<String, Object> map) {
		dissppearViewExceptStamp();
		afterStampCouponStamped(map);
	}

	@Override
	public void drawStamp(Map<String, Object> map, Canvas arg1) {

	}

	@Override
	public void gestureSuccess(Map<String, Object> map) {

	}

	@Override
	public void matchStamp(Map<String, Object> map) {

	}

	@Override
	public void releaseStamp(List<Map<String, Object>> map) {

	}

	private void vibrate() {
		Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(500);
	}

	@SuppressLint("ClickableViewAccessibility")
	private void afterStampCouponStamped(Map<String, Object> map) {
		mStampView.setOffersCouponViewListener(null);

		// 消し込み
		useCoupon(map);

	}

	@SuppressWarnings("static-access")
	private void readyStamp() {

		// スタンプ情報
		Map<String, Object> option = new HashMap<String, Object>();
		option.put("angle", 0);
		option.put("x", 160);
		option.put("y", 150);

		// 一番目のスタンプ情報を利用する
		Map<String, Object> stampMap = mCoupon.getStampGroups().get(0);

		// サイズ
		resizeStamp(stampMap, option, false);

		// 押す時間
		option.put("stamped_at", DateHelper.dateFormatDateToString(OffersKit.getInstance().getServerDate(),
				DateHelper.OFFER_DATE_FORMART));

		afterStamp(option);
	}

	@SuppressWarnings("unchecked")
	private void resizeStamp(Map<String, Object> originMap, Map<String, Object> option, boolean isRealStamp) {

		// 本物のスタンプじゃない場合
		if (isRealStamp == false) {
			// グループID
			option.put("group_id", originMap.get("id"));

			// スタンプ情報
			List<Map<String, Object>> stampsList = (List<Map<String, Object>>) originMap.get("stamps");
			// 一番目のスタンプ
			Map<String, Object> stampMap = (Map<String, Object>) (stampsList.get(0));
			// スタンプid
			option.put("stamp_id", stampMap.get("id"));

		}

		Point newSize = SdkOfferHelper.getStampImageSize(mActivity, mStampCircleHeight);
		option.put("width", newSize.x);
		option.put("height", newSize.y);

	}

	private void resizeAngle(Map<String, Object> originMap, Map<String, Object> option) {

		// 逆転調整
		float angle = Float.valueOf(originMap.get("angle").toString());
		if (angle >= 90 && angle <= 180) {
			angle -= 90;
		}

		if (angle >= 180 && angle <= 270) {
			angle += 90;
		}

		option.put("angle", angle);
	}

	/**
	 * ポイントクーポン
	 */

	private boolean checkPointCoupon() {
		mCategroy = CouponCategroy.Nomoral;
		// ポイントクーポン
		if (Constant.KEY_OFFER_COUPON_POINT.equals(mCoupon.getCategory())) {
			mCategroy = CouponCategroy.Point;
		} else if (Constant.KEY_OFFER_COUPON_CHANGE.equals(mCoupon.getCategory())) {
			// 交換クーポン
			mCategroy = CouponCategroy.Change;
		}
		if ((mCategroy == CouponCategroy.Change || mCategroy == CouponCategroy.Point)
				&& KoseHelper.userIsLogined(mActivity.getApplicationContext()) == false) {
			// messgae
			DialogHelper.showAlertView(Alert_Type.Msg_Confirm, mActivity,
					getString(R.string.MSG_SHOW_CP_POINT_COUPON_NEED_LOGIN_TITLE), null,
					getString(R.string.LBL_BTN_CP_LOGIN), getString(R.string.LBL_BTN_BACK),
					new LeoDialogConfirmListener() {

						@Override
						public void OnCancelClick() {
							backFragment();
						}

						@Override
						public void onOkClick() {
							openTabByTag(Constant.TAG_TAB_MYPAGE);
						}

					});
			return false;
		}

		// ポイントを取得
		getCouponPoint();
		return true;
	}

	private void getCouponPoint() {
		String pointString = mCoupon.getTopDescription();
		if (CheckHelper.isStringEmpty(pointString)) {
			point = 0;
		} else {
			String pointOnlyNumber = pointString;
			if (pointString.startsWith(Constant.KEY_OFFER_COUPON_POINT_PREFIX_MINUS)) {
				pointOnlyNumber = pointString.substring(1);
				point = ConvertHelper.toInteger(pointOnlyNumber) * -1;
			} else if (pointString.startsWith(Constant.KEY_OFFER_COUPON_POINT_PREFIX_PLUS)) {
				pointOnlyNumber = pointString.substring(1);
				point = ConvertHelper.toInteger(pointOnlyNumber);
			} else {
				point = ConvertHelper.toInteger(pointOnlyNumber);
			}
		}
	}
}