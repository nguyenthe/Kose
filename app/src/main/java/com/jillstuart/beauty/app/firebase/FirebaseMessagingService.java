package com.jillstuart.beauty.app.firebase;

/**
 * Created by HongNgoc on 4/4/2017.
 */
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RemoteViews;

import com.google.firebase.messaging.RemoteMessage;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.common.fragment.BaseWebViewNoToolBarFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FirebaseMsgService";
    public static final String INTENT_FILTER = "INTENT_FILTER";

    private LocalBroadcastManager broadcaster;
    ArrayList<String> list_image = new ArrayList<String>();

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //추가한것
        System.out.println("Testing after received message from Firebase " + remoteMessage.getData().get("title") + "----" +
                remoteMessage.getData().get("message") +"---'"+ remoteMessage.getData().get("image")+"---'" + remoteMessage.getData().get("couponID") +"---'" +
                remoteMessage.getData().get("category") +"---'"+remoteMessage.getData().get("couponType"));

        System.out.println("Get Title : " + remoteMessage.getData().get("title"));

        String title = "";
        String message = "";
        if (remoteMessage.getNotification() == null) {
            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("message");
            System.out.println("Title Title 1: " + title + "--" + message);
            //get data from firebase then parse to Object
            parseStringToObject(message);
            //send broadcast
            newIntent(list_image);
            System.out.println("Body : " + message);
            sendNotification(message,title);
        } else {
            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
            System.out.println("Title Title 2: " + title + "--" + message);
            parseStringToObject(message);
            newIntent(list_image);
            System.out.println("Body : " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle());
        }

    }

    public void parseStringToObject(String message){
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(message);
            System.out.println("Size Array was : " + jsonarray.length());
            for(int i=0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                ObjectReceiverData objectReceiverData = new ObjectReceiverData();
                objectReceiverData.setImage(jsonobject.getString("image"));
                objectReceiverData.setCouponType(jsonobject.getString("couponType"));
                objectReceiverData.setCouponID(jsonobject.getString("couponID"));
                objectReceiverData.setTitle(jsonobject.getString("title"));
                objectReceiverData.setCategory(jsonobject.getString("category"));
                System.out.println("url image : " + objectReceiverData.getImage());
                list_image.add(objectReceiverData.getImage());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void newIntent(ArrayList<String> array){
        Intent intent = new Intent("MyData");
        intent.putStringArrayListExtra("image",array);
        broadcaster.sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    private void sendNotification(String messageBody,String title) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        final RemoteViews expandedView = new RemoteViews(this.getPackageName(),
                R.layout.notification_custom_remote);
        expandedView.setTextViewText(R.id.text,messageBody);
        expandedView.setTextViewText(R.id.title,title);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.hoasen)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setContent(expandedView);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }

}


