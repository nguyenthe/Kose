package com.jillstuart.beauty.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.SdkCupidoHelper;

public class SplashActivity extends Activity {

	private Intent mNotiIntent = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		mNotiIntent = getIntent();
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		Handler hdl = new Handler();
		hdl.postDelayed(new splashHandler(), 500);

		
	}

	class splashHandler implements Runnable {
		public void run() {

			Intent i = new Intent(getApplication(), MainActivity.class);
			detectCupido(i);
			
			startActivity(i);
			overridePendingTransition(0, 0);
			SplashActivity.this.finish();
		}
	}
	
	private void detectCupido(Intent intent){
		if (mNotiIntent == null || mNotiIntent.getExtras() == null) {
			return;
		}
		
		String responseType = mNotiIntent.getExtras().getString(SdkCupidoHelper.CUPIDO_KEY_RESPONSE_TYPE);
		int cupidoID =  mNotiIntent.getExtras().getInt(SdkCupidoHelper.CUPIDO_KEY_ID);
		
		if (CheckHelper.isStringEmpty(responseType) == false) {
			intent.putExtra(SdkCupidoHelper.CUPIDO_KEY_FLG, true);
			intent.putExtra(Constant.KEY_FG_ID, cupidoID);
		}
	}
}
