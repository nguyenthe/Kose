package com.jillstuart.beauty.app.offer.view;

import co.leonisand.offers.OffersCoupon;

import com.jillstuart.beauty.app.common.EnumConstant.CouponCategroy;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class CouponDetailBaseSubView extends LinearLayout {

	// data
	protected Integer point;
	protected CouponCategroy categroy;

	protected BaseActionBarActivity activity;

	public CouponDetailBaseSubView(Context context) {
		super(context);
	}

	public CouponDetailBaseSubView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CouponDetailBaseSubView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	protected boolean isPointShow() {

		if (categroy == CouponCategroy.Change
				|| categroy == CouponCategroy.Point) {
			return true;
		}
		return false;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public CouponCategroy getCategroy() {
		return categroy;
	}

	public void setCategroy(CouponCategroy categroy) {
		this.categroy = categroy;
	}

	public boolean isCouponShowUsed(OffersCoupon coupon) {
		boolean isUsed = coupon.isUsed();

		switch (coupon.getCouponTypeCode()) {
		case OffersCouponTypeCodeIdentification:
		case OffersCouponTypeCodeTap:
			 //利用済みの再利用可能クーポン、表示は非利用
			if (coupon.usable() && isUsed == true) {
				isUsed = false;
			}
			break;

		default:
			break;
		}

		return isUsed;
	}
}
