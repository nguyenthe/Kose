package com.jillstuart.beauty.app.data.model;


public class UserRegistInfo extends UserInfo{

	private int type;

	private String cardNo;
	private String ecEmail;
	private String ecNo;
	private String birthday;//max2桁
	private String memoryday;//max4桁
	
	private UserRecoveryInfo recoveryInfo;
	
	public UserRegistInfo() {
		
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getEcEmail() {
		return ecEmail;
	}
	public void setEcEmail(String ecEmail) {
		this.ecEmail = ecEmail;
	}
	public String getEcNo() {
		return ecNo;
	}
	public void setEcNo(String ecNo) {
		this.ecNo = ecNo;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getMemoryday() {
		return memoryday;
	}
	public void setMemoryday(String memoryday) {
		this.memoryday = memoryday;
	}
	public UserRecoveryInfo getRecoveryInfo() {
		return recoveryInfo;
	}
	public void setRecoveryInfo(UserRecoveryInfo recoveryInfo) {
		this.recoveryInfo = recoveryInfo;
	}
	
	
}
