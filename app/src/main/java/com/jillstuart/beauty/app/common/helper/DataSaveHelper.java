package com.jillstuart.beauty.app.common.helper;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Java_Data_Type;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class DataSaveHelper {

	public static void saveLocalDefault(Context context, String key, Object value,Java_Data_Type type) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = sharedPref.edit();
		
		switch (type) {
		case String:
			edit.putString(key, ConvertHelper.toString(value));
			break;
		case Boolean:
			edit.putBoolean(key, ConvertHelper.toBoolean(value));
			break;
		case Integer:
			edit.putInt(key, ConvertHelper.toInteger(value));
			break;
		default:
			break;
		}

		edit.commit();
	}
	
	public static String getLocalStringDefault(Context context, String key, String defaultValue) {
		String value = PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
		return value;
	}
	
	public static void saveLocal(Context context, String key, Object value,Java_Data_Type type) {
		SharedPreferences sharedPref = context.getSharedPreferences(Constant.KEY_LOCAL_SAVE, Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sharedPref.edit();
		
		switch (type) {
		case String:
			edit.putString(key, ConvertHelper.toString(value));
			break;
		case Boolean:
			edit.putBoolean(key, ConvertHelper.toBoolean(value));
			break;
		case Integer:
			edit.putInt(key, ConvertHelper.toInteger(value));
			break;
		default:
			break;
		}

		edit.commit();
	}

	public static Integer getLocalInteger(Context context, String key, Integer defaultValue) {
		SharedPreferences sharedPref = context.getSharedPreferences(Constant.KEY_LOCAL_SAVE, Context.MODE_PRIVATE);
		if (sharedPref.contains(key)) {
			Integer value = sharedPref.getInt(key, defaultValue);
			return value;
		}

		return defaultValue;
	}
	
	public static boolean getLocalBoolean(Context context, String key, boolean defaultValue) {
		SharedPreferences sharedPref = context.getSharedPreferences(Constant.KEY_LOCAL_SAVE, Context.MODE_PRIVATE);
		if (sharedPref.contains(key)) {
			boolean value = sharedPref.getBoolean(key, defaultValue);
			return value;
		}

		return defaultValue;
	}

	public static String getLocalString(Context context, String key, String defaultValue) {
		SharedPreferences sharedPref = context.getSharedPreferences(Constant.KEY_LOCAL_SAVE, Context.MODE_PRIVATE);
		if (sharedPref.contains(key)) {
			return sharedPref.getString(key, defaultValue);
		}

		return defaultValue;
	}

//	public static void saveLocalString(Context context, String key, String value) {
//		SharedPreferences sharedPref = context.getSharedPreferences(Constant.KEY_LOCAL_SAVE, Context.MODE_PRIVATE);
//		SharedPreferences.Editor edit = sharedPref.edit();
//		edit.putString(key, value);
//		edit.commit();
//	}
}
