package com.jillstuart.beauty.app.mypage.login;

import java.util.HashSet;
import java.util.Set;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.data.model.UserRecoveryInfo;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;
import com.jillstuart.beauty.app.mypage.view.MyPageQuestionAskView;

public class MyPageLoginInputRecoveryFragment extends MyPageBaseFragment {

	private MyPageQuestionAskView mQuesAsk;

	// data
	private UserRegistInfo info;

	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_login_input_recovery, viewgroup, false);

		info = getArguments().getParcelable(Constant.KEY_FG_DATA);

		initViews(view);

		return view;
	}

	private void initViews(View view) {
		Button btnNextButton = (Button) view.findViewById(R.id.mypage_login_input_recovery_btn_next);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});

		mQuesAsk = (MyPageQuestionAskView) view.findViewById(R.id.mypage_login_input_recovery_ques_ask);

		mQuesAsk.setActivity(mActivity);
	}

	private boolean checkIsInputValid() {

		resetEditText();

		boolean isValid = true;
		StringBuffer msg = new StringBuffer();
		Set<View> targetsSet = new HashSet<View>();

		// 質問
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAsk.getQuestion())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_SELECT_NOT), "質問"));
			targetsSet.add(mQuesAsk.getEditTextQues());
		}

		// 答え
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAsk.getAnswer())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT), "質問の答え"));
			targetsSet.add(mQuesAsk.getEditTextAnswer());
		}

		if (isValid == false) {
			for (View view : targetsSet) {
				setEditTextValid(view, isValid);
			}

			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null, msg.toString(), null);

			return false;
		}
		return true;
	}

	private void resetEditText() {
		setEditTextValid(mQuesAsk.getEditTextAnswer(), true);
		setEditTextValid(mQuesAsk.getEditTextQues(), true);
	}

	private void btnNextClick() {
		// check input
		if (checkIsInputValid() == false) {
			return;
		}

		// data
		UserRecoveryInfo recoveryInfo = new UserRecoveryInfo();
		recoveryInfo.setAnswer(mQuesAsk.getAnswer());
		recoveryInfo.setQuesCode(mQuesAsk.getQuestionCode());
		info.setRecoveryInfo(recoveryInfo);

		openNext();
	}

	private void openNext() {

		MyPageLoginInputAccountFragment fragment = new MyPageLoginInputAccountFragment();
		Bundle bd = new Bundle();
		bd.putParcelable(Constant.KEY_FG_DATA, info);
		fragment.setArguments(bd);

		openFragment(fragment);
	}
}
