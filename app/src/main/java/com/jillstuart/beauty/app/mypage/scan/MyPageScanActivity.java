package com.jillstuart.beauty.app.mypage.scan;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.scan.zxing.CustomScannerActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import permissions.dispatcher.PermissionsDispatcher;

public class MyPageScanActivity extends Activity {

	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);

		setViews();
	}

	protected void onStart() {
		super.onStart();

		openCamera();
	}

	protected void onResume() {
		super.onResume();

	}

	private void setViews() {
		setContentView(R.layout.fragment);

	}

	private void scanCustomScanner() {

		IntentIntegrator integrator = new IntentIntegrator(this);

		Collection<String> codes = Collections.unmodifiableList(Arrays.asList(Constant.MYPAGE_SCAN_CODE));
		integrator.setDesiredBarcodeFormats(codes);
		integrator.setCaptureActivity(CustomScannerActivity.class);
		integrator.setOrientationLocked(true);
		integrator.setBeepEnabled(true);
		integrator.initiateScan();
	}

	private void openCamera() {
		try {
			// double check,has permission then open camera
			boolean isPermissioned = PermissionsDispatcher.isPerssioned(MyPageScanActivity.this,
					PermissionsDispatcher.REQUEST_CAMERA);
			if (isPermissioned) {
				scanCustomScanner();
			} else {
				// hasn't permission,finish
				MyPageScanActivity.this.finish();
			}
		} catch (Exception e) {
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

		String cardNumber = Constant.STRING_EMPTY;

		if (result != null) {

			if (result.getContents() != null) {
				// Toast.makeText(this, "Scanned: " + result.getContents(),
				// Toast.LENGTH_LONG).show();

				cardNumber = result.getContents();

			}
			afterScan(cardNumber);

		} else {
			// This is important, otherwise the result will not be passed to the
			// fragment
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	private void afterScan(String number) {
		if (CheckHelper.isStringEmpty(number)) {
			number = Constant.STRING_EMPTY;
		}

		Intent intent = new Intent(Constant.KEY_MY_PAGE_EVENT_SCAN);
		// add data
		intent.putExtra(Constant.KEY_FG_DATA, number);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

		finish();
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.KEYCODE_BACK || e.getKeyCode() == KeyEvent.KEYCODE_HOME) {

			afterScan(null);

			return true;
		}

		return super.dispatchKeyEvent(e);
	};
}
