package com.jillstuart.beauty.app.mypage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.view.AutoResizeTextView;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserPointInfo;
import com.jillstuart.beauty.app.mypage.login.MyPageLoginEcFragment;
import com.jillstuart.beauty.app.mypage.recovery.MyPageRecoveryEditFragment;
import com.jillstuart.beauty.app.mypage.recovery.MyPageRecoveryFragment;
import com.jillstuart.beauty.app.mypage.scan.MyPageScanDescFragment;
import com.jillstuart.beauty.app.mypage.view.MyPagePointInforView;

public class MyPageFragment extends MyPageBaseFragment {

	private MyPagePointInforView pointInfoView;
	private AutoResizeTextView pointView;
	private AutoResizeTextView pointClassNameView;
	private TextView pointProgramIdView;

	private View wholeBackView;

	//画面リフレッシュ
	private BroadcastReceiver mRefreshReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// String cardNumber = intent.getStringExtra(Constant.KEY_FG_DATA);
			refreshView();
		}
	};

	private void refreshView() {

		Fragment frg = null;
		frg = mFragmentManager.findFragmentByTag(Constant.TAG_TAB_MYPAGE);
		final FragmentTransaction ft = mFragmentManager.beginTransaction();
		ft.detach(frg);
		ft.attach(frg);
		ft.commitAllowingStateLoss();
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		isLogined = KoseHelper.userIsLogined(mActivity.getApplicationContext());

		int viewid = isLogined ? R.layout.mypage_main_logined
				: R.layout.mypage_main_unlogin;

		wholeBackView = layoutinflater.inflate(viewid, viewgroup, false);
		initViews(wholeBackView);
		wholeBackView.setVisibility(View.INVISIBLE);
		return wholeBackView;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		if (isLogined) {
			getPointInfo();
		}else {
			wholeBackView.setVisibility(View.VISIBLE);
		}

		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mRefreshReceiver,
				new IntentFilter(Constant.KEY_MY_PAGE_EVENT_LOGIN));
	}

	private void initViews(View view) {

		if (isLogined) {
			initLoginedView(view);
		} else {
			initUnloginView(view);
		}
		// 登録済みと未登録のボタンIDを同じにする
		Button btnRecovery = (Button) view
				.findViewById(R.id.mypage_btn_recovery);
		btnRecovery.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openRecovery();
			}
		});
	}

	private void initLoginedView(View view) {
		pointProgramIdView = (TextView) view
				.findViewById(R.id.mypage_logined_program_id_text);
		pointView = (AutoResizeTextView) view.findViewById(R.id.mypage_logined_point);
		pointClassNameView = (AutoResizeTextView) view.findViewById(R.id.mypage_logined_point_class);

		pointInfoView = (MyPagePointInforView) view
				.findViewById(R.id.mypage_logined_point_info);
		pointInfoView.setBtnEcClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openEc();
			}
		});

		pointInfoView.setBtnCardClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openScan();
			}
		});
		
		Button btnToCouponButton = (Button)view.findViewById(R.id.mypage_btn_to_coupon);
		btnToCouponButton.setOnClickListener(new LeoViewOnClickListener() {
			
			@Override
			public void onSingleClick(View v) {
				openCouponList();
			}
		});
	}

	private void initUnloginView(View view) {
		Button btnEc = (Button) view.findViewById(R.id.mypage_unlogin_btn_ec);
		btnEc.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openEc();
			}
		});
		Button btnCard = (Button) view
				.findViewById(R.id.mypage_unlogin_btn_scan);
		btnCard.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				openScan();
			}
		});
	}

	private void openCouponList(){
		((MainActivity)mActivity).openTabByTagForPointCoupon();
	}
	
	private void openEc() {
		openFragment(new MyPageLoginEcFragment());
	}

	private void openRecovery() {
		MyPageBaseFragment fragment = null;
		if (isLogined) {
			// open edit
			fragment = new MyPageRecoveryEditFragment();
		} else {
			fragment = new MyPageRecoveryFragment();
		}
		openFragment(fragment);
	}

	private void openScan() {
		MyPageScanDescFragment fragment = new MyPageScanDescFragment();		
		openFragment(fragment);
	}

	private void getPointInfo() {
		
		JillDataControll.requestData(Data_Type.API_Get_User_Point, null,
				mActivity, new LeoDataAPIListener() {

					@Override
					public void onRequstDataComplete(final UserInfo data) {
						mActivity.dismissOverlayProgress();

						if (data.isResultSuccess()) {
							
							final UserPointInfo pointInfo = (UserPointInfo) data;
							setPointInfo(pointInfo);
							
						} else {
							//共通に表示される
							if (JillApiConstant.API_STATUS_ERR_NET_CONNECT.equals(data.getResultCode())) {
								return;
							}
							
							DialogHelper
									.showAlertView(
											Alert_Type.Msg_Show_WithClose,
											mActivity,
											null,
											data.getResultMsg(),
											null);
						}

					}

				});
	}

	private void setPointInfo(UserPointInfo pointInfo) {
		wholeBackView.setVisibility(View.VISIBLE);

		this.pointClassNameView.setText(pointInfo.getPointClassName());
		this.pointView.setText(pointInfo.getPointString());
		this.pointInfoView.setPointInfo(pointInfo);
		this.pointProgramIdView.setText(KoseHelper.getProgramId(mActivity.getApplicationContext()));

	}
}
