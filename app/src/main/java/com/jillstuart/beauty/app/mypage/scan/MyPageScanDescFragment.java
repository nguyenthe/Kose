package com.jillstuart.beauty.app.mypage.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import permissions.dispatcher.PermissionsDispatcher;
import permissions.dispatcher.PermissionsDispatcher.Permission_Status;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.listener.LeoPersmissionListener;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;
import com.jillstuart.beauty.app.mypage.login.MyPageLoginPreviewFragment;

public class MyPageScanDescFragment extends MyPageBaseFragment {

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
			String cardNumber = intent.getStringExtra(Constant.KEY_FG_DATA);
			afterScan(cardNumber);
		}
	};

	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);

		View view = layoutinflater.inflate(R.layout.mypage_login_scan_desc, viewgroup, false);

		initViews(view);

		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	private void initViews(View view) {
		Button btnNextButton = (Button) view.findViewById(R.id.mypage_login_scan_desc_next_btn);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});
	}

	private void btnNextClick() {
		// check permission

		((MainActivity) mActivity).checkPermission(PermissionsDispatcher.REQUEST_CAMERA,
				new LeoPersmissionListener() {

					@Override
					public void onResult(Permission_Status status) {
						if (Permission_Status.Permitted == status) {
							openScan();
						}
					}
				});

	}

	private void openScan() {

		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
				new IntentFilter(Constant.KEY_MY_PAGE_EVENT_SCAN));

		Intent intent = new Intent(getActivity(), MyPageScanActivity.class);
		startActivity(intent);

	}

	private void afterScan(String cardNumber) {

		if (CheckHelper.isStringEmpty(cardNumber)) {
			return;
		}

		UserRegistInfo info = new UserRegistInfo();
		info.setType(Page_Type.MyPage_Login_Card.getValue());
		info.setCardNo(cardNumber);

		MyPageLoginPreviewFragment previewFragment = new MyPageLoginPreviewFragment();
		Bundle bd = new Bundle();

		bd.putParcelable(Constant.KEY_FG_DATA, info);
		previewFragment.setArguments(bd);

		// open preview page
		openFragment(previewFragment);
	}
}
