package com.jillstuart.beauty.app.offer;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.*;
import android.widget.*;
import co.leonisand.offers.OffersCategory;
import co.leonisand.offers.OffersCoupon;
import co.leonisand.offers.OffersKit;

import java.util.*;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.fragment.BaseListViewFragment;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.ImageDownloadHepler;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.listener.LeoAdapterViewOnItemClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataCountListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataListListener;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.data.OffersDataControll;

public class CouponListFragment extends BaseListViewFragment {

	private ArrayAdapter<OffersCoupon> mListAdapter;

	private String couponPointCategroies;
	private String couponNormalCategroies;

	// 画面リフレッシュ
	private BroadcastReceiver mRefreshReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if (mActivity.getString(R.string.LBL_TB_COUPON_POINT).equals(mActivity.getActionBarTitle())) {
					checkDataType();
					loadFirst();
				}
			} catch (Exception e) {
			}

		}
	};

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setHasOptionsMenu(true);

		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRefreshReceiver,
				new IntentFilter(Constant.KEY_PAGE_LOAD_DATA_FLG));
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater menuinflater) {
		super.onCreateOptionsMenu(menu, menuinflater);
		menuinflater.inflate(R.menu.menu_coupons, menu);
	}

	public boolean onOptionsItemSelected(MenuItem menuitem) {
		return super.onOptionsItemSelected(menuitem);
	}

	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {
		super.onCreateView(layoutinflater, viewgroup, bundle);

		View view = layoutinflater.inflate(R.layout.fragment, viewgroup, false);
		FrameLayout framelayout = (FrameLayout) view.findViewById(R.id.fragment_content);
		mListView = new ListView(mActivity);
		mListView.setDividerHeight(0); // 線消す

		mListAdapter = new ArrayAdapter<OffersCoupon>(mActivity, 0) {
			private LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getSystemService("layout_inflater");

			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = mLayoutInflater.inflate(R.layout.listview_cell_coupon, null);
				}

				OffersCoupon offerscoupon = (OffersCoupon) getItem(position);

				TextView title = (TextView) convertView.findViewById(R.id.cell_coupon_title);
				title.setText(offerscoupon.getTitle());

				TextView type = (TextView) convertView.findViewById(R.id.coupon_cell_type);
				type.setText(getCategory(offerscoupon));

				TextView description = (TextView) convertView.findViewById(R.id.cell_coupon_desc);

				// edit by tll 20140927 説明文は必須じゃない
				if (CheckHelper.isStringEmpty(offerscoupon.getDescription())) {
					description.setVisibility(View.GONE);

				} else {
					description.setVisibility(View.VISIBLE);
					description.setText(offerscoupon.getDescription());
				}

				// edit by tll 20141002
				// 画像設定
				setImageView(convertView, offerscoupon.getThumbnailImage());

				return convertView;
			}
		};
		mListView.setAdapter(mListAdapter);
		framelayout.addView(mListView);

		super.setListViewPaging();

		mListView.setOnItemClickListener(new LeoAdapterViewOnItemClickListener() {

			@Override
			public void onSingleClick(AdapterView<?> adapteView, View view, int position, long id) {

				doOnItemClick(adapteView, view, position, id);
			}
		});
		return view;
	}

	private String getCategory(OffersCoupon offerscoupon) {
		String categroy = offerscoupon.getCategory();
		if (Constant.KEY_OFFER_COUPON_POINT.equals(categroy)) {
			categroy = Constant.KEY_OFFER_COUPON_POINT;
		} else if (Constant.KEY_OFFER_COUPON_CHANGE.equals(categroy)) {
			categroy = Constant.KEY_OFFER_COUPON_CHANGE;
		} else {
			categroy = Constant.KEY_OFFER_COUPON_NORMAL;
		}

		return categroy;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		checkDataType();

		loadFirst();
	}

	private void loadFirst() {
		mNowCount = 0;
		mActivity.showOverlayProgress();

		LeoListener afterListener = new LeoListener() {

			@Override
			public void onEnd() {

				// get coupon count
				OffersDataControll.getDataCount(dataType, getCategroyParameter(dataType), mActivity,
						new LeoDataCountListener() {

					@Override
					public void onRequstDataCountComplete(int count) {
						mMaxCount = count;

						// get coupon
						loadCouponsNormalStartIndex(0);// 初期化
						
						setReadForBadge();
					}
				});
			}

		};

		loadCategroy(afterListener);
	}

	private void loadCategroy(final LeoListener listener) {

		// get Category
		OffersDataControll.getData(Data_Type.Offer_Category_List, null, mActivity, new LeoDataListListener() {

			@SuppressWarnings("unchecked")
			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {

				if (status != Request_Status.Success) {

					mActivity.dismissOverlayProgress();

					return;
				}

				List<OffersCategory> couponCategores = (List<OffersCategory>) datas;

				StringBuilder normal = new StringBuilder();
				StringBuilder point = new StringBuilder();

				for (OffersCategory ca : couponCategores) {
					if (Constant.KEY_OFFER_COUPON_POINT.equals(ca.getName())
							|| Constant.KEY_OFFER_COUPON_CHANGE.equals(ca.getName())) {
						point.append(ConvertHelper.toString(ca.getId())).append(Constant.STRING_COMA);
					} else {
						normal.append(ConvertHelper.toString(ca.getId())).append(Constant.STRING_COMA);
					}
				}

				couponNormalCategroies = normal.deleteCharAt(normal.lastIndexOf(Constant.STRING_COMA)).toString();
				couponPointCategroies = point.deleteCharAt(point.lastIndexOf(Constant.STRING_COMA)).toString();

				if (listener != null) {
					listener.onEnd();
				}
			}
		});

	}

	private Bundle getCategroyParameter(Data_Type type) {
		Bundle params = new Bundle();

		switch (type) {
		case Offer_Coupon_List_Normal:
			params.putString(OffersDataControll.KEY_DATA_PARA_IDS_CATEGROY, couponNormalCategroies);
			break;
		case Offer_Coupon_List_Point:
			params.putString(OffersDataControll.KEY_DATA_PARA_IDS_CATEGROY, couponPointCategroies);
			break;
		default:
			break;
		}

		return params;
	}

	@Override
	protected void loadMore() {
		loadCouponsNormalStartIndex(mNowCount);
	}

	/**
	 * 配信しているクーポンリスト取得
	 * 
	 * @param なし
	 * @return なし
	 * @author 20140919 add by tll
	 */
	private void loadCouponsNormalStartIndex(int start) {

		Bundle params = getCategroyParameter(dataType);
		params.putString(OffersDataControll.KEY_DATA_PARA_PAGEING, String.valueOf(start));

		if (start == 0) {
			mListAdapter.clear();
		}

		OffersDataControll.getData(dataType, params, mActivity, new LeoDataListListener() {

			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {

				mActivity.dismissOverlayProgress();

				if (isLoadingMore) {
					afterLoadMore();
				}

				if (status == Request_Status.Success) {
					@SuppressWarnings("unchecked")
					List<OffersCoupon> items = (List<OffersCoupon>) datas;
					for (OffersCoupon item : items) {
						mListAdapter.add(item);
					}

					// ページング記録
					mNowCount += items.size();
					setRead(items);
				}
			}

		});

	}

	private void setRead(List<OffersCoupon> items) {
		for (OffersCoupon item : items) {
			item.setAlreadyRead();
		}
	}

	/**
	 * ・会員未登録の状態でポイント利用クーポンを既読状態にする 
	 * ・会員登録直後にはタブバーにNEWアイコンは表示されない
	 * ・会員登録後、新しくクーポンが配信されれるとタブバーにNEWアイコンが表示される
	 */
	private void setReadForBadge(){
		Integer couponCount = OffersKit.getInstance().unreadCouponsCount();
		if (couponCount == 0) {
			return;
		}

		boolean isLogined = KoseHelper.userIsLogined(mActivity.getApplicationContext());
		//会員未登録 + 通常クーポン　じゃない
		if ((dataType == Data_Type.Offer_Coupon_List_Normal && isLogined == false) == false) {
			return;
		}

		// point coupon all set read
		Bundle params = getCategroyParameter(Data_Type.Offer_Coupon_List_Point);
		OffersDataControll.getData(Data_Type.Offer_Coupon_List_Point, params, mActivity, new LeoDataListListener() {

			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {

				if (status == Request_Status.Success) {
					@SuppressWarnings("unchecked")
					List<OffersCoupon> items = (List<OffersCoupon>) datas;
					setRead(items);
				}
			}

		});
	}
	
	/**
	 * クーポンの画像設定
	 */
	private void setImageView(View v, String imageUrl) {

		ProgressBar imageProgressBar = (ProgressBar) v.findViewById(R.id.cell_coupon_image_loading);

		// edit by tll 20141001 全体画像ダウンロードcache修正
		ImageView mImageView = (ImageView) v.findViewById(R.id.cell_coupon_image);
		new ImageDownloadHepler(imageUrl, mImageView, imageProgressBar);
	}

	protected void doOnItemClick(AdapterView<?> adapteView, View view, int position, long id) {

		final OffersCoupon offerscoupon = (OffersCoupon) adapteView.getItemAtPosition(position);

		Bundle bd = new Bundle();
		bd.putInt(Constant.KEY_OFFER_COUNPON_DETAIL_ID, offerscoupon.getId());

		CouponDetailFragment couponfragment = new CouponDetailFragment();
		couponfragment.setArguments(bd);
		mFragmentManager.beginTransaction().add(R.id.container, couponfragment, offerscoupon.getTitle())
				.addToBackStack(null).commit();

	}
}
