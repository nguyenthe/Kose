package com.jillstuart.beauty.app.common.fragment;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.DemoActivity;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.adapter.DividerItemDecoration;
import com.jillstuart.beauty.app.adapter.Movie;
import com.jillstuart.beauty.app.adapter.MoviesAdapter;
import com.jillstuart.beauty.app.adapter.RecyclerTouchListener;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.helper.OpenHepler;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.squareup.picasso.Picasso;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class BaseWebViewNoToolBarFragment extends BaseFragment implements View.OnClickListener {
	protected WebView mWebView;
	protected boolean isBasic;
	protected WebViewClient mWebViewClient;
	protected LeoListener openOutSiteDissmissLisenter;
	String msg;
	private ViewFlipper mViewFlipper;
	private GestureDetector mGestureDetector;
	private  GestureDetector gesture;
	ImageView imageCancel;
	ImageView imageIconHide;
	TextView indicator_label;
	int total_child_view;
	private android.widget.LinearLayout.LayoutParams layoutParams;
	//static List<String> list_image  = new ArrayList<String>();
	static String url_picasso = null;
	final int width_image = 400;
	final int height_image = 300;

	//phan moi lam con sang
	private List<Movie> movieList = new ArrayList<>();
	private RecyclerView recyclerView;
	private ImageView imageView, imageReplace;
	private MoviesAdapter mAdapter;
	static boolean running = false;

	//demo static about viewflipper
        /*static int[] resources = {
                        R.drawable.imagea,
                        R.drawable.imageb,
                        R.drawable.imagec,
                        R.drawable.imaged
        };*/
	static int id_image        ;
	static boolean click_status = false;

	static Animation animation;

	public BaseWebViewNoToolBarFragment(){}

	public View onCreateView(LayoutInflater layoutinflater,
							 ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);

		final View view = layoutinflater.inflate(R.layout.webview_simple, viewgroup,
				false);
		getViewFromLayout(view);

                /*Demo_GestureDetector();                        // function to swipe imageview
                ActionFliper();
                mViewFlipper.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                System.out.println("Click on viewflipper ---");
                        }
                });
                view.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                                System.out.println("View touch ");
                                return gesture.onTouchEvent(event);
                        }
                });
                imageCancel.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                                System.out.println("Aaaaaaaaaa Click ");
                                imageCancel.setVisibility(View.GONE);
                                indicator_label.setVisibility(View.GONE);
                                id_image = mViewFlipper.getDisplayedChild();
                                //enabled status when clicked ...
                                click_status = true;
                                //save status into SharedPreferences ...!!!
                                *//*SharedPreferences myPrefs = getContext().getSharedPreferences("myPrefs", getContext().MODE_WORLD_READABLE);
                                SharedPreferences.Editor editor = myPrefs.edit();*//*
                                SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getContext());
                                SharedPreferences.Editor editor=saved_values.edit();
                                editor.putInt("MEM1", id_image);
                                editor.putBoolean("MEM2", click_status);
                                editor.commit();
                                System.out.println("ID ID ID IMAGE CURRENT : " + id_image);
                                //------------------------------------- CONFUSE ------------------------------
                                //confuse  ------------------------------------------------------
                                handlerDropImage();
                                return true;
                        }
                });*/

		//phan lam con sang
		recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

		imageView = (ImageView)view.findViewById(R.id.cancel);
		imageView.setOnClickListener(this);

		mAdapter = new MoviesAdapter(movieList,getContext());
		recyclerView.setHasFixedSize(true);
		//RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
		LinearLayoutManager layoutManager
				= new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setAdapter(mAdapter);

		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {
				Movie movie = movieList.get(position);
				Toast.makeText(getContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
				if(running == true){
					imageView.setVisibility(View.VISIBLE);
					recyclerView.clearAnimation();
				}else{
					Intent intent = new Intent(getContext(),DemoActivity.class);
					startActivity(intent);
				}
			}

			@Override
			public void onLongClick(View view, int position) {

			}

			@Override
			public void onTouch(View view, int position) {

			}
		}));
		prepareMovieData();
		setWebViewOption();
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
                /*LocalBroadcastManager.getInstance(getContext()).registerReceiver((mMessageReceiver),
                                new IntentFilter("MyData")
                );*/
	}

	//phan lam con sang

	public void handlerDropImage1(){
		animation = AnimationUtils.loadAnimation(getContext(),
				R.anim.slide_down);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				System.out.println("Bat dau animation ...");
			}
			@Override
			public void onAnimationEnd(final Animation animation) {
				System.out.println("Ket Thuc !!!! Update Imageview within new position !!!! ");
				imageView.setVisibility(View.GONE);
				running = true;
				recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
					@Override
					public void onClick(View view, int position) {
						Movie movie = movieList.get(position);
						Toast.makeText(getContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
						imageView.setVisibility(View.VISIBLE);
						recyclerView.clearAnimation();
						running = false;
					}
					@Override
					public void onLongClick(View view, int position) {

					}

					@Override
					public void onTouch(View view, int position) {

					}
				}));

			}
			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		animation.setFillAfter(true);
		animation.setFillEnabled(true);
		recyclerView.startAnimation(animation);

	}

	public void choichoi(){
		recyclerView.clearAnimation();
	}

	private void prepareMovieData() {
		Movie movie = new Movie("", "http://www.9play.org/wp-content/uploads/2017/03/hoa-anh-dao.png");
		movieList.add(movie);

		movie = new Movie("", "http://wapvip.pro/chuyen-muc/nhacchuonghinhnen/Hinh-Nen-Hoa-Hong/WapVip.Pro-Hinh-Nen-Hoa-Hong-161.jpg");
		movieList.add(movie);

		movie = new Movie("", "http://thanhnienmoi.com/thumb/128x128/2/upload/product/chuong-trinh-tour-du-lich-nhat-ban-ngam-hoa-anh-dao31132.jpg");
		movieList.add(movie);

		movie = new Movie("", "http://thuvienanhdep.net/wp-content/uploads/2016/03/hinh-anh-lang-hoa-dep-nhat-chuc-mung-sinh-nhat-khai-truong-thanh-lap7.jpg");
		movieList.add(movie);

		mAdapter.notifyDataSetChanged();
	}
	//end

        /*private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, Intent intent) {
                        ArrayList<String> myList = (ArrayList<String>) intent.getSerializableExtra("image");
                        System.out.println("Size Image list : " + myList.size());
                        //check size of list_image
                        if (myList.size() == 0) {
                                imageCancel.setVisibility(View.GONE);
                                imageIconHide.setVisibility(View.GONE);
                        } else {
                                imageCancel.setVisibility(View.VISIBLE);
                                mViewFlipper.setVisibility(View.VISIBLE);
                                for (int i = 0; i < myList.size(); i++) {
                                        final ImageView imageView = new ImageView(getActivity());
                                        Picasso
                                                        .with(getContext())
                                                        .load(myList.get(i))
                                                        .transform(new RoundedTransformation())
                                                        .resize(width_image,height_image)
                                                        .into(imageView)
                                        ;
                                        imageView.setOnClickListener((View.OnClickListener)getContext());
                                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                        mViewFlipper.addView(imageView);
                                        //ActionFliper();
                                        //mViewFlipper.showNext();
                                }

                        }
                        *//*SharedPreferences myPrefs = getContext().getSharedPreferences("myPrefs", getContext().MODE_WORLD_READABLE);
                        SharedPreferences.Editor editor = myPrefs.edit();*//*
                        SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = saved_values.edit();
                        editor.putInt("array_size", myList.size());
                        for (int i = 0; i < myList.size(); i++)
                                editor.putString("array_" + i, myList.get(i));
                        editor.commit();
                }
        };*/

	public void getViewFromLayout(View view){
		mWebView = (WebView) view.findViewById(R.id.webview_simple);
		// Get the ViewFlipper
                /*mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);
                //Textview for show indicator slide image
                indicator_label = (TextView)view.findViewById(R.id.indicator_label);
                //ImageView use to drop coupon
                imageCancel = (ImageView)view.findViewById(R.id.iconcancel);
                imageIconHide = (ImageView)view.findViewById(R.id.iconhide);*/
	}



        /*protected void Demo_GestureDetector(){
                total_child_view = mViewFlipper.getChildCount();
                gesture = new GestureDetector(getActivity(),
                                new GestureDetector.SimpleOnGestureListener() {
                                        @Override
                                        public boolean onDown(MotionEvent e) {
                                                return true;
                                        }
                                        @Override
                                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                                                                   float velocityY) {
                                                // Swipe left (next)
                                                if (e1.getX() > e2.getX()) {
                                                        mViewFlipper.setInAnimation(getContext(), R.anim.left_in);
                                                        mViewFlipper.setOutAnimation(getContext(), R.anim.left_out);
                                                        indicator_label.setText(mViewFlipper.getDisplayedChild() + 1 + "/" + total_child_view);
                                                        mViewFlipper.showNext();
                                                }
                                                // Swipe right (previous)
                                                if (e1.getX() < e2.getX()) {
                                                        mViewFlipper.setInAnimation(getContext(), R.anim.right_in);
                                                        mViewFlipper.setOutAnimation(getContext(), R.anim.right_out);
                                                        indicator_label.setText(mViewFlipper.getDisplayedChild() + 1 + "/" + total_child_view);
                                                        mViewFlipper.showPrevious();
                                                }

                                                return super.onFling(e1, e2, velocityX, velocityY);
                                        }
                                });
        }


        protected void ActionFliper(){
                mViewFlipper.setInAnimation(getActivity(), android.R.anim.fade_in);
                mViewFlipper.setOutAnimation(getActivity(), android.R.anim.fade_out);
                mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                                return gesture.onTouchEvent(event);
                        }
                });
        }*/

        /*public void handlerDropImage(){
                Animation animation = AnimationUtils.loadAnimation(getContext(),
                                R.anim.slide_down_demo);
                animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                                System.out.println("Bat dau animation ...");
                        }

                        @Override
                        public void onAnimationEnd(final Animation animation) {
                                System.out.println("Ket Thuc !!!! Update Imageview within new position !!!! ");
                                imageCancel.setVisibility(View.GONE);
                                indicator_label.setVisibility(View.GONE);
                                mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
                                        @Override
                                        public boolean onTouch(View v, MotionEvent event) {
                                                System.out.println("Aaaaaaaaaaaa112344");
                                                imageCancel.setVisibility(View.VISIBLE);
                                                indicator_label.setVisibility(View.GONE);
                                                mViewFlipper.getCurrentView().clearAnimation();
                                                return gesture.onTouchEvent(event);
                                        }
                                });

                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                });
                animation.setFillAfter(true);
                animation.setFillEnabled(true);
                mViewFlipper.getCurrentView().startAnimation(animation);

        }*/

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		System.out.println("ON SAVEINSTANCE RECEIVED ID : " + id_image);
		outState.putInt("VAR1", id_image);
		outState.putBoolean("VAR2",click_status);
	}


	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if(savedInstanceState != null){

			System.out.println("SaveInstance != null");

		}else{
			//animation.setFillAfter(true);
			//handlerDropImage1();
			//choichoi();
		}

                /*if (savedInstanceState != null) {
                        System.out.println("VVVVVVVVVVV " + savedInstanceState.getInt("VAR1"));
                }else {
                        System.out.println("ViewCreated Notworking");
                        *//*final SharedPreferences myPrefs = getContext().getSharedPreferences("myPrefs", getContext().MODE_WORLD_READABLE);*//*
                        final SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                        int size = myPrefs.getInt("array_size", 0);
                        for(int i=0; i<size; i++) {
                                myPrefs.getString("array_" + i, null);
                                System.out.println("Array Image Url thu : " + i + "--" + myPrefs.getString("array_" + i, null));
                        }

                        mViewFlipper.setVisibility(View.VISIBLE);
                        indicator_label.setVisibility(View.GONE);
                        imageIconHide.setVisibility(View.GONE);

                        final int username = myPrefs.getInt("MEM1",0);
                        boolean click_status_2 = myPrefs.getBoolean("MEM2",false);
                        final int total = myPrefs.getInt("MEM3",0);
                        System.out.println("IIIIIIIMMMMM " + username);
                        System.out.println("Click Status " + click_status_2);

                        if(click_status_2 == true){
                                System.out.println("Already Click !!!!");
                                System.out.println("ID Image : " + username);
                                imageCancel.setVisibility(View.GONE);
                                indicator_label.setVisibility(View.GONE);
                                mViewFlipper.setVisibility(View.GONE);
                                imageIconHide.setVisibility(View.VISIBLE);
                                //imageIconHide.setImageResource(resources[username]);
                                Picasso
                                                .with(getContext())
                                                .load(myPrefs.getString("array_" + username, null))
                                                .into(imageIconHide);

                                imageIconHide.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                                Toast.makeText(getContext(),"Image Hide Click",Toast.LENGTH_LONG).show();
                                                mViewFlipper.setVisibility(View.VISIBLE);
                                                System.out.println("Hien Lai Hinh Anh : " + username);
                                                for (int i = 0; i < myPrefs.getInt("array_size", 0); i++) {
                                                        final ImageView imageView = new ImageView(getActivity());
                                                        Picasso
                                                                        .with(getContext())
                                                                        .load(myPrefs.getString("array_" + i, null))
                                                                        .transform(new RoundedTransformation())
                                                                        .resize(width_image,height_image)
                                                                        .into(imageView);
                                                        imageView.setOnClickListener(this);
                                                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                                        mViewFlipper.addView(imageView);
                                                        mViewFlipper.setDisplayedChild(username);
                                                        //ActionFliper();

                                                }
                                                imageCancel.setVisibility(View.VISIBLE);
                                                indicator_label.setVisibility(View.GONE);
                                                imageIconHide.setVisibility(View.GONE);
                                                click_status = false;
                                                SharedPreferences.Editor editor = myPrefs.edit();
                                                editor.putInt("MEM1",username);
                                                editor.putBoolean("MEM2",click_status);
                                                editor.commit();
                                        }
                                });
                        }else{
                                System.out.println("Did not Click !!!!");
                                mViewFlipper.setVisibility(View.VISIBLE);
                                for (int i = 0; i < size; i++) {
                                        final ImageView imageView = new ImageView(getActivity());
                                        Picasso
                                                        .with(getContext())
                                                        .load(myPrefs.getString("array_" + i, null))
                                                        .transform(new RoundedTransformation())
                                                        .resize(width_image,height_image)
                                                        .into(imageView);
                                        imageView.setOnClickListener(this);
                                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                        mViewFlipper.addView(imageView);

                                        //ActionFliper();
                                        //mViewFlipper.showNext();
                                }
                                if(size == 0){
                                        indicator_label.setVisibility(View.GONE);
                                        imageCancel.setVisibility(View.GONE);
                                }else {
                                        mViewFlipper.setDisplayedChild(username);
                                        indicator_label.setVisibility(View.GONE);
                                        indicator_label.setText(username + "/" + total);
                                        imageIconHide.setVisibility(View.GONE);
                                }
                        }

                }*/

	}

	private void setWebViewOption() {
		super.setWebViewSetting(mWebView);

		if (isBasic) {
			setWebViewClientBasic();
		} else {
			setWebViewClient();
		}

		mWebView.setWebViewClient(mWebViewClient);

		setKeyListener();

	}

	private void setKeyListener() {

		mWebView.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {

				//edit by tll 20150901
				if (event.getAction() == KeyEvent.ACTION_UP
						&& event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

					if (mWebView.canGoBack()) {
						mWebView.goBack();
						return true;
					}else {
						return false;
					}
				}

				return false;
			}
		});
	}

	private void setWebViewClient() {
		mWebViewClient = new ToolBarWebViewClient();
	}

	private void setWebViewClientBasic() {
		mWebViewClient = new ToolBarWebViewClient() {
			@Override
			public void onReceivedHttpAuthRequest(WebView view,
												  HttpAuthHandler handler, String host, String realm) {

				loadBasic(handler);
			}
		};
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.cancel:
				handlerDropImage1();
				break;
		}
	}

	private class ToolBarWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			mActivity.showOverlayProgress();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			mActivity.dismissOverlayProgress();

			if (!mWebView.getSettings().getLoadsImagesAutomatically()) {
				mWebView.getSettings().setLoadsImagesAutomatically(true);
			}
		};

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			System.out.println("URl URL " + url);
			// detect url of webview
			if(url.equals("http://www.jillstuart-beauty.com/ja-jp/app/howto/")){
				Toast.makeText(getContext(),"hello webview",Toast.LENGTH_LONG).show();
				//catch URL in WEBVIEW
                                /*Intent intent = new Intent(getActivity(), NewIntentDemo.class);
                                startActivity(intent);*/
			}
			if (KoseHelper.isCanAccessUrlAtApp(url) == false) {
				System.out.println("Click vo webview ssdasdadsad");
				OpenHepler.openOutSideWebViewConfirm(mActivity, null, url,openOutSiteDissmissLisenter);
				return true;
			}

			if (isBasic) {
				if (KoseHelper.isNeedAddParameterForWebViewLoadUrl(url,
						mActivity)) {
					System.out.println("Need Add Parammerterrrrr   ");
					String newUrl = KoseHelper
							.addParameterForWebViewLoadUrl(url);
					loadUrl(newUrl);
					return true;
				}
			}

			return false;
		}
	}

	private void loadBasic(HttpAuthHandler handler){
//    if (mWebView != null && mActivity.isConnectedOrConnecting()) {
		handler.proceed(UrlConstant.APP_BASIC_ID,
				UrlConstant.APP_BASIC_PW);
//    }else {
//       mActivity.showErrorNoNetworkConnection();
//    }
	}
	protected void loadUrl(String url) {
		if (mWebView != null && mActivity.isConnectedOrConnecting()) {
			mWebView.loadUrl(url);
		} else {
			mActivity.showErrorNoNetworkConnection();
		}
	}

}
