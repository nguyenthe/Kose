package com.jillstuart.beauty.app.common.listener;

import android.view.View;
import android.widget.AdapterView;

import com.jillstuart.beauty.app.common.Constant;

/**
 *Single,Double Click
 *
 * history:create by tll 20140409
 **/
public abstract class LeoAdapterViewOnItemClickListener implements
		AdapterView.OnItemClickListener {

	long lastClickTime = 0;

	@Override
	public void onItemClick(AdapterView<?> adapteView, View view, int position,
			long id) {
		long clickTime = System.currentTimeMillis();

		// double click
		if (clickTime - lastClickTime > Constant.TIME_DOUBLE_CLICK_DELTA_MSC) {
			// single click
			onSingleClick(adapteView, view, position, id);

		} else {

		}
		lastClickTime = clickTime;

	}

	public abstract void onSingleClick(AdapterView<?> adapteView, View view,
			int position, long id);
}
