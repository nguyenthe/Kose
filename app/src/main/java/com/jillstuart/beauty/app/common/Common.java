package com.jillstuart.beauty.app.common;

//import com.google.analytics.tracking.android.Fields;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.application.MainApplication;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class Common {

	/**
	 * 
	 * @param activity
	 * @return
	 */
	public static int density(Activity activity) {
		return (int) (double) (activity.getResources().getDisplayMetrics().density);
	}

	/**
	 * スクリーンサイズ
	 * 
	 * @param activity
	 * @return
	 */
	public static Point getDisplaySize(Activity activity) {

		Display display = ((WindowManager) activity
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		@SuppressWarnings("deprecation")
		Point size = new Point(display.getWidth(), display.getHeight());

		return size;
	}

	/**
	 * アプリ表示サイズ( -status bar - tabbar - action bar)
	 * 
	 * @param activity
	 * @param context
	 * @return
	 */
	public static Point getCanUseDisplaySize(Activity activity, Context context) {
		// whole screen
		Point size = getDisplaySize(activity);

		// status bar height
		int statusbarHeight = 0;
		Resources resources = context.getResources();
		int resourceId = resources.getIdentifier("navigation_bar_height",
				"dimen", "android");
		if (resourceId > 0) {
			statusbarHeight = resources.getDimensionPixelSize(resourceId);
			statusbarHeight = statusbarHeight / density(activity);
		}

		int tabHeight = ((MainActivity) activity).getTabHostHeight();
		int actionbarHeight = ((MainActivity) activity).getActionBarHeight();

		int height = size.y - statusbarHeight - tabHeight - actionbarHeight;

		return new Point(size.x, height);
	}

	/**
	 * Google Analytics
	 * 
	 * @param activity
	 * @param appScreen
	 * @return
	 */
	public static boolean sendGoogleAnalyticsScreenName(Activity activity,
			String appScreen) {

		final Tracker tracker = ((MainApplication) activity.getApplication())
				.getTracker();

		if (tracker != null) {

			tracker.setScreenName(appScreen);
			tracker.send(new HitBuilders.ScreenViewBuilder().build());

			return true;
		}

		return false;
	}

	/**
	 * Font Custom
	 * 
	 * @param activity
	 * @param txtView
	 */
	public static void setCustomFont(Activity activity, TextView txtView) {
		Typeface font = Typeface.createFromAsset(activity.getAssets(),
				Constant.SYS_FONT_CUSTOM_FILE);
		txtView.setTypeface(font);

	}

	// キーボード
	public static void hiddenShowKeyBoard(Activity activity, View v,
			Boolean isShow) {
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (isShow) {
			imm.showSoftInput(v, 0);
		} else {
			imm.hideSoftInputFromWindow(v.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
}
