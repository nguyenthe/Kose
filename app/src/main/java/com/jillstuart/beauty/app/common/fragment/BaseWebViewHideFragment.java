package com.jillstuart.beauty.app.common.fragment;

import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.animation.LeonisHightChangeAnimation;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.common.view.LeonisHideWebView;
import com.jillstuart.beauty.app.common.view.LeonisHideWebView.OnScrollChangedCallback;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("SetJavaScriptEnabled")
public class BaseWebViewHideFragment extends BaseWebViewFragment {

	private boolean mExpand;

	private float mHideMinHeight = 180;
	private View mTabHost;
	private int mTabHostHight = 0;

	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.isHasBrowseBar = true;

		mExpand = false;

		mActivity.backListener = new LeoListener() {

			@Override
			public void onEnd() {
				collapse();
			}
		};
		return super.onCreateView(layoutinflater, viewgroup, bundle);
	}

	public boolean onBackKeyEvent() {
		if (canGoBack()) {
			goBack();
		} else {
			collapse();
			mFragmentManager.popBackStack();
		}
		return true;
	}

	protected void expand() {

		if (mExpand) {
			return;
		}
		LeonisHightChangeAnimation animTab = new LeonisHightChangeAnimation(mTabHost, 0, true);
		animTab.setDuration(Constant.TIME_WEBVIEW_HIDE_TABBAR_MSC);
		mTabHost.startAnimation(animTab);
		mExpand = true;
	}

	protected void collapse() {
		if (mExpand == false) {
			return;
		}

		LeonisHightChangeAnimation animTab = new LeonisHightChangeAnimation(mTabHost, mTabHostHight, true);
		animTab.setDuration(Constant.TIME_WEBVIEW_HIDE_TABBAR_MSC);
		mTabHost.startAnimation(animTab);
		mExpand = false;
	}

	public boolean dispatchKeyEvent(KeyEvent e) {

		if (KeyEvent.ACTION_DOWN == e.getAction() && KeyEvent.KEYCODE_BACK == e.getKeyCode()) {

			if (canGoBack()) {
				goBack();
				return true;
			}
		}

		return false;
	}

	@SuppressLint("ClickableViewAccessibility")
	private void setToolBarMove() {

		((LeonisHideWebView) mWebView).setOnScrollChangedCallback(new OnScrollChangedCallback() {

			public void onScroll(final int l, final int t, final int oldl, final int oldt) {

				boolean isDown = false;

				// 移動距離が足りない
				if (t > 0 && t < mHideMinHeight) {
					return;
				}

				// topになっている
				if (t == 0) {
					isDown = false;
				} else if (t - oldt > 0) {
					isDown = true;
				}

				if (isDown) {
					expand();
				} else {
					collapse();
				}

			}
		});

	}

	@Override
	protected void setWebView() {

		mTabHost = mActivity.findViewById(android.R.id.tabhost);
		mTabHostHight = ((MainActivity) mActivity).getTabHostHeight();

		setToolBarMove();

	}

}
