package com.jillstuart.beauty.app.common.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;

import com.jillstuart.beauty.app.common.Constant;

@SuppressLint("SimpleDateFormat")
public class DateHelper {
	public static String OFFER_DATE_FORMART = "yyyyMMddHHmmss";

	@SuppressLint("SimpleDateFormat")
	public static String dateFormatOfferDateStringToFormatString(
			String dateString, String formatString) {
		if (dateString == null) {
			return Constant.STRING_EMPTY;
		}
		if (formatString == null) {
			formatString = "yyyy.MM.dd";
		}

		Date date = dateFormatStringToDate(dateString);
		if (date == null) {
			return Constant.STRING_EMPTY;
		}

		return dateFormatDateToString(dateFormatStringToDate(dateString), formatString);

	}

	public static String dateDayOfWeek(String dateString) {

		Date date = dateFormatStringToDate(dateString);
		if (date == null) {
			return Constant.STRING_EMPTY;
		}

		String finalDay = new SimpleDateFormat("EEEE").format(date);
		return finalDay;

	}

	public static Date dateFormatStringToDate(String dateString) {
		try {
			Date date = (Date) new SimpleDateFormat(OFFER_DATE_FORMART)
					.parse(dateString);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String dateFormatDateToString(Date date, String format) {
		if (format == null) {
			format = OFFER_DATE_FORMART;
		}

		return new SimpleDateFormat(format).format(date);
	}

	public static String dateStringNowOfferFormat() {
		Date nowDate = Calendar.getInstance().getTime();

		return dateFormatDateToString(nowDate, OFFER_DATE_FORMART);
	}
}
