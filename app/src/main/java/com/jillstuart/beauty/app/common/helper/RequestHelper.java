package com.jillstuart.beauty.app.common.helper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.os.Handler;
import android.os.Looper;

import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.listener.LeoDataListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIJsonListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;

public class RequestHelper {

	private final static Handler mHandler = new Handler(Looper.getMainLooper());

	private final static String CODE_TYPE = "UTF-8";
	private final static String REQUEST_TYPE_POST = "POST";

	public static void requestPost(Data_Type type, HashMap<String, String> parameter, BaseActionBarActivity activity,
			final LeoDataAPIJsonListener afterListener) {

		String urlBase = JillApiConstant.API_URL_BASE + KoseHelper.getProgramId(activity.getApplicationContext()) + "%1$s";
		String urlString = null;

		if (parameter == null) {
			parameter = new HashMap<String, String>();
		}

		switch (type) {
		case API_Post_Regist_Ec:
		case API_Post_Regist_Card:
			urlString = String.format("%1$s%2$s", JillApiConstant.API_URL_BASE, JillApiConstant.API_URL_LOGIN);
			break;
		case API_Post_Add_Ec:
		case API_Post_Add_Card:
			urlString = String.format(urlBase, JillApiConstant.API_URL_ADD_EC_OR_CARD);
			break;
		case API_Post_Recovery_Edit:
			urlString = String.format(urlBase, JillApiConstant.API_URL_RECOVRY_EDIT);
			break;
		case API_Post_Recovery:
			urlString = JillApiConstant.API_URL_BASE + parameter.get(JillApiConstant.API_KEY_IN_PROGRAM_ID)
					+ JillApiConstant.API_URL_RECOVERY;
			parameter.remove(JillApiConstant.API_KEY_IN_PROGRAM_ID);
			break;
		case API_Post_ChangePoint:
			urlString = String.format(urlBase, JillApiConstant.API_URL_CHANGE_POINT);
			break;
		case API_Get_User_Point:
			urlString = String.format(urlBase, JillApiConstant.API_URL_GET_POINT);
			break;
		default:
			break;
		}

		parameter.put(JillApiConstant.API_KEY_IN_ACKEY, JillApiConstant.API_URL_KEY);

		requestApi(urlString, toJsonObject(type, parameter), afterListener);

	}

	private static JSONObject toJsonObject(Data_Type type, HashMap<String, String> parameter) {

		// 通常
		if ((type == Data_Type.API_Post_Add_Card || type == Data_Type.API_Post_Regist_Card) == false) {
			return new JSONObject(parameter);
		}

		// cards 特別処理
		JSONObject json = null;
		try {
			final String cardNoString = parameter.get(JillApiConstant.API_KEY_IN_CARD);
			parameter.remove(JillApiConstant.API_KEY_IN_CARD);

			json = new JSONObject(parameter);

			JSONArray cardNoArray = new JSONArray();
			cardNoArray.put(cardNoString);

			json.put(JillApiConstant.API_KEY_IN_CARD, cardNoArray);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Post Request
	 * 
	 * @param urlString
	 * @param parameter
	 * @param afterListener
	 */
	private static void requestApi(final String urlString, final JSONObject parameters,
			final LeoDataAPIJsonListener afterListener) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				HttpURLConnection conn = null;
				InputStreamReader is = null;
				OutputStream os = null;
				try {
					// constants
					URL url = new URL(urlString);

					conn = (HttpURLConnection) url.openConnection();

					conn.setReadTimeout(JillApiConstant.API_READ_TIME_OUT_MILLI);/* milliseconds */
					conn.setConnectTimeout(JillApiConstant.API_CONNECT_TIME_OUT_MILLI);/* milliseconds */

					conn.setDoInput(true);
					conn.setDoOutput(true);

					// make HTTP header
					conn.setRequestMethod(REQUEST_TYPE_POST);
					conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
					conn.setRequestProperty("Accept", "application/json");

					// open
					conn.connect();

					// post
					os = conn.getOutputStream();
					os.write(parameters.toString().getBytes(CODE_TYPE));
					os.flush();

					checkAPIResponse(conn, afterListener);

				} catch (Exception e) {
					e.printStackTrace();
					requestError(afterListener);
				}

				finally {
					// clean up
					try {
						if (os != null) {
							os.close();
						}
						if (is != null) {
							is.close();
						}
						if (conn != null) {
							conn.disconnect();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}).start();

	}

	
	
	public static void downLoad(final String urlString,final LeoDataSingleListener complate){
		new Thread(new Runnable() {

			@Override
			public void run() {
				
				
				 
				   
//				HttpClient httpClient = null;
//				HttpResponse httpResponse = null;
				InputStream inputStream = null;
				HttpURLConnection urlConnection = null;
				try {

//					httpClient = new DefaultHttpClient();
//
//					HttpGet httpGet = new HttpGet(urlString);
//					httpResponse = httpClient.execute(httpGet);
//					HttpEntity httpEntity = httpResponse.getEntity();

					// Read content
//					inputStream = httpEntity.getContent();

					//connect
					URL url = new URL(urlString);
					urlConnection = (HttpURLConnection) url.openConnection();
					
					// Read content
					inputStream = new BufferedInputStream(urlConnection.getInputStream());
					BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, CODE_TYPE));
					StringBuilder sBuilder = new StringBuilder();

					String line = null;
					while ((line = bReader.readLine()) != null) {
						sBuilder.append(line);
					}

					inputStream.close();
					final String result = sBuilder.toString();
					
					mHandler.post(new Runnable() {
						public void run() {
							complate.onRequstDataComplete(Request_Status.Success,result);
						}
					});
					

				} catch (Exception e) {
					e.printStackTrace();
					
					requestError(complate);
				}

				finally {
					// clean up
					try {
						if (inputStream != null) {
							inputStream.close();
						}
						if (urlConnection != null) {
							urlConnection.disconnect();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}).start();
	}
	
	public static void requestJson(final String urlString, final LeoDataAPIJsonListener afterListener) {
		
		downLoad(urlString, new LeoDataSingleListener() {
			
			@Override
			public void onRequstDataComplete(Request_Status status, Object data) {
				
				try {
					
					if (Request_Status.Failure == status) {
						requestError(afterListener);
						
						return;
					}
					
					final JSONObject json = new JSONObject(new JSONTokener((String)data));
					mHandler.post(new Runnable() {
						public void run() {
							afterListener.onRequstDataComplete(json);
						}
					});
					
				} catch (JSONException e) {
					e.printStackTrace();
					requestError(afterListener);
				}
				
			}
		});
		
	}
	
	private static void checkAPIResponse(HttpURLConnection conn, final LeoDataAPIJsonListener afterListener)
			throws Exception {

		if (afterListener == null) {
			return;
		}

		// return
		// failure
		int HttpResult = conn.getResponseCode();
		if (HttpResult != HttpURLConnection.HTTP_OK) {
			requestError(afterListener);
			return;
		}

		// success
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), CODE_TYPE));
		String line = null;
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}

		br.close();

		final JSONObject json = new JSONObject(new JSONTokener(sb.toString()));

		mHandler.post(new Runnable() {
			public void run() {
				afterListener.onRequstDataComplete(json);
			}
		});

	}

	
	private static void requestError(final LeoDataListener afterListener){
		if (afterListener == null) {
			return;
		}
		
		mHandler.post(new Runnable() {
			public void run() {
				if (afterListener instanceof LeoDataAPIJsonListener) {
					((LeoDataAPIJsonListener)afterListener).onRequstDataComplete(new JSONObject());
				}else if (afterListener instanceof LeoDataSingleListener) {
					((LeoDataSingleListener) afterListener).onRequstDataComplete(Request_Status.Failure, Constant.STRING_EMPTY);
				}
			}
		});
	}
}
