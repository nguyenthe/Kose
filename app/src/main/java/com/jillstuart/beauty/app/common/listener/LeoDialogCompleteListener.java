package com.jillstuart.beauty.app.common.listener;

public interface LeoDialogCompleteListener {
	/**
	 * Listener
	 */

	// 消える
	public interface LeoDialogDismissListener extends
			LeoDialogCompleteListener {
		void OnDismiss();
	}

	// 確認
	public interface LeoDialogConfirmListener extends
			LeoDialogCompleteListener {
		void OnCancelClick();

		void onOkClick();
	}

	// edit text input
	public interface LeoDialogTextInputCompleteListener extends
			LeoDialogCompleteListener {
		void OnDialogTextInputComplete(String text);
	}

	// List select
	public interface LeoDialogListSelectCompleteListener extends
			LeoDialogCompleteListener {
		void OnDialogListSelectComplete(int index);
	}
}
