package com.jillstuart.beauty.app.common;


public class Constant {
	//Tag
	public static final String TAG_TAB_BAG = "tab_indicator_badge_text";
	public static final String TAG_TAB_HOME = "home";
	public static final String TAG_TAB_SHOPLIST = "shoplist";
	public static final String TAG_TAB_NEWS = "news";
	public static final String TAG_TAB_COUPON = "coupon";
	public static final String TAG_TAB_MYPAGE = "mypage";

	public static final String TAG_DIALOG_CONFRIM = "dialog_confirm";
	public static final String TAG_DIALOG_CONFRIM_VIEW = "dialog_viewConfirm";
	public static final String TAG_DIALOG_ALERT = "dialog_alert";
	
	public static final int TAG_COUPON_STAMP_VIEW = 88;
	public static final int TAG_COUPON_NUMBER_INPUT_VIEW = 77;

	//index
	public static final int TAB_INDEX_COUPON = 3;
	public static final int TAB_INDEX_RECOM = 2;
	
	//KEY
	//tab
	public static final String KEY_DATA_VALUE = "key_data_value";
	public static final String KEY_DATA_KEY = "key_data_key";
	
	public static final String KEY_TAB_NAME = "key_tab_name";
	//fragment
	public static final String KEY_FG_CLASS_NAME = "fragment";
	public static final String KEY_FG_TITLE = "title";
	public static final String KEY_FG_ICON = "icon";
	public static final String KEY_FG_TYPE = "type";
	public static final String KEY_FG_URL = "url";
	public static final String KEY_FG_ID = "id";
	public static final String KEY_FG_DATA = "data";
	
	//offer
	public static final String KEY_OFFER_GROUP_COUNT = "offer_group_count";
	public static final String KEY_OFFER_CAMPAIN_LIST = "offer_campain_list";
	public static final String KEY_OFFER_COUNPON_DETAIL_ID = "coupon_detail_id";
	public static final String SHOW_OFFER_BADGE_TEXT = "new";
	
	//ポイントクーポン
	public static final String KEY_OFFER_COUPON_POINT = "Happy Event Coupon";
	public static final String KEY_OFFER_COUPON_CHANGE = "Jewel Point Program Coupon";
	public static final String KEY_OFFER_COUPON_NORMAL = "Coupon";
	
	public static final String KEY_OFFER_COUPON_POINT_PREFIX_PLUS = "+";
	public static final String KEY_OFFER_COUPON_POINT_PREFIX_MINUS = "-";

	//そのた
	public static final String KEY_PAGE_LOAD_DATA_FLG = "page_load_data_flag";

	//マイページ
	public static final String KEY_MY_PAGE_EVENT_SCAN = "my_page_event_scan";
	public static final String KEY_MY_PAGE_EVENT_LOGIN = "my_page_event_login";
	
	//ローカル保存
	public static final String KEY_LOCAL_SAVE = "my_page_locak_save";
	public static final String KEY_LOCAL_SAVE_OFFER_LIST_IS_POINT_COUPON = "loacl_save_offer_list_is_point_coupon";
	public static final String KEY_LOCAL_SAVE_IS_CHECKED_PERMISSON = "loacl_save_is_checked_permission";
	
	//FLAG
	public static int FLG_ACTIVITY_REQUEST_OTHER_LEFT = 9;
	public static int FLG_ACTIVITY_REQUEST_SCAN = 6;
	
	//TIME
	public static final int TIME_DOUBLE_CLICK_DELTA_MSC = 600;// milliseconds
	public static final int TIME_WEBVIEW_HIDE_TABBAR_MSC = 500;
//	public static final int TIME_REFRESH_OFFER_MSC = 10*1000;
//	public static final int TIME_REFRESH_MYPAGE_MSC = 10*1000;
	
	//--文字列
	public static final String STRING_EMPTY = "";
	public static final String STRING_ENTER = "\n";
	public static final String STRING_COMA = ",";
	
	//--数字
	public static final int UNSELECT_INDEX = -1;
	public static final int NO_SET = -1;
	
	public static final float BASE_SCREEN_WIDTH = 720;
	public static final float BASE_SCREEN_DENSITY = 320;
	
	public static final String SYS_FONT_CUSTOM_FILE = "font/Montserrat-Light.otf";
	
	//スキャンコード
	public static final String MYPAGE_SCAN_CODE = ("EAN_13");//複数の時：("code1","code2")
	
	//INPUT 長さ
	public static final int INPUT_MAX_LENGTH_NUMBER = 20;
	public static final int INPUT_MAX_LENGTH_STRING = 40;
	public static final int INPUT_MAX_LENGTH_MAIL = 255;
	public static final int MYPAGE_EC_USER_ID_LENGTH = 13;
	public static final int MYPAGE_DATE_INPUT_MAX_LENGTH = 2;
	
	public static final int MYPAGE_CARD_SHOW_MAX = 10;
	
}
