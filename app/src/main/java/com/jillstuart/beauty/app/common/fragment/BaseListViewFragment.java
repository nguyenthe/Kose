package com.jillstuart.beauty.app.common.fragment;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Java_Data_Type;
import com.jillstuart.beauty.app.common.helper.AnimationHelper;
import com.jillstuart.beauty.app.common.helper.DataSaveHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

public class BaseListViewFragment extends BaseFragment {

	protected ListView mListView;
	protected boolean isLoadingMore;
	protected int mMaxCount;
	protected int mNowCount;

	private int currentScrollState;
	private int currentVisibleItemCount;

	private View mFooterView;
	private View mProgressBarLoadMore;

	protected long lastTime = -1;

	protected Data_Type dataType;
//	public boolean isPointCoupon;

	protected void setListViewPaging() {

		addFooter();

		mListView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				currentScrollState = scrollState;
				isScrollCompleted();
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// currentFirstVisibleItem = firstVisibleItem;
				currentVisibleItemCount = visibleItemCount;
			}

			private void isScrollCompleted() {
				if (currentVisibleItemCount > 0 && currentScrollState == SCROLL_STATE_IDLE) {

					if (isLoadingMore) {
						return;
					}

					if (mNowCount >= mMaxCount) {
						return;
					}

					beforeLoadMore();
					loadMore();
				}
			}
		});

	}

	@SuppressLint("InflateParams")
	private void addFooter() {

		mFooterView = ((LayoutInflater) mActivity.getApplicationContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.load_more_footer, null, false);
		mProgressBarLoadMore = (View) mFooterView.findViewById(R.id.load_more_progressCircle);
		mProgressBarLoadMore.setVisibility(View.GONE);
		mListView.addFooterView(mFooterView);
	}

	protected void loadMore() {

	}

	protected void beforeLoadMore() {
		isLoadingMore = true;
		mProgressBarLoadMore.setVisibility(View.VISIBLE);
		AnimationHelper.startRotateAnimation(mProgressBarLoadMore);
	}

	protected void afterLoadMore() {
		isLoadingMore = false;
		AnimationHelper.stopAnimation(mProgressBarLoadMore);
		mProgressBarLoadMore.setVisibility(View.GONE);
	}

	public void onDestroyView() {
		super.onDestroyView();

		mListView.setAdapter(null);
		mListView.setOnItemClickListener(null);
		mListView.setOnScrollListener(null);
	}

	protected void checkDataType() {
		this.dataType = Data_Type.None;

		boolean isPointCoupon = DataSaveHelper.getLocalBoolean(mActivity.getApplicationContext(), Constant.KEY_LOCAL_SAVE_OFFER_LIST_IS_POINT_COUPON, false);
		
		if (isPointCoupon) {
			setActionBarTitle(mActivity.getString(R.string.LBL_TB_COUPON_POINT));
			this.dataType = Data_Type.Offer_Coupon_List_Point;
		} else {
			setActionBarTitle(mActivity.getString(R.string.LBL_TB_COUPON));
			this.dataType = Data_Type.Offer_Coupon_List_Normal;
		}

		//default へ
		DataSaveHelper.saveLocal(mActivity.getApplicationContext(), Constant.KEY_LOCAL_SAVE_OFFER_LIST_IS_POINT_COUPON, false, Java_Data_Type.Boolean);
	}

}
