package com.jillstuart.beauty.app.mypage.recovery;


import java.util.HashSet;
import java.util.Set;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserRecoveryInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;
import com.jillstuart.beauty.app.mypage.view.MyPageQuestionAskView;

public class MyPageRecoveryFragment extends MyPageBaseFragment{

	private EditText mEditTextPointProgramId;
	private MyPageQuestionAskView mQuesAsk;
	
	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_recovery, viewgroup,
				false);
	
		initViews(view);
		
		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	private void initViews(View view){
		Button btnNextButton = (Button)view.findViewById(R.id.mypage_recovery_btn_next);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {
			
			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});
		
		mEditTextPointProgramId = (EditText)view.findViewById(R.id.mypage_recovery_program_id);
		setEditTextMaxLength(mEditTextPointProgramId, Constant.INPUT_MAX_LENGTH_NUMBER);
		
		mQuesAsk = (MyPageQuestionAskView)view.findViewById(R.id.mypage_recovery_ques_ask);
		mQuesAsk.setActivity(mActivity);
		setOnFoucsChange();
		
		if (Config.MYPAGE_IS_INPUT_FILL) {
			mEditTextPointProgramId.setText("A152492087980");
			mQuesAsk.setQuestion("歌手");
			mQuesAsk.setQuestionCode("0040");
			mQuesAsk.setAnswer("西野");
		}
	}

	private void setOnFoucsChange() {
		setEditTextOnFoucsChangeKeyBoard(mEditTextPointProgramId);
	}
	
	private void btnNextClick(){
		//check input
		if (checkIsInputValid() == false) {
			return;
		}
		
		openNext();
	}
	
	private boolean checkIsInputValid(){
		
		resetEditText();
		
		boolean isValid = true;
		StringBuffer msg = new StringBuffer();
		Set<View> targetsSet = new HashSet<View>();
		
		//program id
		//必須
		String programIdString = ConvertHelper.toString(mEditTextPointProgramId.getText());
		if(CheckHelper.isStringEmpty(programIdString)){
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),"Jewel Point Program ID"));
			targetsSet.add(mEditTextPointProgramId);
		}else if (CheckHelper.isNumber(programIdString) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_ERR),"Jewel Point Program ID"));
			targetsSet.add(mEditTextPointProgramId);
		}
		
		//質問
		//必須
		if(CheckHelper.isStringEmpty(mQuesAsk.getQuestion())){
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_SELECT_NOT),"質問"));
			targetsSet.add(mQuesAsk.getEditTextQues());
		}
		
		//答え
		//必須
		if(CheckHelper.isStringEmpty(mQuesAsk.getAnswer())){
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),"質問の答え"));
			targetsSet.add(mQuesAsk.getEditTextAnswer());
		}
		
		if (isValid == false) {
			for (View view : targetsSet) {
				setEditTextValid(view, isValid);
			}
			
			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null, msg.toString(), null);
			
			return false;
		}
		return true;
	}
	
	private void resetEditText(){
		setEditTextValid(mEditTextPointProgramId, true);
		setEditTextValid(mQuesAsk.getEditTextAnswer(), true);
		setEditTextValid(mQuesAsk.getEditTextQues(), true);
	}
	
	private void openNext(){
		
	    UserRecoveryInfo info = new UserRecoveryInfo();
		info.setPointProgramId(ConvertHelper.toString(mEditTextPointProgramId.getText()));
		info.setAnswer(mQuesAsk.getAnswer());
		info.setQuesCode(mQuesAsk.getQuestionCode());
		
		JillDataControll.requestData(Data_Type.API_Post_Recovery, info, mActivity,  new LeoDataAPIListener(){

			@Override
			public void onRequstDataComplete(UserInfo data) {
				afterApi(data, getString(R.string.MSG_SHOW_MP_RE_SUCCESS_TITLE), getString(R.string.MSG_SHOW_MP_RE_SUCCESS_TEXT), true);
			}
			
		});
	}
}
