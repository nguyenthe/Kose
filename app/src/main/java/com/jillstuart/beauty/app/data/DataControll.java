package com.jillstuart.beauty.app.data;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.CheckHelper;

public class DataControll {
	
 protected static boolean checkNet(BaseActionBarActivity activity) {
		boolean isConnected = CheckHelper.isNetConnectedOrConnecting(activity);
		if (isConnected == false) {
			activity.showErrorMessage(null, activity.getString(R.string.MSG_ERR_NET_CONNECT_ERR));
		}

		return isConnected;
	}
}
