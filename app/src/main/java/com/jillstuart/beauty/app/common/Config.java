package com.jillstuart.beauty.app.common;



public class Config {
	
	//m=1 付けるか
	public static final boolean IS_ADD_URL_PARA = true;//release true
	//ホワイトリスト
	public static final boolean IS_WHITE_LIST_VALID = true;//release true
	//強制アップデート有効か
	public static final boolean IS_CHECK_MARKET_VERSION_VALID = true;//release true
	
	//---FOR TEST
	//mypage
	public static final boolean MYPAGE_IS_INPUT_FILL = false; //release false

	//リセット有効か
	public static final boolean USER_IS_CAN_RESET = false;//release false
	
	
	//*************************
	//TAG
	public static final String TAB_SELECT_TAG = Constant.TAG_TAB_HOME;
	
	//--KEY
	//******!!!!!!!!!注意：xml/ga_global_tracker.xmlに変更!!!!!!!!!!!!!!!
//	public static final String GOOGLE_ANALYTICS_KEY ="UA-61284334-1"; //本番
//	public static final String GOOGLE_ANALYTICS_KEY ="UA-61423891-1";//テスト
	
	public static final String STAMP_USE_PASSWROD = "0831";
	
	public static final int OFFERS_MAX_COUNT = 100;
	public static final int OFFERS_PAGE_COUNT = 5;
	
	
}
