package com.jillstuart.beauty.app.offer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.*;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.*;
import co.leonisand.offers.OffersRecommendation;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.fragment.BaseFragment;
import com.jillstuart.beauty.app.common.helper.ImageDownloadHepler;
import com.jillstuart.beauty.app.common.listener.LeoDataListener;
import com.jillstuart.beauty.app.data.OffersDataControll;
import com.jillstuart.beauty.app.R;

public class RecomDetailFragment extends BaseFragment {

	public static String TITLE = "レコメンデーション";

	private OffersRecommendation mRecommendation;
	private int mRecommendation_id;
	private WebView mWebview;
	private ImageView mImageView;
	private ProgressBar mImageViewProgressBar;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		mRecommendation_id = getArguments().getInt(Constant.KEY_FG_ID);

	}

	@SuppressLint("SetJavaScriptEnabled")
	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);

		View view = layoutinflater.inflate(R.layout.fragment, viewgroup, false);

		FrameLayout framelayout = (FrameLayout) view.findViewById(R.id.fragment_content);

		View recomView = layoutinflater.inflate(R.layout.offer_recom_detail, viewgroup, false);

		mImageView = (ImageView) recomView.findViewById(R.id.recom_detail_image);
		mImageViewProgressBar = (ProgressBar) recomView.findViewById(R.id.recom_detail_image_loading);

		mWebview = (WebView) recomView.findViewById(R.id.recom_detail_webview);

		mWebview.getSettings().setJavaScriptEnabled(true);
		mWebview.setWebChromeClient(new WebChromeClient() {
			public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
				mActivity.showErrorMessage(null,
						consoleMessage.message() + ":" + consoleMessage.lineNumber() + ":" + consoleMessage.sourceId());
				return true;
			}

			@Override
			public boolean onJsAlert(WebView view, String url, String message, android.webkit.JsResult result) {
				try {
					mActivity.showErrorMessage(null, message.toString());
					return true;
				} finally {
					result.confirm();
				}
			}
		});

		framelayout.addView(recomView);

		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		getData();
	}

	private void getData() {

		mActivity.showOverlayProgress();

		Bundle params = new Bundle();
		params.putInt(OffersDataControll.KEY_DATA_PARA_ID, mRecommendation_id);
		OffersDataControll.getData(Data_Type.Offer_Recom_Detail, params, mActivity,
				new LeoDataListener.LeoDataSingleListener() {

					@Override
					public void onRequstDataComplete(Request_Status status, Object data) {

						mActivity.dismissOverlayProgress();

						if (status == Request_Status.Success) {

							mRecommendation = (OffersRecommendation) data;

							setContents();
						}
					}
				});
	
	}

	private void setContents() {
		// image
		new ImageDownloadHepler(mRecommendation.getThumbnailImage(), mImageView, mImageViewProgressBar);

		// webview
		String html = mRecommendation.getContent();
		// mWebview.loadUrl(html);
		mWebview.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);

	}

}
