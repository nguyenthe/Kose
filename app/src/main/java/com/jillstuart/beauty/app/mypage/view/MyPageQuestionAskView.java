package com.jillstuart.beauty.app.mypage.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataJsonListListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogListSelectCompleteListener;
import com.jillstuart.beauty.app.data.JsonDataControll;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MyPageQuestionAskView extends FrameLayout {
	private EditText mEditTextQues;
	private EditText mEditTextAnswer;

	private BaseActionBarActivity activity;
	private List<HashMap<String, String>> quesList;
	private List<String> quesNameList;
	private int selectIndex = Constant.UNSELECT_INDEX;

	private String questionCode = Constant.STRING_EMPTY;
	private String question = Constant.STRING_EMPTY;

	public MyPageQuestionAskView(Context context) {
		super(context);
		initialize();
	}

	public MyPageQuestionAskView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public MyPageQuestionAskView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.mypage_question_ask_view);

		int quesHintId = attributes.getResourceId(R.styleable.mypage_question_ask_view_question_hint,
				R.string.LBL_MP_IN_QUS_HINT);
		int answerTitleId = attributes.getResourceId(R.styleable.mypage_question_ask_view_answer_title,
				R.string.LBL_MP_IN_QUS_TITLE);

		attributes.recycle();

		inflate(getContext(), R.layout.mypage_view_question_ask, this);

		mEditTextQues = (EditText) findViewById(R.id.question);
		mEditTextAnswer = (EditText) findViewById(R.id.answer);

		mEditTextQues.setFilters(new InputFilter[] { new InputFilter.LengthFilter(Constant.INPUT_MAX_LENGTH_STRING) });
		mEditTextAnswer
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(Constant.INPUT_MAX_LENGTH_STRING) });

		mEditTextQues.setHint(quesHintId);

		TextView answerTitle = (TextView) findViewById(R.id.answer_title);
		answerTitle.setText(answerTitleId);

		// question focus,keyboard not show
		mEditTextQues.setInputType(InputType.TYPE_NULL);

	}

	protected void setEditTextOnFoucsChangeKeyBoard(View view) {
		if (activity == null) {
			return;
		}

		view.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				Common.hiddenShowKeyBoard(activity, v, hasFocus);
			}
		});
		
	}

	private void setEditTextOnFoucsChangeQuestionSelect(View view) {
		if (activity == null) {
			return;
		}

		view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (activity.getCurrentFocus() == v) {
						showQuestionList();
				}
			}
		});

	}

	private void showQuestionList() {
		if (quesList == null) {
			// load data
			getQuestionList(true);
			return;
		}

		DialogHelper.showAlertViewWithData(Alert_Type.List_Select, activity, null, null, quesNameList, selectIndex,
				new LeoDialogListSelectCompleteListener() {

					@Override
					public void OnDialogListSelectComplete(int index) {
						selectIndex = index;
						if (index != Constant.UNSELECT_INDEX) {
							HashMap<String, String> selectMap = quesList.get(index);
							question = selectMap.get(Constant.KEY_DATA_VALUE);
							questionCode = selectMap.get(Constant.KEY_DATA_KEY);
							mEditTextQues.setText(question);
						}
					}

				});

	}

	private void getQuestionList(final boolean completedShowList) {
		JsonDataControll.getDataJson(Data_Type.API_Get_Json_Recovery_Ques, activity, new LeoDataJsonListListener() {

			@Override
			public void onRequstDataComplete(Request_Status status, List<HashMap<String, String>> datas) {

				if (status != Request_Status.Success) {
					return;
				}

				quesList = datas;
				getQuestionNameList();

				if (completedShowList) {
					showQuestionList();
				}
			}

		});
	}

	private void getQuestionNameList() {
		quesNameList = new ArrayList<String>();
		for (HashMap<String, String> value : quesList) {
			quesNameList.add(value.get(Constant.KEY_DATA_VALUE));
		}
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return ConvertHelper.toString(mEditTextAnswer.getText());
	}

	public void setAnswer(String answer) {
		mEditTextAnswer.setText(answer);
	}

	// public Activity getActivity() {
	// return activity;
	// }

	public void setActivity(BaseActionBarActivity activity) {
		this.activity = activity;

		setEditTextOnFoucsChangeQuestionSelect(mEditTextQues);
		setEditTextOnFoucsChangeKeyBoard(mEditTextAnswer);
	}

	public EditText getEditTextQues() {
		return mEditTextQues;
	}

	public EditText getEditTextAnswer() {
		return mEditTextAnswer;
	}

	public void removeLisenters() {
		mEditTextQues.setOnFocusChangeListener(null);
		mEditTextAnswer.setOnFocusChangeListener(null);
	}

}
