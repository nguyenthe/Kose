package com.jillstuart.beauty.app.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.jillstuart.beauty.app.common.helper.RequestHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIJsonListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataJsonListListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;

import android.app.Activity;

public class JsonDataControll extends DataControll{

	private static String KEY_CHANGE_COUPON_SHOP = "自宅受け取り";
	private static String KEY_CHANGE_COUPON_SHOP_CODE = "9999";

	public static void getDataJson(Data_Type data_Type, BaseActionBarActivity activity,
			LeoDataListener listener) {

		// 質問リスト以外はネットチェック必要
		if (data_Type != Data_Type.API_Get_Json_Recovery_Ques && checkNet(activity) == false) {
			if (listener != null) {
				if (listener instanceof LeoDataSingleListener) {
					((LeoDataSingleListener)listener).onRequstDataComplete(Request_Status.Error_Net_Connect, null);
				}else if (listener instanceof LeoDataJsonListListener) {
					((LeoDataJsonListListener)listener).onRequstDataComplete(Request_Status.Error_Net_Connect, null);
				}
			}
			return;
		}

		switch (data_Type) {

		case API_Get_Json_Coupon_Delivery_Shops:
			getJsonCouponShopList(listener, activity);
			break;
		case API_Get_Json_Recovery_Ques:
			getJsonQuestionList(listener);
			break;
		case Kose_Shop_Css:
			getKoseShopCssContent(listener);
			break;
		case API_Get_Json_App_Version_Check:
			getJsonAppVersionCheck(listener);
			break;
		default:
			break;
		}
	}

	private static void getJsonAppVersionCheck(final LeoDataListener listener) {

		RequestHelper.requestJson(UrlConstant.URL_DOWNLOAD_VERSION_CHECK_JSON, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {
				((LeoDataAPIJsonListener)listener).onRequstDataComplete(data);
			}

		});

	}
	
	private static void getKoseShopCssContent(final LeoDataListener listener){
		if (listener instanceof LeoDataSingleListener) {
			RequestHelper.downLoad(UrlConstant.URL_TAB_SHOPLIST_CSS, (LeoDataSingleListener)listener);
		}
	}
	
	/**
	 * 質問
	 * 
	 * @param listener
	 */
	private static void getJsonQuestionList(LeoDataListener listener) {
		List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

		for (int i = 0; i < 5; i++) {
			HashMap<String, String> value = new HashMap<String, String>();
			switch (i) {
			case 0:
				value.put(Constant.KEY_DATA_KEY, "0010");
				value.put(Constant.KEY_DATA_VALUE, "初めてのペットの名前は?");
				break;
			case 1:
				value.put(Constant.KEY_DATA_KEY, "0020");
				value.put(Constant.KEY_DATA_VALUE, "中学生の親友の名前は?");
				break;
			case 2:
				value.put(Constant.KEY_DATA_KEY, "0030");
				value.put(Constant.KEY_DATA_VALUE, "一番好きな映画は?");
				break;
			case 3:
				value.put(Constant.KEY_DATA_KEY, "0040");
				value.put(Constant.KEY_DATA_VALUE, "一番好きな歌手は?");
				break;
			case 4:
				value.put(Constant.KEY_DATA_KEY, "0050");
				value.put(Constant.KEY_DATA_VALUE, "あなたの母親の旧姓は?");
				break;
			default:
				break;
			}
			data.add(value);
		}
		if (listener != null && listener instanceof LeoDataJsonListListener) {
			((LeoDataJsonListListener) listener).onRequstDataComplete(Request_Status.Success, data);
		}
	}

	private static void getJsonCouponShopList(final LeoDataListener listener, final Activity activity) {

		RequestHelper.requestJson(UrlConstant.URL_DOWNLOAD_COUPON_SHOPS, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {
				jsonCouponShopList(data, activity, listener);
			}

		});

	}

	private static void jsonCouponShopList(JSONObject jsonObject, Activity activity,LeoDataListener listener) {
		List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
		
		if (jsonObject.has(UrlConstant.URL_DONLAOD_KEY_DATA)) {
			JSONArray jArray = null;
			try {
				jArray = jsonObject.getJSONArray(UrlConstant.URL_DONLAOD_KEY_DATA);


				if (jArray != null) {
					for (int i = 0; i < jArray.length(); i++) {
							JSONObject jsonData = (JSONObject) jArray.get(i);
							HashMap<String, String> formatData = new HashMap<String, String>();
							formatData.put(Constant.KEY_DATA_KEY,ConvertHelper.toString(jsonData.get("code")));
							formatData.put(Constant.KEY_DATA_VALUE,ConvertHelper.toString(jsonData.get("name")));
							data.add(formatData);
					}
				}
				
				jsonCouponShopListEc(data, activity, listener);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			//shop list no data
			jsonCouponShopListEc(data, activity, listener);
		}
	}

	private static void jsonCouponShopListEc(List<HashMap<String, String>> data, Activity activity,
			LeoDataListener listener) {
		
		if (KoseHelper.userIsLoginedEc(activity.getApplicationContext())) {

			@SuppressWarnings("serial")
			HashMap<String, String> homeAddress = new HashMap<String, String>() {
				{
					put(Constant.KEY_DATA_KEY, KEY_CHANGE_COUPON_SHOP_CODE);
					put(Constant.KEY_DATA_VALUE, KEY_CHANGE_COUPON_SHOP);
				}
			};
			
			data.add(0, homeAddress);
		}

		//no shop data
		if (data ==  null || data.size() == 0) {
			//MSG_ERR_GET_DATA_AND_TRY
			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, activity, null, activity.getString(R.string.MSG_ERR_GET_DATA_AND_TRY), null);
			return;
		}
		
		if (listener != null && listener instanceof LeoDataJsonListListener) {
			((LeoDataJsonListListener) listener).onRequstDataComplete(Request_Status.Success, data);
		}
	}
}
