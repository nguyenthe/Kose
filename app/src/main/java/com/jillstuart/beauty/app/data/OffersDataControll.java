package com.jillstuart.beauty.app.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;

import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.SdkOfferHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataCountListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataListListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleWithParameterListener;

import co.leonisand.offers.OffersKit;
import co.leonisand.offers.OffersStatus;
import co.leonisand.offers.OffersKit.OffersKitStatusCode;
import co.leonisand.offers.OffersKit.OffersListener;

public class OffersDataControll {

	public static final String KEY_DATA_PARA_PAGEING = "paging";
	public static final String KEY_DATA_PARA_IDS_CATEGROY = "ca_ids";
	public static final String KEY_DATA_PARA_IDS_GROUP = "group_ids";

	public static final String KEY_DATA_PARA_ID = "id";
	public static final String KEY_DATA_PARA_TYPE = "type";
	public static final String KEY_DATA_PARA_KEY = "key";
	public static final String KEY_DATA_PARA_VALUE = "value";

	public static void getData(Data_Type data_Type, Bundle params, BaseActionBarActivity activity,
			LeoDataListener listener) {
		switch (data_Type) {
		case Offer_Coupon_List:
		case Offer_Coupon_List_Normal:
		case Offer_Coupon_List_Point:
			getCouponList(data_Type, params, activity, listener);
			break;
		case Offer_Recom_List:
			getRecomList(data_Type, params, activity, listener);
			break;
		case Offer_Category_List:
			getCategory(activity, listener);
			break;
		case Offer_Coupon_Detail:
			getSingleCoupon(params, activity, listener);
			break;
		case Offer_Recom_Detail:
			getSingleRecom(params, activity, listener);
			break;
		default:
			break;
		}
	}

	public static void getDataCount(Data_Type data_Type, Bundle params, BaseActionBarActivity activity,
			final LeoDataCountListener listener) {

		int count = Constant.NO_SET;

		switch (data_Type) {
		case Offer_Coupon_List:
			count = OffersKit.getInstance().couponsCount();
			break;
		case Offer_Recom_List:
			count = OffersKit.getInstance().recommendationsCount();
			break;
		case Offer_Coupon_List_Point:
		case Offer_Coupon_List_Normal:

			// ジャンルなどの時、全体クーポン数しか取得できない場合、すべてのクーポンを取得して、数を取る
			getCouponList(data_Type, params, activity, new LeoDataListListener() {

				@Override
				public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {
					if (status == Request_Status.Success && listener != null) {
						int cnt = datas.size();
						listener.onRequstDataCountComplete(cnt);
					} else if (status != Request_Status.Success && listener != null) {
						listener.onRequstDataCountComplete(0);
					}
				}

			});
			break;
		default:
			break;
		}

		if (count != Constant.NO_SET && listener != null) {
			listener.onRequstDataCountComplete(count);
		}
	}

	/**
	 * クーポンリストデータ
	 * 
	 * @param listener
	 */
	private static void getCouponList(Data_Type data_Type, Bundle params, BaseActionBarActivity activity,
			final LeoDataListener listener) {
		params = getParameters(data_Type, params);

		getCouponsUnblock(data_Type, params, activity, listener);
	}

	/**
	 * unblock クーポン取得
	 */
	private static void getCouponsUnblock(final Data_Type data_Type, final Bundle params,
			final BaseActionBarActivity activity, final LeoDataListener listener) {

		OffersKit.getInstance().unlockCoupons(new OffersListener() {

			@Override
			public void onDone(Map<String, Object> map) {

				// 普通のクーポンを取得
				getCouponNormal(data_Type, params, activity, listener);
			}

			@Override
			public void onFail(Integer arg0) {
				// 普通のクーポンを取得
				getCouponNormal(data_Type, params, activity, listener);
			}

		});
	}

	private static void getCouponNormal(final Data_Type data_Type, Bundle params, final BaseActionBarActivity activity,
			final LeoDataListener listener) {

		OffersKit.getInstance().coupons(true, params, new OffersListener() {

			public void onDone(Map<String, Object> map) {
				checkRsultOffer(activity, map, SdkOfferHelper.Offer_Data_Key.Coupons.getValue(), listener);
			}

			public void onFail(Integer s) {
				checkRsultOffer(activity, null, null, listener);
			}

		});
	}

	/**
	 * クーポンリストデータ
	 * 
	 * @param listener
	 */
	private static void getRecomList(Data_Type data_Type, Bundle params, final BaseActionBarActivity activity,
			final LeoDataListener listener) {

		params = getParameters(data_Type, params);

		OffersKit.getInstance().recommendations(true, params, new OffersListener() {

			public void onDone(Map<String, Object> map) {
				checkRsultOffer(activity, map, SdkOfferHelper.Offer_Data_Key.Recommendations.getValue(), listener);
			}

			public void onFail(Integer s) {
				checkRsultOffer(activity, null, null, listener);
			}

		});
	}

	/**
	 * ジャンル
	 */
	private static void getCategory(final BaseActionBarActivity activity, final LeoDataListener listener) {
		OffersKit.getInstance().categories(true, new OffersListener() {
			@Override
			public void onDone(Map<String, Object> map) {
				checkRsultOffer(activity, map, SdkOfferHelper.Offer_Data_Key.Categroies.getValue(), listener);
			}

			@Override
			public void onFail(Integer arg0) {
				checkRsultOffer(activity, null, null, listener);
			}

		});
	}

	/**
	 * クーポン 詳細
	 * 
	 * @param params
	 * @param activity
	 * @param listener
	 */
	private static void getSingleCoupon(Bundle params, final BaseActionBarActivity activity,
			final LeoDataListener listener) {
		int id = params.getInt(KEY_DATA_PARA_ID);
		OffersKit.getInstance().coupon(id, true, new OffersListener() {

			public void onDone(Map<String, Object> map) {

				checkRsultOffer(activity, map, SdkOfferHelper.Offer_Data_Key.Coupon.getValue(), listener);

			}

			public void onFail(Integer s) {
				checkRsultOffer(activity, null, null, listener);
			}

		});
	}

	/**
	 * レコメンデーション 詳細
	 * 
	 * @param params
	 * @param activity
	 * @param listener
	 */
	private static void getSingleRecom(Bundle params, final BaseActionBarActivity activity,
			final LeoDataListener listener) {
		int id = params.getInt(KEY_DATA_PARA_ID);
		OffersKit.getInstance().recommendation(id, true, new OffersListener() {

			public void onDone(Map<String, Object> map) {
				checkRsultOffer(activity, map, SdkOfferHelper.Offer_Data_Key.Recommendation.getValue(), listener);
			}

			public void onFail(Integer s) {
				checkRsultOffer(activity, null, null, listener);
			}
		});
	}

	/**
	 * 結果
	 * 
	 * @param activity
	 * @param status
	 * @param listener
	 * @param datas
	 */
	@SuppressWarnings("unchecked")
	private static <T> void checkRsultOffer(BaseActionBarActivity activity, Map<String, Object> result, String dataKey,
			LeoDataListener listener) {
		List<T> datas = null;
		Object single = null;

		Request_Status resultStatus = Request_Status.Failure;
		OffersStatus status = null;
		if (result != null && result.get(SdkOfferHelper.Offer_Data_Key.Status.getValue()) != null) {
			status = (OffersStatus) result.get(SdkOfferHelper.Offer_Data_Key.Status.getValue());
			if (status != null && status.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusSuccess)) {
				resultStatus = Request_Status.Success;
			}
		}

		// 成功
		if (resultStatus == Request_Status.Success) {
			if (result.get(dataKey) instanceof ArrayList<?>) {
				datas = (ArrayList<T>) result.get(dataKey);
			} else {
				single = result.get(dataKey);
			}
		} else {
			checkRsultShowOfferMsg(activity, status, dataKey);

			if (result != null &&result.get(dataKey) instanceof ArrayList<?>) {
				datas = new ArrayList<T>();
			} else {
				single = new Object();
			}
		}

		if (listener instanceof LeoDataListListener) {
			((LeoDataListListener) listener).onRequstDataComplete(resultStatus, datas);
		} else if (listener instanceof LeoDataSingleListener) {
			((LeoDataSingleListener) listener).onRequstDataComplete(resultStatus, single);
		} else if (listener instanceof LeoDataSingleWithParameterListener) {
			HashMap<String, Object> rs = new HashMap<String, Object>();
			rs.put(KEY_DATA_PARA_TYPE, status);
			rs.put(KEY_DATA_PARA_VALUE, single);
			((LeoDataSingleWithParameterListener) listener).onRequstDataComplete(resultStatus, rs);

		}

	}

	private static void checkRsultShowOfferMsg(BaseActionBarActivity activity, OffersStatus status, String dataKey) {
		if (status == null) {
			activity.showErrorNoNetworkConnection();
		} else if (status.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusSuccess) == false) {

			String msg = null;

			// 詳細
			if (SdkOfferHelper.Offer_Data_Key.Coupon.getValue().equals(dataKey)
					|| SdkOfferHelper.Offer_Data_Key.Recommendation.getValue().equals(dataKey)) {
				
				if ((status.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusPleaseRetry) || status.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusNotFound)) == false) {
					msg = status.getCodeMessage();
				}
			}

			if (CheckHelper.isStringEmpty(msg) == false) {
				activity.showErrorMessage(null, msg);

			}
		}
	}

	/**
	 * パラメーター
	 * 
	 * @param data_Type
	 * @param paramsData
	 * @return
	 */
	private static Bundle getParameters(Data_Type data_Type, Bundle paramsData) {
		Bundle params = getBaseParameters();

		if (paramsData == null || paramsData.size() == 0) {
			return params;
		}

		switch (data_Type) {
		case Offer_Coupon_List:
		case Offer_Coupon_List_Normal:
		case Offer_Coupon_List_Point:
			if (paramsData.containsKey(KEY_DATA_PARA_IDS_CATEGROY)) {
				params.putString("category_ids", paramsData.getString(KEY_DATA_PARA_IDS_CATEGROY));
			}
			if (paramsData.containsKey(KEY_DATA_PARA_IDS_GROUP)) {
				params.putString("group_ids", paramsData.getString(KEY_DATA_PARA_IDS_GROUP));
			}
			break;

		default:
			break;
		}

		if (paramsData.containsKey(KEY_DATA_PARA_PAGEING)) {
			params.putString("offset", paramsData.getString(KEY_DATA_PARA_PAGEING));
			params.putString("limit", String.valueOf(Config.OFFERS_PAGE_COUNT));
		}

		return params;
	}

	private static Bundle getBaseParameters() {
		Bundle params = new Bundle();
		params.putString("sort_target", "delivery_from");
		params.putString("sort_direction", "descending");
		params.putString("offset", ConvertHelper.toString(0));
		params.putString("limit", String.valueOf(Config.OFFERS_MAX_COUNT));
		return params;
	}

//	private static Bundle removeParametersPaging(Bundle params) {
//		params.putString("limit", String.valueOf(Config.OFFERS_MAX_COUNT));
//
//		return params;
//	}
}
