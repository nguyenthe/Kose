package com.jillstuart.beauty.app.offer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;
import co.leonisand.offers.OffersRecommendation.ActionType;
import co.leonisand.offers.OffersRecommendation.ContentType;
import co.leonisand.offers.OffersKit.OffersListener;
import co.leonisand.offers.OffersRecommendation;

import java.util.*;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.fragment.BaseListViewFragment;
import com.jillstuart.beauty.app.common.helper.DateHelper;
import com.jillstuart.beauty.app.common.helper.ImageDownloadHepler;
import com.jillstuart.beauty.app.common.listener.LeoAdapterViewOnItemClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataCountListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataListListener;
import com.jillstuart.beauty.app.data.OffersDataControll;

public class RecomListFragment extends BaseListViewFragment {

	private ArrayAdapter<OffersRecommendation> mListAdapter;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setHasOptionsMenu(true);
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater menuinflater) {
		super.onCreateOptionsMenu(menu, menuinflater);
		menuinflater.inflate(R.menu.menu_recommendations, menu);
	}

	public boolean onOptionsItemSelected(MenuItem menuitem) {
		return super.onOptionsItemSelected(menuitem);
	}

	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle) {

		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.fragment, viewgroup, false);
		FrameLayout framelayout = (FrameLayout) view.findViewById(R.id.fragment_content);
		mListView = new ListView(mActivity);

		mListView.setDividerHeight(0); // 線消す
		mListAdapter = new ArrayAdapter<OffersRecommendation>(mActivity, 0) {
			private LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getSystemService("layout_inflater");

			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = mLayoutInflater.inflate(R.layout.listview_cell_recom, null);
				}

				OffersRecommendation offersrecommendation = (OffersRecommendation) getItem(position);

				TextView title = (TextView) convertView.findViewById(R.id.recommendations_cell_title);
				title.setText(offersrecommendation.getName());

				TextView description = (TextView) convertView.findViewById(R.id.recommendations_cell_date);

				String dateString = DateHelper.dateFormatOfferDateStringToFormatString(
						offersrecommendation.getDeliveryFromAt(), getString(R.string.format_recom_list_date));
				description.setText(dateString);

				// edit by tll 20141002
				// 画像設定
				setImageView(convertView, offersrecommendation.getThumbnailImage());

				return convertView;
			}
		};
		mListView.setAdapter(mListAdapter);
		framelayout.addView(mListView);

		super.setListViewPaging();

		mListView.setOnItemClickListener(new LeoAdapterViewOnItemClickListener() {

			@Override
			public void onSingleClick(AdapterView<?> adapteView, View view, int position, long id) {

				doOnItemClick(adapteView, view, position, id);
			}
		});
		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

		this.loadFirst();
	}

	private void loadFirst() {
		mNowCount = 0;
		mActivity.showOverlayProgress();

		OffersDataControll.getDataCount(Data_Type.Offer_Recom_List, null, mActivity, new LeoDataCountListener() {
			
			@Override
			public void onRequstDataCountComplete(int count) {
				
				mMaxCount = count;
				
				loadDateWithStartIndex(0);
			}
		});
		
	}

	@Override
	protected void loadMore() {
		loadDateWithStartIndex(mNowCount);
	}

	private void loadDateWithStartIndex(int start) {

		Bundle params = new Bundle();
		params.putString(OffersDataControll.KEY_DATA_PARA_PAGEING, String.valueOf(start));
		
		if (start == 0) {
			mListAdapter.clear();
		}

		OffersDataControll.getData(Data_Type.Offer_Recom_List, params, mActivity, new LeoDataListListener() {

			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {
				mActivity.dismissOverlayProgress();

				if (isLoadingMore) {
					afterLoadMore();
				}
				
				if (status == Request_Status.Success) {
					@SuppressWarnings("unchecked")
					List<OffersRecommendation> items = (List<OffersRecommendation>) datas;
					for (OffersRecommendation item : items) {
						mListAdapter.add(item);
					}

					// ページング記録
					mNowCount += items.size();
					//既読設定
					setRead(items);
				}
			}
			
		});
		
	}

	private void setRead(List<OffersRecommendation> items){
		for (OffersRecommendation item : items) {
			// アクションタイプ
			setActionType(item, ActionType.VIEW);
		}
	}
	
	private void setActionType(OffersRecommendation item, ActionType actionType) {
		item.sendAction(actionType, new OffersListener() {

			@Override
			public void onFail(Integer arg0) {

			}

			@Override
			public void onDone(Map<String, Object> arg0) {

			}
		});

	}

	/**
	 * クーポンの画像設定
	 */
	private void setImageView(View v, String imageUrl) {

		ProgressBar imageProgressBar = (ProgressBar) v.findViewById(R.id.recommendations_cell_image_loading);
		// edit by tll 20141001 全体画像ダウンロードcache修正
		ImageView mImageView = (ImageView) v.findViewById(R.id.recommendations_cell_image);
		new ImageDownloadHepler(imageUrl, mImageView, imageProgressBar);
	}

	protected void doOnItemClick(AdapterView<?> adapteView, View view, int position, long id) {

		OffersRecommendation offersrecommendation = (OffersRecommendation) adapteView.getItemAtPosition(position);

		// アクションタイプ
		setActionType(offersrecommendation, ActionType.OPEN);

		if (ContentType.URL == offersrecommendation.getContentType()) {

			Bundle bundle = new Bundle();
			bundle.putString(Constant.KEY_FG_URL, offersrecommendation.getContent());

			RecomDetailWebFragment webViewFragment = new RecomDetailWebFragment();
			webViewFragment.setArguments(bundle);
			mFragmentManager.beginTransaction().add(R.id.container, webViewFragment, offersrecommendation.getName())
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();

		} else if (ContentType.ARTICLE == offersrecommendation.getContentType()) {

			Bundle bundle = new Bundle();
			bundle.putInt(Constant.KEY_FG_ID, offersrecommendation.getId());

			RecomDetailFragment recommendationfragment = new RecomDetailFragment();
			recommendationfragment.setArguments(bundle);
			mFragmentManager.beginTransaction()
					.add(R.id.container, recommendationfragment, offersrecommendation.getName())
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
		}
	}

}
