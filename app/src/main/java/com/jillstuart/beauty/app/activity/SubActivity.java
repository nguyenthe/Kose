package com.jillstuart.beauty.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SubActivity extends BaseActionBarActivity {

	private Fragment mFragment;

	private OnBackStackChangedListener mOnBackStackChangedListener;

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem menuitem) {
		boolean res = true;
		switch (menuitem.getItemId()) {
		case android.R.id.home:

			if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				getSupportFragmentManager().popBackStack();
			} else {
				finish();
			}

			break;
		default:
			res = super.onOptionsItemSelected(menuitem);
			break;
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		
		setContentView(R.layout.activity_sub);

		mActionBar.setDisplayHomeAsUpEnabled(true); // ホームアイコン表示
		mActionBar.setHomeButtonEnabled(true); // ホームアイコンボタン有効

		mOnBackStackChangedListener = new OnBackStackChangedListener() {
			public void onBackStackChanged() {
				refreshActionBar();
			}
		};

		getSupportFragmentManager().addOnBackStackChangedListener(
				mOnBackStackChangedListener);

		Intent intent = getIntent();
		String fragment = intent.getStringExtra(Constant.KEY_FG_CLASS_NAME);
		String title = intent.getStringExtra(Constant.KEY_FG_TITLE);

		Class<Fragment> clazz;
		try {
			clazz = (Class<Fragment>) Class.forName(fragment);
			Constructor<Fragment> constructor = clazz.getConstructor();
			mFragment = constructor.newInstance();

//			Field f = clazz.getField("TITLE");
//			title = (String) f.get(clazz);
//
//			System.out.println(title);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
//		} catch (NoSuchFieldException e) {
//			e.printStackTrace();
		}

		setActionBarTitle(title);
		
		mActionBar
				.setHomeAsUpIndicator(R.drawable.navigationbar_icon_dissapear);

		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, mFragment, title).commit();
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {

		// 現在のFragment
		Fragment fragment = getSupportFragmentManager().findFragmentById(
				R.id.container);
		try {
			Method m = fragment.getClass().getMethod("dispatchKeyEvent",
					KeyEvent.class);
			boolean ret = (Boolean) m.invoke(fragment, e);
			if (ret) {
				return ret;
			}
		} catch (NoSuchMethodException e1) {
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		}
		return super.dispatchKeyEvent(e);
	}

	@Override
	public void finish() {
		super.finish();
		setResult(RESULT_CANCELED);
		overridePendingTransition(0, 0);
	}

	private void refreshActionBar() {

		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			mActionBar.setHomeAsUpIndicator(R.drawable.navigationbar_icon_back);
		} else {
			mActionBar
					.setHomeAsUpIndicator(R.drawable.navigationbar_icon_dissapear);
		}

	}

	public void onDestroy() {

		getSupportFragmentManager().removeOnBackStackChangedListener(
				mOnBackStackChangedListener);

		super.onDestroy();
	}

	@Override
	public void onBackStackChanged() {
		super.onBackStackChanged();
	}
}
