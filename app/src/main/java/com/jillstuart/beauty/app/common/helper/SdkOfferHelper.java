package com.jillstuart.beauty.app.common.helper;


import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Constant;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;

@SuppressLint("SimpleDateFormat")
public class SdkOfferHelper {

	private static float OFFER_DETAIL_STAMP_CIRCLE_SIZE_RATE = 0.6f;
	private static float OFFER_DETAIL_STAMP_SIZE_RATE = 1f;

	public enum Offer_Type {
		OfferCoupon(1), OfferRecom(2);

		private final int value;

		private Offer_Type(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Offer_Data_Key {
		Status("status"), Coupon("coupon"), Coupons("coupons"), Recommendation(
				"recommendation"), Recommendations("recommendations"),Categroies("categories");

		private final String value;

		private Offer_Data_Key(final String newValue) {
			value = newValue;
		}

		public String getValue() {
			return value;
		}
	}

	public static Point getStampCircleSize(Activity activity, int viewMaxHeight) {
		double width = (Common.getDisplaySize(activity).x * OFFER_DETAIL_STAMP_CIRCLE_SIZE_RATE);
		double height = viewMaxHeight * OFFER_DETAIL_STAMP_CIRCLE_SIZE_RATE;
		if (width > height) {
			width = height;
		}else {
			height = width;
		}

		Point sizePoint = new Point((int) width, (int) width);
		return sizePoint;
	}

	public static Point getStampImageSize(Activity activity,  int viewMaxHeight) {

		int size = (int) (Constant.BASE_SCREEN_DENSITY * OFFER_DETAIL_STAMP_SIZE_RATE);
		if (size > viewMaxHeight) {
			size = viewMaxHeight;
		}
		return new Point(size,size);
	}

}