package com.jillstuart.beauty.app.data.model;

import org.json.JSONObject;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.helper.CheckHelper;

import android.os.Parcel;
import android.os.Parcelable;

public class UserInfo implements Parcelable {

	protected String pointProgramId;
	protected String resultCode;
	//private String resultMsg;
	// private boolean resultIsSuccess;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

	}

	public void convertJsonToObject(JSONObject json){
		try {
			if (json.has(JillApiConstant.API_KEY_OUT_PROGRAM_ID)) {
				this.pointProgramId = json.getString(JillApiConstant.API_KEY_OUT_PROGRAM_ID);
			}else {
				this.pointProgramId = Constant.STRING_EMPTY;
			}
			
			if (json.has(JillApiConstant.API_KEY_OUT_RESULT_CODE)) {
				this.resultCode = json.getString(JillApiConstant.API_KEY_OUT_RESULT_CODE);
			}else {
				this.resultCode = Constant.STRING_EMPTY;
			}
		} catch (Exception e) {
			this.resultCode = Constant.STRING_EMPTY;
			e.printStackTrace();
		}
	}
	
	public String getPointProgramId() {
		return CheckHelper.isStringEmpty(this.pointProgramId) ? Constant.STRING_EMPTY : this.pointProgramId;
	}

	public void setPointProgramId(String pointProgramId) {
		this.pointProgramId = pointProgramId;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {

		if (JillApiConstant.API_STATUS_SUCCESS.equals(resultCode)) {
			return "正常終了";
		} else if (JillApiConstant.API_STATUS_ERR_KEY.equals(resultCode)) {
			//アクティベーション不正
			return "サーバー認証エラーが発生しました。\nしばらくお待ちください。";
		} else if (JillApiConstant.API_STATUS_ERR_PARA.equals(resultCode)) {
			//パラメータ不正
			return "サーバー通信エラーが発生しました。\n送信内容をお確かめください。";
		} else if (JillApiConstant.API_STATUS_ERR_SYSTEM.equals(resultCode)) {
			//システムエラー
			return "サーバーエラーが発生しました。\nしばらくお待ちください。";
		} else if (JillApiConstant.API_STATUS_ERR_EC_EXIST.equals(resultCode)) {
			//EC会員登録済み"
			return "ご登録済み公式オンラインショップIDです。\n別のIDをお使いください。";
		} else if (JillApiConstant.API_STATUS_ERR_EC.equals(resultCode)) {
			//EC会員不一致
			return "メールアドレスと会員コードの組み合わせが間違っているようです。\n入力内容をお確かめください。";
		} else if (JillApiConstant.API_STATUS_ERR_CARD_COUNT_OVER
				.equals(resultCode)) {
			//顧客コード登録件数オーバー
			return "会員情報が最大登録件数に達しました。";
		} else if (JillApiConstant.API_STATUS_ERR_CARD_NUMBER_EXIST
				.equals(resultCode)) {
			//顧客コード登録済み
			return "ご登録済みメンバーズカードです。\n別のカードをお使いください。";
		} else if (JillApiConstant.API_STATUS_ERR_USER.equals(resultCode)) {
			//アプリ会員不一致
			return "入力内容をお確かめください。";
		} else if (JillApiConstant.API_STATUS_ERR_POINT_NOT_LACK
				.equals(resultCode)) {
			//ポイント不足
			return "ご利用可能ポイントが不足しています。";
		} else if (JillApiConstant.API_STATUS_ERR_CARD_NUMBER_INVAILD
				.equals(resultCode)) {
			//顧客コード不一致
			return "入力内容をお確かめください。";
		} else if (JillApiConstant.API_STATUS_ERR_NET_CONNECT
				.equals(resultCode)) {
			//ネット接続エラー
			return "通信に失敗しました。\n電話状況の良いところで再度お試してください。";
		}

		return "接続エラーが発生しました。\n後ほどお試しください。";

	}

	// public void setResultMsg(String resultMsg) {
	// this.resultMsg = resultMsg;
	// }

	public boolean isResultSuccess() {

		if (CheckHelper.isStringEmpty(resultCode) == false
				&& JillApiConstant.API_STATUS_SUCCESS.equals(resultCode)) {
			return true;
		}

		return false;
	}

}
