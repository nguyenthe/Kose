package com.jillstuart.beauty.app.activity;

import android.app.Activity;
import android.os.Bundle;

import com.jillstuart.beauty.app.R;

/**
 * Created by HongNgoc on 3/27/2017.
 */

public class DemoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_new_intent);

    }
}
