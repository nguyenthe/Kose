package com.jillstuart.beauty.app.common.helper;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Social_Type;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener;
import com.jillstuart.beauty.app.common.listener.LeoListener;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * 
 * @author lily tan
 *
 */
public class OpenHepler {

	// **************** WEB *****************
	public static void openOutSideWebView(final Activity activity, final String url) {
		openOutSide(activity, url, Intent.ACTION_VIEW);
	}

	public static void openOutSideWebViewConfirm(final Activity activity, String title, final String url,final LeoListener dismissListener) {
		if (CheckHelper.isStringEmpty(title)) {
			title = activity.getResources().getString(R.string.MSG_ASK_LINK_OPEN_OUTSIDE);
		}

		DialogHelper.showAlertView(Alert_Type.Msg_Confirm, activity, title, null,
				new LeoDialogCompleteListener.LeoDialogConfirmListener() {

					@Override
					public void onOkClick() {
						openOutSideWebView(activity, url);
						if (dismissListener != null) {
							dismissListener.onEnd();
						}
					}

					@Override
					public void OnCancelClick() {
						
						if (dismissListener != null) {
							dismissListener.onEnd();
						}
					}
				});

	}

	private static void openOutSide(Activity activity, String url, String action) {
		if (CheckHelper.isStringEmpty(url)) {
			return;
		}

		Intent intent = new Intent(action);
		intent.setData(Uri.parse(url));
		activity.startActivity(intent);
	}

	// **************** TEL *****************
	public static void openCallConfirm(final Activity activity, String title, final String url) {
		if (CheckHelper.isStringEmpty(title)) {
			title = activity.getResources().getString(R.string.MSG_ASK_CALL_TEL);
		}
		DialogHelper.showAlertView(Alert_Type.Msg_Confirm, activity, title, null,
				new LeoDialogCompleteListener.LeoDialogConfirmListener() {

					@Override
					public void OnCancelClick() {

					}

					@Override
					public void onOkClick() {
						openOutSide(activity, url, Intent.ACTION_CALL);
					}
				});
	}

	// **************** TWITTER / FACEBOOK *****************
	/**
	 * open social
	 * 
	 * @param activity
	 * @param type
	 * @param parameter
	 */
	public static void openSocial(final Activity activity, final Social_Type type, final String parameter) {

		String title = null;
		switch (type) {
		case Instagram_Open:
			title = activity.getString(R.string.MSG_ASK_OPEN_INSTAGRAM);
			break;
		case Facebook_Open:
			title = activity.getString(R.string.MSG_ASK_OPEN_FACEBOOK);
			break;
		default:
			break;
		}
		DialogHelper.showAlertView(Alert_Type.Msg_Confirm, activity, title, null,
				new LeoDialogCompleteListener.LeoDialogConfirmListener() {

					@Override
					public void onOkClick() {
						doOpenSocial(activity, type, parameter);
					}

					@Override
					public void OnCancelClick() {

					}

				});

	}

	private static void doOpenSocial(Activity activity, Social_Type type, String parameter) {
		switch (type) {
		case Instagram_Open:
			doOpenInstagram(activity, type, parameter);
			break;
		case Facebook_Open:
			doOpenFacebook(activity, type, parameter);
			break;
		default:
			break;
		}
	}

	private static void doOpenInstagram(Activity activity, Social_Type type, String parameter) {
		Intent intent = null;
		try {
			// get the Twitter app if possible
			
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/_u/" + parameter));
			intent.setPackage("com.instagram.android");
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(intent);
		} catch (Exception e) {
			// no Twitter app, revert to browser
			openOutSideWebView(activity, UrlConstant.URL_OTHER_INSTAGRAM);
		}

	}

	private static void doOpenFacebook(Activity activity, Social_Type type, String parameter) {
		Intent intent = null;
		try {
			// get the Twitter app if possible
			activity.getPackageManager().getPackageInfo("com.facebook.katana", 0);
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + parameter));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		} catch (Exception e) {
			// no Twitter app, revert to browser
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.facebook.com/profile.php?id=" + parameter));
		}

		activity.startActivity(intent);
	}


}
