package com.jillstuart.beauty.app.common.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

public class BaseAlertDialogFragment extends DialogFragment {

    public static final String FIELD_LAYOUT = "layout";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_MESSAGE = "message";
    public static final String FIELD_LIST_ITEMS = "list_items";
    public static final String FIELD_LIST_ITEMS_STRING = "list_items_string";
    public static final String FIELD_LABEL_POSITIVE = "label_positive";
    public static final String FIELD_LABEL_NEGATIVE = "label_negative";
    public static final String FIELD_LABEL_NEUTRAL = "label_neutral";

    private DialogInterface.OnShowListener mListenerShow;
    private DialogInterface.OnClickListener mListenerNegativeClick;
    private DialogInterface.OnClickListener mListenerPositiveClick;
    private DialogInterface.OnClickListener mListenerNeutralClick;

    private View mView;

    private AlertDialog mAlertDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // dialog title
        if (args.containsKey(FIELD_TITLE)) {
            builder.setTitle(args.getString(FIELD_TITLE));
        }

        // dialog message
        if (args.containsKey(FIELD_MESSAGE)) {
            builder.setMessage(args.getString(FIELD_MESSAGE));
        }

        // dialog customize content view
        if (args.containsKey(FIELD_LAYOUT)) {
        /*
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content = inflater.inflate(args.getInt(FIELD_LAYOUT), null);

        builder.setView(content);
        */

            builder.setView(mView);
        }


    /*
    // dialog string list
    final List<String> items = new ArrayList<String>();
    if (args.containsKey(FIELD_LIST_ITEMS)) {
        final int[] listItems = args.getIntArray(FIELD_LIST_ITEMS);
        for (int i = 0; i < listItems.length; i++) {
            items.add(getString(listItems[i]));
        }
    }
    if (args.containsKey(FIELD_LIST_ITEMS_STRING)) {
        final String[] listItems = args.getStringArray(FIELD_LIST_ITEMS_STRING);
        for (int i = 0; i < listItems.length; i++) {
            items.add(listItems[i]);
        }
    }

    if (items.size() > 0) {
        builder.setItems(items.toArray(new String[items.size()]), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if ( mListenerItemClick != null) {
                    mListenerItemClick.onDialogItemClick(getTag(), mAlertDialog, items.get(which), which);
                }
            }

        });
    }
    */

        // positive button title and click listener
        if (args.containsKey(FIELD_LABEL_POSITIVE)) {
            builder.setPositiveButton(args.getString(FIELD_LABEL_POSITIVE), mListenerPositiveClick);
        }

        // negative button title and click listener
        if (args.containsKey(FIELD_LABEL_NEGATIVE)) {
            builder.setNegativeButton(args.getString(FIELD_LABEL_NEGATIVE), mListenerNegativeClick);
        }

        // neutral button title and click listener
        if (args.containsKey(FIELD_LABEL_NEUTRAL)) {
            builder.setNeutralButton(args.getString(FIELD_LABEL_NEUTRAL), mListenerNeutralClick);
        }

        // make dialog
        mAlertDialog = builder.create();

        // show listener
        if (mListenerShow != null) {
            mAlertDialog.setOnShowListener(mListenerShow);
        }

        return mAlertDialog;
    }

    public void setView(View view){
        mView = view;
    }
    public void setShowListener(DialogInterface.OnShowListener listener){
        mListenerShow = listener;
    }
    public void setPotitiveClickListener(DialogInterface.OnClickListener listener){
        mListenerPositiveClick = listener;
    }
    public void setNegativeClickListener(DialogInterface.OnClickListener listener){
        mListenerNegativeClick = listener;
    }
    public void setNeutralClickListener(DialogInterface.OnClickListener listener){
        mListenerNeutralClick = listener;
    }

}
