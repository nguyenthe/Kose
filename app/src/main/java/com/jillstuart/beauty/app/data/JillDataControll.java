package com.jillstuart.beauty.app.data;

import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;

import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Java_Data_Type;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.DataSaveHelper;
import com.jillstuart.beauty.app.common.helper.RequestHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIJsonListener;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserPointInfo;
import com.jillstuart.beauty.app.data.model.UserRecoveryInfo;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;

public class JillDataControll extends DataControll{

	public static void requestData(Data_Type data_Type, UserInfo info, BaseActionBarActivity activity,
			LeoDataAPIListener listener) {

		activity.showOverlayProgress();

		if (checkNet(activity) == false) {
			if (listener != null) {
				UserInfo rs = new UserInfo();
				rs.setResultCode(JillApiConstant.API_STATUS_ERR_NET_CONNECT);
				listener.onRequstDataComplete(rs);
			}
			return;
		}

		switch (data_Type) {
		case API_Get_User_Point:
			getPoint(data_Type, activity, listener);
			break;

		case API_Post_Regist_Ec:
		case API_Post_Regist_Card:
		case API_Post_Add_Ec:
		case API_Post_Add_Card:
			sendUserRegist(data_Type, info, activity, listener);
			break;
		case API_Post_Recovery:
		case API_Post_Recovery_Edit:
			sendRecovery(data_Type, info, activity, listener);
			break;

		case API_Post_ChangePoint:
			sendPointChange(data_Type, info, activity, listener);
			break;
		default:
			break;
		}
	}

	


	// **********send api**********
	private static void sendUserRegist(final Data_Type data_Type, UserInfo info, final BaseActionBarActivity activity,
			final LeoDataAPIListener listener) {
		UserRegistInfo pointInfo = (UserRegistInfo) info;

		HashMap<String, String> parameter = new HashMap<String, String>();

		if (data_Type == Data_Type.API_Post_Regist_Card || data_Type == Data_Type.API_Post_Regist_Ec) {
			parameter.put(JillApiConstant.API_KEY_IN_QUES_CODE, pointInfo.getRecoveryInfo().getQuesCode());
			parameter.put(JillApiConstant.API_KEY_IN_QUES_ANSWER, pointInfo.getRecoveryInfo().getAnswer());
			parameter.put(JillApiConstant.API_KEY_IN_DAY_BIRTHDAY, pointInfo.getBirthday());
			parameter.put(JillApiConstant.API_KEY_IN_DAY_MEMORYDAY, pointInfo.getMemoryday());
		}

		switch (data_Type) {
		case API_Post_Regist_Card:
		case API_Post_Add_Card:
			parameter.put(JillApiConstant.API_KEY_IN_CARD, pointInfo.getCardNo());
			break;
		case API_Post_Regist_Ec:
		case API_Post_Add_Ec:
			parameter.put(JillApiConstant.API_KEY_IN_EC_MAIL, pointInfo.getEcEmail());
			parameter.put(JillApiConstant.API_KEY_IN_EC_CODE, pointInfo.getEcNo());
			break;
		default:
			break;
		}

		RequestHelper.requestPost(data_Type, parameter, activity, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {

				final UserInfo userInfo = new UserInfo();
				userInfo.convertJsonToObject(data);

				// 失敗
				if (userInfo.isResultSuccess() == false) {
					if (listener != null) {
						listener.onRequstDataComplete(userInfo);
					}
					return;

				}

				// 成功
				afterLoginProccess(data_Type, activity, userInfo, new LeoListener() {

					@Override
					public void onEnd() {
						if (listener != null) {
							listener.onRequstDataComplete(userInfo);
						}
					}
				});
			}

		});
	}

	private static void sendPointChange(Data_Type data_Type, UserInfo info, final BaseActionBarActivity activity,
			final LeoDataAPIListener listener) {
		UserPointInfo pointInfo = (UserPointInfo) info;

		HashMap<String, String> parameter = new HashMap<String, String>();
		parameter.put(JillApiConstant.API_KEY_IN_POINT, pointInfo.getPointChangeString());

		RequestHelper.requestPost(data_Type, parameter, activity, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {
				if (listener != null) {
					UserInfo userInfo = new UserInfo();
					userInfo.convertJsonToObject(data);
					listener.onRequstDataComplete(userInfo);
				}
			}

		});
	}

	private static void sendRecovery(final Data_Type data_Type, UserInfo info, final BaseActionBarActivity activity,
			final LeoDataAPIListener listener) {
		final UserRecoveryInfo recoveryInfo = (UserRecoveryInfo) info;
		HashMap<String, String> parameter = new HashMap<String, String>();

		parameter.put(JillApiConstant.API_KEY_IN_QUES_ANSWER, recoveryInfo.getAnswer());
		parameter.put(JillApiConstant.API_KEY_IN_QUES_CODE, recoveryInfo.getQuesCode());

		switch (data_Type) {
		case API_Post_Recovery_Edit:
			parameter.put(JillApiConstant.API_KEY_IN_QUES_ANSWER_NEW, recoveryInfo.getAnswerNew());
			parameter.put(JillApiConstant.API_KEY_IN_QUES_CODE_NEW, recoveryInfo.getQuesCodeNew());
			break;
		case API_Post_Recovery:
			parameter.put(JillApiConstant.API_KEY_IN_PROGRAM_ID, recoveryInfo.getPointProgramId());
			break;
		default:
			break;
		}

		RequestHelper.requestPost(data_Type, parameter, activity, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {

				final UserInfo userInfo = new UserInfo();
				userInfo.convertJsonToObject(data);

				// 失敗
				if (userInfo.isResultSuccess() == false) {
					if (listener != null) {
						listener.onRequstDataComplete(userInfo);
					}
					return;
				}

				// 成功
				afterLoginProccess(data_Type, activity, recoveryInfo, new LeoListener() {

					@Override
					public void onEnd() {
						if (listener != null) {
							listener.onRequstDataComplete(userInfo);
						}
					}
				});

			}

		});
	}

	// **********get api**********
	private static void getPoint(Data_Type data_Type, final BaseActionBarActivity activity,
			final LeoDataAPIListener listener) {

		HashMap<String, String> parameter = new HashMap<String, String>();
		RequestHelper.requestPost(data_Type, parameter, activity, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {
				// convert to userInfo
				if (listener != null) {
					UserPointInfo info = new UserPointInfo();
					info.convertJsonToObject(data);
					afterGetPoint(activity, info);
					listener.onRequstDataComplete(info);
				}
			}

		});
	}



	private static void afterLoginProccess(Data_Type dataType, BaseActionBarActivity activity, UserInfo info,
			LeoListener listener) {

		boolean isFirst = false;
		switch (dataType) {
		case API_Post_Recovery:
		case API_Post_Regist_Card:
		case API_Post_Regist_Ec:
			isFirst = true;
			break;

		default:
			break;
		}

		if (isFirst) {

			DataSaveHelper.saveLocal(activity.getApplicationContext(), JillApiConstant.KEY_MY_PAGE_PROGRAM_ID, info.getPointProgramId(), Java_Data_Type.String);
			DataSaveHelper.saveLocal(activity.getApplicationContext(), JillApiConstant.KEY_MY_PAGE_IS_LOGINED, true, Java_Data_Type.Boolean);
			
			// offer と連携
			((MainActivity) activity).connectPointProgramIdToOffer();

			if (listener != null) {
				listener.onEnd();
			}
		} else {
			if (listener != null) {
				listener.onEnd();
			}
		}
	}

	private static void afterGetPoint(Activity activity, UserPointInfo info) {

		if (info.isResultSuccess() == false) {
			return;
		}
		
		DataSaveHelper.saveLocal(activity.getApplicationContext(), JillApiConstant.KEY_MY_PAGE_IS_LOGINED_EC, info.isHasEcNumber(), Java_Data_Type.Boolean);
	}
}
