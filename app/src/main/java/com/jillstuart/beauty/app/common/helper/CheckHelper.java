package com.jillstuart.beauty.app.common.helper;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Date_Type;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.data.JsonDataControll;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIJsonListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener;

import android.app.Activity;

public class CheckHelper {

	private static Pattern patternNum = Pattern.compile("([0-9]+)");
	private static final int DATE_MONTH_MAX = 12;
	private static final int DATE_DAY_MAX = 31;
	private static final String MAIL_MARK = "@";

	/**
	 * 数字チェック
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isNumber(String input) {
		Matcher m = patternNum.matcher(input);
		m.reset();
		return m.find();
	}

	/**
	 * 文字列空白チェク
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isStringEmpty(Object obj) {
		if (obj == null) {
			return true;
		}

		if (obj.toString().trim().length() == 0) {
			return true;
		}

		return false;
	}

	public static boolean isDate(String str, Date_Type type) {
		if (isStringEmpty(str)) {
			return false;
		}

		if (isNumber(str) == false) {
			return false;
		}

		int max = 0;
		switch (type) {
		case Month:
			max = DATE_MONTH_MAX;
			break;
		case Day:
			max = DATE_DAY_MAX;
			break;
		default:
			break;
		}

		if (ConvertHelper.toInteger(str) > max) {
			return false;
		}
		return true;
	}

	public static boolean isMail(String mail) {
		if (isStringEmpty(mail)) {
			return false;
		}

		if (mail.contains(MAIL_MARK) == false) {
			return false;
		}

		return true;
	}

	public static boolean isHttpUrl(String url) {
		if (url == null) {
			return false;
		}

		if (url.startsWith("http") || url.startsWith("https")) {
			return true;
		}

		return false;
	}

	public static boolean isNetConnectedOrConnecting(BaseActionBarActivity activity) {
		return activity.isConnectedOrConnecting();
	}

	public static <T> boolean isHasContent(List<T> list) {
		if (list != null && list.size() > 0) {
			return true;
		}

		return false;
	}

	public static boolean isJsonStringNull(String str) {
		if (isStringEmpty(str)) {
			return true;
		}

		if ("null".equals(str)) {
			return true;
		}

		return false;
	}

	public static boolean isJsonListNull(List<String> list) {
		if (list == null) {
			return true;
		}
		if (list.size() == 0) {
			return true;
		}

		String first = list.get(0);
		return isJsonStringNull(first);
	}

	// private AsyncTask<Object, Void, JSONObject> marketAsyncTask;
	// バージョンチェック

	private String mPackageName;

	public void checkVersionUpdate(String packageName, final String currentVersion,
			final BaseActionBarActivity activity) {

		this.mPackageName = packageName;

		JsonDataControll.getDataJson(Data_Type.API_Get_Json_App_Version_Check, activity, new LeoDataAPIJsonListener() {

			@Override
			public void onRequstDataComplete(JSONObject data) {
				try {
					checkVersion(data, activity, currentVersion);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		});
	}

	private void checkVersion(JSONObject response, BaseActionBarActivity activity, String currentVersion)
			throws JSONException {

		if (response == null) {
			return;
		}

		String marketVersion = response.getString("version");

		if (CheckHelper.isStringEmpty(marketVersion)) {
			return;
		}

		int currentNo = ConvertHelper.toInteger(currentVersion.replace(".", ""));
		int marketNo = ConvertHelper.toInteger(marketVersion.replace(".", ""));
		//アプリバージョンはリリース中バージョンより古くないなら、終了
		if ((currentNo < marketNo) == false) {
			return;
		}
		
		int[] current = ConvertHelper.StringsToIntList(currentVersion.split("\\."));
		int[] market = ConvertHelper.StringsToIntList(marketVersion.split("\\."));

		Alert_Type alert_type = Alert_Type.Msg_Show_WithClose;
		check: for (int i = 0; i < current.length && i < market.length; i++) {
			if (current[i] < market[i]) {

				switch (i) {
				// 前の2桁なら、強制アップデート
				case 0:
				case 1:
					alert_type = Alert_Type.Msg_Show_WithOk;
					break;
				// 3桁なら、キャンセルできる
				case 2:
					alert_type = Alert_Type.Msg_Confirm;
					break;
				}

				break check;
			}
		}

		openCheckVersionDialog(alert_type, marketVersion, activity);
	}

	private void openCheckVersionDialog(Alert_Type type, String marketVersion, final Activity activity) {

		String title = activity.getString(R.string.MSG_SHOW_HAS_NEW_VERSION_TITLE);
		String text = String.format(activity.getString(R.string.MSG_SHOW_HAS_NEW_VERSION_MSG),
				activity.getString(R.string.app_name), marketVersion);
		String okBtnString = activity.getString(R.string.MSG_SHOW_HAS_NEW_VERSION_OK_BTN);
		String cancelBtnString = activity.getString(R.string.MSG_SHOW_HAS_NEW_VERSION_CANCEL_BTN);
		switch (type) {
		case Msg_Confirm:

			DialogHelper.showAlertView(type, activity, title, text, okBtnString, cancelBtnString,
					new LeoDialogCompleteListener.LeoDialogConfirmListener() {

						@Override
						public void onOkClick() {
							openAppMarket(activity);
						}

						@Override
						public void OnCancelClick() {
							// 処理なし
						}
					});
			break;
		case Msg_Show_WithOk:
			DialogHelper.showAlertView(type, activity, title, text, okBtnString, cancelBtnString,
					new LeoDialogCompleteListener.LeoDialogDismissListener() {

						@Override
						public void OnDismiss() {
							openAppMarket(activity);
						}
					});
			break;
		default:
			break;
		}
	}

	private void openAppMarket(Activity activity) {
		String url = String.format(UrlConstant.URL_ANDROID_MARKET, mPackageName);
		OpenHepler.openOutSideWebView(activity, url);
	}
}
