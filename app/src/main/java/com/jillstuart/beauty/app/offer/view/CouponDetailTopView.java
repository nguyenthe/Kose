package com.jillstuart.beauty.app.offer.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.leonisand.offers.OffersCoupon;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.CouponCategroy;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.helper.ImageDownloadHepler;
import com.jillstuart.beauty.app.common.helper.ImageDownloadHepler.OnLeonisImageDownloadCompleteListener;
import com.jillstuart.beauty.app.common.listener.LeoListener;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataJsonListListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogListSelectCompleteListener;
import com.jillstuart.beauty.app.common.view.DynamicImageViewWithLoading;
import com.jillstuart.beauty.app.data.JsonDataControll;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponDetailTopView extends CouponDetailBaseSubView {

	// View
	private TextView mTitle;
	private DynamicImageViewWithLoading mImageView;
	private TextView mMainDesc;
	private Button mBtnUse;
	private Button mBtnChange;

	// data
	private boolean isUsed;
	private OffersCoupon mCoupon;

	// クーポン店舗選択
	private List<HashMap<String, String>> shopList;
	private List<String> shopNameList;
	private int selectIndex = Constant.UNSELECT_INDEX;
	private String couponShopCode;

	public CouponDetailTopView(Context context) {
		super(context);
		initialize();
	}

	public CouponDetailTopView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public CouponDetailTopView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.coupon_top_view);

		attributes.recycle();

		inflate(getContext(), R.layout.offer_coupon_view_detail_top, this);

		mTitle = (TextView) findViewById(R.id.coupon_detail_title);
		mImageView = (DynamicImageViewWithLoading) findViewById(R.id.coupon_detail_image);

		mMainDesc = (TextView) findViewById(R.id.coupon_detail_mainDesc);

		mBtnUse = (Button) findViewById(R.id.coupon_detail_btn_use);
		mBtnChange = (Button) findViewById(R.id.coupon_detail_btn_change);
	}

	public void setData(OffersCoupon coupon, boolean isUsed, LeoListener listener) {
		mCoupon = coupon;
		this.isUsed = isUsed;

		// views
		mTitle.setText(mCoupon.getTitle());
		mMainDesc.setText(mCoupon.getDescription());

		// 店舗選択ボタン
		if (this.isUsed == false && categroy == CouponCategroy.Change) {
			mBtnChange.setVisibility(View.VISIBLE);
		} else {
			mBtnChange.setVisibility(View.GONE);
		}

		// 利用ボタン
		setBtnUseEnable(!this.isUsed);

		setImageView(listener);

		if (CouponCategroy.Change == categroy) {
			// 配送店舗
			getShopList(false);
		}
	}

	// public void setBtnChangeWithSelectedTitle(String title){
	// if (CheckHelper.isStringEmpty(title)) {
	// //
	// mBtnChange.setText(getContext().getString(R.string.LBL_BTN_CP_CHOOSE));
	// // setBtnChangeEnable(true);
	// mBtnChange.setVisibility(View.GONE);
	// }else {
	// mBtnChange.setText(title);
	// setBtnChangeEnable(false);
	// }
	// }

	@SuppressWarnings("deprecation")
	private void setBtnChangeEnable(boolean isEnable) {
		// クーポン利用
		mBtnChange.setEnabled(isEnable);

		if (isEnable) {
			mBtnChange.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_btn_enable));
		} else {
			mBtnChange.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_btn_unenable));
		}
	}

	@SuppressWarnings("deprecation")
	public void setBtnUseEnable(boolean isEnable) {
		// クーポン利用
		mBtnUse.setEnabled(isEnable);

		if (isEnable) {
			mBtnUse.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_btn_enable));
		} else {
			mBtnUse.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_btn_unenable));
		}
	}

	/**
	 * クーポンの画像設定
	 */
	private void setImageView(final LeoListener listener) {

		String imageUrl = mCoupon.getThumbnailImage();
		new ImageDownloadHepler(imageUrl, mImageView.getImageView(), mImageView.getImageProgressBar(),
				new OnLeonisImageDownloadCompleteListener() {

					@Override
					public void OnImageDownloadComplete(Bitmap bitmap, ImageView imageView) {
						if (listener != null) {
							listener.onEnd();
						}
					}

				});
	}

	private void showShopList() {
		if (shopList == null) {
			// load data
			getShopList(true);
			return;
		}

		DialogHelper.showAlertViewWithData(Alert_Type.List_Select, activity, null, null, shopNameList, selectIndex,
				new LeoDialogListSelectCompleteListener() {

					@Override
					public void OnDialogListSelectComplete(int index) {
						selectIndex = index;
						if (index != Constant.UNSELECT_INDEX) {
							HashMap<String, String> selectMap = shopList.get(index);

							String shopName = selectMap.get(Constant.KEY_DATA_VALUE);
							mBtnChange.setText(shopName);
							setBtnChangeEnable(false);

							couponShopCode = selectMap.get(Constant.KEY_DATA_KEY);
						}
					}

				});

	}

	private void getShopList(final boolean completedShowList) {
		JsonDataControll.getDataJson(Data_Type.API_Get_Json_Coupon_Delivery_Shops, activity,
				new LeoDataJsonListListener() {

					@Override
					public void onRequstDataComplete(Request_Status status, List<HashMap<String, String>> datas) {

						if (status != Request_Status.Success) {
							return;
						}

						shopList = datas;
						getShopNameList();

						if (completedShowList) {
							showShopList();
						}
					}

				});
	}

	private void getShopNameList() {
		shopNameList = new ArrayList<String>();
		for (HashMap<String, String> value : shopList) {
			shopNameList.add(value.get(Constant.KEY_DATA_VALUE));
		}
	}

	// ******
	public CouponCategroy getCategroy() {
		return categroy;
	}

	public void setCategroy(CouponCategroy categroy) {
		this.categroy = categroy;
	}

	public void setBtnUseClickListener(final LeoViewOnClickListener listener) {
		if (this.activity == null) {
			return;
		}

		mBtnUse.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				if (categroy == CouponCategroy.Change && CheckHelper.isStringEmpty(couponShopCode)) {
					// show err msg
					DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, activity,
							getContext().getString(R.string.MSG_ERR_CP_USE_CHOOSE_DILIVERY_SHOP), null, null);
				} else {
					listener.onSingleClick(v);
				}
			}
		});
	}

	public String getCouponShopCode() {
		return couponShopCode;
	}

	public void setActivity(BaseActionBarActivity activity) {
		this.activity = activity;

		mBtnChange.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				// open coupon shop select
				showShopList();
			}
		});
	}
}
