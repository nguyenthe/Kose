package com.jillstuart.beauty.app.common.helper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.util.TypedValue;

import com.jillstuart.beauty.app.common.Constant;

public class ConvertHelper {

	public static String trim(Object obj) {
		if (obj == null) {
			return Constant.STRING_EMPTY;
		}

		return obj.toString().trim();
	}

	public static String toString(Object obj) {
		return trim(obj);
	}

	public static Integer toInteger(Object obj) {
		String str = toString(obj);
		
		if (CheckHelper.isStringEmpty(str)) {
			return 0;
		}
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			return 0;
		}
	}

	public static Boolean toBoolean(Object obj) {
		String str = toString(obj);

		if (CheckHelper.isStringEmpty(str)) {
			return false;
		}
		try {
			return Boolean.valueOf(str);
		} catch (Exception e) {
			return false;
		}
	}

	public static float toDIP(Context context, float targetSize) {

		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				targetSize, context.getResources().getDisplayMetrics());
		// return TypedValue.COMPLEX_UNIT_SP,14;
	}

	public static String ListToString(List<String> list, String Sep) {
		if (list == null) {
			return Constant.STRING_EMPTY;
		}
		
		StringBuilder sbBuilder = new StringBuilder();
		String delim = Constant.STRING_EMPTY;
		for (String i : list) {
			sbBuilder.append(delim).append(i);
			delim = Sep;
		}

		return sbBuilder.toString();
	}

	public static List<String> JsonArrayToList(JSONArray jArray) {
		List<String> list = new ArrayList<String>();
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				try {
					list.add(ConvertHelper.toString(jArray.get(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}
	
	public static int[] StringsToIntList(String[] strs) {
		int[] array = new int[strs.length];
		for (int i = 0; i < array.length; i++) {
			array[i] = Integer.parseInt(strs[i]);
		}
		return array;
	}
}
