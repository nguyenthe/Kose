package com.jillstuart.beauty.app.mypage.login;

import java.util.HashSet;
import java.util.Set;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;

public class MyPageLoginEcFragment extends MyPageBaseFragment {
	private EditText mEditTextAddress;
	private EditText mEditTextId;

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_login_ec, viewgroup,
				false);
	
		initViews(view);
		
		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	private void initViews(View view){
		Button btnNextButton = (Button)view.findViewById(R.id.mypage_login_ec_btn_next);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {
			
			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});
		
		setOnFoucsChange(view);
		
		if (Config.MYPAGE_IS_INPUT_FILL) {
			mEditTextAddress.setText("tci@tci.co.jp");
			mEditTextId.setText("1234565643212");
		}
		
	}
	
	private void setOnFoucsChange(View view){
		
		mEditTextAddress = (EditText)view.findViewById(R.id.mypage_login_ec_address_edittext);
		mEditTextId = (EditText)view.findViewById(R.id.mypage_login_ec_id_edittext);
		
		setEditTextOnFoucsChangeKeyBoard(mEditTextAddress);
		setEditTextOnFoucsChangeKeyBoard(mEditTextId);
		
		setEditTextMaxLength(mEditTextAddress, Constant.INPUT_MAX_LENGTH_MAIL);
		setEditTextMaxLength(mEditTextId, Constant.MYPAGE_EC_USER_ID_LENGTH);
	}

	private boolean checkIsInputValid(){
		
		resetEditText();
		
		boolean isValid = true;
		StringBuffer msg = new StringBuffer();
		Set<View> targetsSet = new HashSet<View>();
		
		//必須
		String addressString = ConvertHelper.toString(mEditTextAddress.getText());
		if(CheckHelper.isStringEmpty(addressString)){
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),"メールアドレス"));
			targetsSet.add(mEditTextAddress);
		}else if (CheckHelper.isMail(addressString) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_IN_ERR),"メールアドレス"));
			targetsSet.add(mEditTextAddress);
		}
		
		String idString = ConvertHelper.toString(mEditTextId.getText());
		if(CheckHelper.isStringEmpty(idString)){
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),"ユーザーID"));
			targetsSet.add(mEditTextId);
		}else if (CheckHelper.isNumber(idString) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_ERR),"ユーザーID"));
			targetsSet.add(mEditTextId);
		}
		
		if (isValid == false) {
			for (View view : targetsSet) {
				setEditTextValid(view, isValid);
			}
			
			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity, null, msg.toString(), null);
			
			return false;
		}
		return true;
	}
	
	private void resetEditText(){
		setEditTextValid(mEditTextAddress, true);
		setEditTextValid(mEditTextId, true);
	}
	
	private void btnNextClick(){
		//check input
		if (checkIsInputValid() == false) {
			return;
		}
		
		openNext();
	}
	
	private void openNext(){
		String addressString = ConvertHelper.trim(mEditTextAddress.getText());
		String idString = ConvertHelper.trim(mEditTextId.getText());
		
		UserRegistInfo info = new UserRegistInfo();
		info.setType(Page_Type.MyPage_Login_Ec.getValue());
		info.setEcEmail(addressString);
		info.setEcNo(idString);
		
		MyPageLoginPreviewFragment previewFragment = new MyPageLoginPreviewFragment();
		
		Bundle bd = new Bundle();
		bd.putParcelable(Constant.KEY_FG_DATA, info);
		previewFragment.setArguments(bd);
		
		//open preview page
		super.openFragment(previewFragment);
	}
}
