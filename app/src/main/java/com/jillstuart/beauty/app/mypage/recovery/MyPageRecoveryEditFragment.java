package com.jillstuart.beauty.app.mypage.recovery;

import java.util.HashSet;
import java.util.Set;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserRecoveryInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;
import com.jillstuart.beauty.app.mypage.view.MyPageQuestionAskView;

public class MyPageRecoveryEditFragment extends MyPageBaseFragment {
	private MyPageQuestionAskView mQuesAskOld;
	private MyPageQuestionAskView mQuesAskNew;

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_recovery_edit,
				viewgroup, false);

		initViews(view);

		return view;
	}

	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	private void initViews(View view) {
		Button btnNextButton = (Button) view
				.findViewById(R.id.mypage_recovery_edit_btn_next);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});

		mQuesAskOld = (MyPageQuestionAskView) view
				.findViewById(R.id.mypage_recovery_edit_ques_ask_old);
		mQuesAskOld.setActivity(mActivity);
		mQuesAskNew = (MyPageQuestionAskView) view
				.findViewById(R.id.mypage_recovery_edit_ques_ask_new);
		mQuesAskNew.setActivity(mActivity);
	}

	private boolean checkIsInputValid() {

		resetEditText();

		boolean isValid = true;
		StringBuffer msg = new StringBuffer();
		Set<View> targetsSet = new HashSet<View>();

		// 質問　旧
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAskOld.getQuestion())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_SELECT_NOT),
					"既存の質問"));
			targetsSet.add(mQuesAskOld.getEditTextQues());
		}

		// 答え
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAskOld.getAnswer())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),
					"既存の質問の答え"));
			targetsSet.add(mQuesAskOld.getEditTextAnswer());
		}

		// 質問 新
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAskNew.getQuestion())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_SELECT_NOT),
					"新しい質問"));
			targetsSet.add(mQuesAskNew.getEditTextQues());
		}

		// 答え
		// 必須
		if (CheckHelper.isStringEmpty(mQuesAskNew.getAnswer())) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),
					"新しい質問の答え"));
			targetsSet.add(mQuesAskNew.getEditTextAnswer());
		}

		if (isValid == false) {
			for (View view : targetsSet) {
				setEditTextValid(view, isValid);
			}

			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose,
					mActivity, null, msg.toString(), null);

			return false;
		}
		return true;
	}

	private void resetEditText() {
		setEditTextValid(mQuesAskOld.getEditTextAnswer(), true);
		setEditTextValid(mQuesAskOld.getEditTextQues(), true);
		setEditTextValid(mQuesAskNew.getEditTextAnswer(), true);
		setEditTextValid(mQuesAskNew.getEditTextQues(), true);
	}

	private void btnNextClick() {
		// check input
		if (checkIsInputValid() == false) {
			return;
		}
		openNext();
	}

	private void openNext() {

		UserRecoveryInfo info = new UserRecoveryInfo();
		info.setQuesCode(mQuesAskOld.getQuestionCode());
		info.setAnswer(mQuesAskOld.getAnswer());
		info.setQuesCodeNew(mQuesAskNew.getQuestionCode());
		info.setAnswerNew(mQuesAskNew.getAnswer());
		
		JillDataControll.requestData(Data_Type.API_Post_Recovery_Edit, info, mActivity, new LeoDataAPIListener() {
			@Override
			public void onRequstDataComplete(UserInfo data) {
				afterApi(data, getString(R.string.MSG_SHOW_MP_RE_EDIT_SUCCESS_TITLE), getString(R.string.MSG_SHOW_MP_RE_EDIT_SUCCESS_TEXT), false);
			}
		});
		
		
	}
}
