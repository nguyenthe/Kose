package com.jillstuart.beauty.app.activity;

import android.annotation.SuppressLint;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.*;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.widget.*;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TabHost.TabSpec;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;

import co.leonisand.offers.OffersKit;
import co.leonisand.offers.OffersKit.OffersListener;
import co.leonisand.platform.Leonis;
import permissions.dispatcher.PermissionsDispatcher;
/*import permissions.dispatcher.PermissionsDispatcher;
import permissions.dispatcher.PermissionsDispatcher.Permission_Status;*/

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.application.MainApplication;
import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Java_Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.common.EnumConstant.Social_Type;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DataSaveHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.helper.OpenHepler;
import com.jillstuart.beauty.app.common.helper.SdkCupidoHelper;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataListListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataSingleListener;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogConfirmListener;
import com.jillstuart.beauty.app.common.listener.LeoPersmissionListener;
import com.jillstuart.beauty.app.common.view.ReclickableTabHost;
import com.jillstuart.beauty.app.common.view.ReclickableTabHost.OnReclickableCurrentTabClickListener;
import com.jillstuart.beauty.app.data.OffersDataControll;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.firebase.FirebaseMessagingService;
import com.jillstuart.beauty.app.fragment.*;
import com.jillstuart.beauty.app.mypage.MyPageFragment;
import com.jillstuart.beauty.app.offer.CouponListFragment;
import com.jillstuart.beauty.app.offer.RecomListFragment;
import com.jillstuart.beauty.app.other.OtherWebFragment;

@SuppressWarnings("deprecation")
public class MainActivity extends BaseActionBarActivity {

	private Context mContext;

	private DrawerLayout mDrawerLayout;
	private ListView mLeftDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private ArrayAdapter<Map<String, Object>> mListAdapter;
	private LayoutInflater mInflater;
	private ReclickableTabHost mTabHost;

	private Intent mNotiIntent = null;
	private LeoPersmissionListener permissionListener;
	
	public boolean onOptionsItemSelected(MenuItem menuitem) {
		boolean res = true;
		switch (menuitem.getItemId()) {
		case android.R.id.home:
			if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				getSupportFragmentManager().popBackStack();
			} else {
				mDrawerToggle.onOptionsItemSelected(menuitem);
			}
			break;
		default:
			res = super.onOptionsItemSelected(menuitem);
			break;
		}
		return res;
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		// exception処理
		detectException();
		//mNotiIntent = getIntent();
		FirebaseMessaging.getInstance().subscribeToTopic("news");
		FirebaseInstanceId.getInstance().getToken();

		System.out.println("Token Token ....."+FirebaseInstanceId.getInstance().getToken());

		mContext = this;
		setContentView(R.layout.activity_main);
		mInflater = LayoutInflater.from(mContext);

		mActionBar.setDisplayHomeAsUpEnabled(true); // ホームアイコン表示
		mActionBar.setHomeButtonEnabled(true); // ホームアイコンボタン有効

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.navigationbar_icon_others,
				R.string.drawer_open, R.string.drawer_close);
		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		//startService(new Intent(FirebaseMessagingService.class.getName()));

		mTabHost = (ReclickableTabHost) findViewById(android.R.id.tabhost);
		this.setTabEnvet();

		mTabHost.setup(mContext, getSupportFragmentManager(), R.id.container);
		this.setTabView();

		// 初期化tab index 選択
		openTabByTag(Config.TAB_SELECT_TAG);

		this.setLeftView();
		this.setLeftEnvent();
		this.setLeftData();

		Common.sendGoogleAnalyticsScreenName(MainActivity.this, "起動Android");
	}

	public void onConfigurationChanged(Configuration configuration) {
		super.onConfigurationChanged(configuration);
		mDrawerToggle.onConfigurationChanged(configuration);
	}

	protected void onPostCreate(Bundle bundle) {
		super.onPostCreate(bundle);
		mDrawerToggle.syncState();
	}

	public void onStart() {
		super.onStart();
		
		//dangerous permission 
		askPermissions();
		detectPush();

	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public void onResume() {
		super.onResume();

		refreshActionBar();

		checkVersionUpdate();

		setBadge();
	}

	public void onDestroy() {

		mTabHost.setOnTabChangedListener(null);
		mLeftDrawerList.setOnItemClickListener(null);

		super.onDestroy();
	}

	public void onStop() {
		super.onStop();
	}

	@SuppressLint("RtlHardcoded")
	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		boolean ret = super.dispatchKeyEvent(e);

		if (!ret) {
			if (e.getAction() == KeyEvent.ACTION_UP && e.getKeyCode() == KeyEvent.KEYCODE_MENU) {
				boolean drawerOpen = mDrawerLayout.isDrawerOpen(mLeftDrawerList);
				if (!drawerOpen) {
					mDrawerLayout.openDrawer(Gravity.LEFT);
				} else {
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				}

				return true;
			}
		}

		return ret;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == Constant.FLG_ACTIVITY_REQUEST_OTHER_LEFT) {
			// mDrawerLayout.closeDrawer(Gravity.LEFT);
			// FragmentManager fm = getSupportFragmentManager();
			// Fragment fragment_byTag = fm.findFragmentByTag("fragment_tag");
		}
	}

	// *****TAB BAR
	private void setTabView() {

		mTabHost.getTabWidget().setDividerDrawable(null);

		String tabTitle = null;
		String tabTag = null;
		int iconId = 0;

		// Home
		tabTitle = getString(R.string.LBL_TB_HOME);
		tabTag = Constant.TAG_TAB_HOME;
		iconId = R.drawable.tabbar_icon_home;
		this.setTabbarTextAndIcon(tabTag, tabTitle, iconId, TabHomeBasicWebFragment.class, false);

		// Shoplist
		tabTitle = getString(R.string.LBL_TB_SHOPLIST);
		tabTag = Constant.TAG_TAB_SHOPLIST;
		iconId = R.drawable.tabbar_icon_shoplist;
		this.setTabbarTextAndIcon(tabTag, tabTitle, iconId, TabShopListWebFragment.class, false);

		// News
		//Remove
	/*	tabTitle = getString(R.string.LBL_TB_NEWS);
		tabTag = Constant.TAG_TAB_NEWS;
		iconId = R.drawable.tabbar_icon_news;
		this.setTabbarTextAndIcon(tabTag, tabTitle, iconId, RecomListFragment.class, true);*/

		// coupon
		tabTitle = getString(R.string.LBL_TB_COUPON);
		tabTag = Constant.TAG_TAB_COUPON;
		iconId = R.drawable.tabbar_icon_coupon;
		this.setTabbarTextAndIcon(tabTag, tabTitle, iconId, CouponListFragment.class, true);

		// Mypage
		tabTitle = getString(R.string.LBL_TB_MYPAGE);
		tabTag = Constant.TAG_TAB_MYPAGE;
		iconId = R.drawable.tabbar_icon_mypage;
		this.setTabbarTextAndIcon(tabTag, tabTitle, iconId, MyPageFragment.class, false);

	}

	@SuppressLint("InflateParams") 
	@SuppressWarnings("rawtypes")
	private void setTabbarTextAndIcon(String tabTag, String tabTitle, int iconId, Class fragmentClass,
			boolean isSetBadge) {

		RelativeLayout mTabHome = (RelativeLayout) mInflater.inflate(R.layout.tabbar_indicator, null, false);
		mTabHome.setTag(tabTag);
		((ImageView) mTabHome.findViewById(R.id.tab_indicator_image)).setImageResource(iconId);
		TextView txtView = ((TextView) mTabHome.findViewById(R.id.tab_indicator_text));
		txtView.setText(tabTitle);
		Typeface font = Typeface.createFromAsset(getAssets(), Constant.SYS_FONT_CUSTOM_FILE);
		txtView.setTypeface(font);
		TabSpec tabSpecHome = mTabHost.newTabSpec(tabTag);
		tabSpecHome.setIndicator(mTabHome);
		Bundle bundle_home = new Bundle();
		bundle_home.putString(Constant.KEY_TAB_NAME, tabTitle);

		mTabHost.addTab(tabSpecHome, fragmentClass, bundle_home);

		if (isSetBadge) {
			this.setBageView(mTabHome);
		}
	}

	@SuppressLint("ResourceAsColor")
	private void setTabEnvet() {
		// add by tll 20141009
		// 選択しているタブバーをクリックイベントlistener追加
		mTabHost.setOnReclickableCurrentTabClickListener(new OnReclickableCurrentTabClickListener() {

			@Override
			public void OnCurrentTabClick(int index) {
				// tab bar 当tab再クリック 子画面消す
				if (isHasSub()) {
					popAllSubs();
				} else {
					// 子画面なし(root),クーポン一覧画面なら、データrefresh call
					if (index == Constant.TAB_INDEX_COUPON) {
						Intent intent = new Intent(Constant.KEY_PAGE_LOAD_DATA_FLG);
						LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
					}
				}
			}

		});

		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			@SuppressLint("DefaultLocale")
			@Override
			public void onTabChanged(String tabId) {

				// 子画面すべて消す
				popAllSubs();

				// tab bar 画像変更
				TabWidget mTabContent = (TabWidget) mTabHost.findViewById(android.R.id.tabs);
				for (int i = 0; i < mTabContent.getChildCount(); i++) {
					View mTabIndicator = mTabContent.getChildAt(i);

					// ImageView mTabBackGroundImage = (ImageView) mTabIndicator
					// .findViewById(R.id.tab_indicator_bg_image);
					ImageView mTabImage = ((ImageView) mTabIndicator.findViewById(R.id.tab_indicator_image));
					TextView mTabText = (TextView) mTabIndicator.findViewById(R.id.tab_indicator_text);

					int icon_id;
					// int background_id;
					int text_color_id;
					if (mTabIndicator.getTag().equals(tabId)) {
						text_color_id = R.color.tab_bar_text_active;
						// background_id = R.drawable.tabbar_back_active_2x;
						icon_id = getResources().getIdentifier(
								"tabbar_icon_" + mTabIndicator.getTag().toString().toLowerCase() + "_down", "drawable",
								getPackageName());

						setActionBarTitle(mTabText.getText().toString());
						// badge消す
						clearBadge(mTabIndicator.getTag());
					} else {
						text_color_id = R.color.tab_bar_text_passive;
						// background_id = R.drawable.tabbar_background_2x;
						icon_id = getResources().getIdentifier(
								"tabbar_icon_" + mTabIndicator.getTag().toString().toLowerCase(), "drawable",
								getPackageName());
					}

					// mTabBackGroundImage.setImageResource(background_id);
					mTabImage.setImageResource(icon_id);
					mTabText.setTextColor(getResources().getColor(text_color_id));

				}
			}
		});
	}

	// @SuppressLint("NewApi")
	private void setBageView(RelativeLayout mTab) {
		// バッチを追加
		TextView tab_badge_text = new TextView(mContext);
		tab_badge_text.setTag(Constant.TAG_TAB_BAG);
		tab_badge_text.setTypeface(Typeface.DEFAULT_BOLD);
		tab_badge_text.setTextSize(8);
		tab_badge_text.setPadding(8, 3, 8, 5);
		tab_badge_text.setMaxLines(1);

		tab_badge_text
				.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT) {
					{
						float center = getResources().getDisplayMetrics().widthPixels / 8;
						float d = getResources().getDisplayMetrics().density;
						setMargins((int) (center + (2 * d)), (int) (2 * d), 0, 0);
					}
				});
		tab_badge_text.setTextColor(getResources().getColor(R.color.tab_bar_badge_text));

		GradientDrawable shape = new GradientDrawable();
		shape.setCornerRadius(10 * Common.density(this));
		shape.setColor(getResources().getColor(R.color.tab_bar_badge_background));
		tab_badge_text.setBackgroundDrawable(shape);
		tab_badge_text.setText("0");
		tab_badge_text.setVisibility(View.GONE);

		mTab.addView(tab_badge_text);
	}

	// ********LEFT

	private void setLeftView() {
		// left
		mLeftDrawerList = (ListView) findViewById(R.id.left_drawer);
		mListAdapter = new ArrayAdapter<Map<String, Object>>(mContext, 0) {
			private LayoutInflater mLayoutInflater = (LayoutInflater) mContext.getSystemService("layout_inflater");

			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = mLayoutInflater.inflate(R.layout.listview_cell_other, parent, false);
				}
				TextView txt = (TextView) convertView.findViewById(android.R.id.title);

				Typeface font = Typeface.createFromAsset(getAssets(), Constant.SYS_FONT_CUSTOM_FILE);
				txt.setTypeface(font);

				// ImageView img = (ImageView) convertView
				// .findViewById(android.R.id.widget_frame);

				Map<String, Object> map = (Map<String, Object>) getItem(position);

				txt.setText((CharSequence) map.get(Constant.KEY_FG_TITLE));
				// img.setImageResource((Integer) map.get("icon"));

				return convertView;
			}
		};

		mLeftDrawerList.setAdapter(mListAdapter);
	}

	private void setLeftEnvent() {
		mLeftDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterview, View view, int i, long l) {

				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) adapterview.getItemAtPosition(i);
				if (map.get(Constant.KEY_FG_TITLE).toString().equals(getString(R.string.LBL_BTN_RESET))) {
					reset();
					return;
				}

				// if
				// (map.get(Constant.KEY_FG_TITLE).toString().equals(getString(R.string.LBL_BTN_PUSH)))
				// {
				// pushListTest();
				// return;
				// }

				openWeb(map);
			}
		});
	}

	private void openWeb(Map<String, Object> map) {

		@SuppressWarnings("unused")
		String gaName = null;
		if (map.get(Constant.KEY_FG_TITLE).toString().equals(getString(R.string.LBL_OT_TITLE_JILL))) {
			gaName = getString(R.string.LBL_GA_Other_Jill);
			OpenHepler.openOutSideWebViewConfirm(MainActivity.this, null, UrlConstant.URL_OTHER_JILL, null);
		} else if (map.get(Constant.KEY_FG_TITLE).toString().equals(getString(R.string.LBL_OT_TITLE_FACEBOOK))) {
			gaName = getString(R.string.LBL_GA_Other_Facebook);
			OpenHepler.openSocial(MainActivity.this, Social_Type.Facebook_Open, UrlConstant.URL_OTHER_FACEBOOK_USER);
		} else if (map.get(Constant.KEY_FG_TITLE).toString().equals(getString(R.string.LBL_OT_TITLE_INSTAGRAM))) {
			gaName = getString(R.string.LBL_GA_Other_Instagram);
			OpenHepler.openSocial(MainActivity.this, Social_Type.Instagram_Open, UrlConstant.URL_OTHER_INSTAGRAM_USER);
		}

		// GA
		// Common.sendGoogleAnalyticsScreenName(MainActivity.this, gaName);
	}

	private void setLeftData() {
		mListAdapter.clear();
		Map<String, Object> hashmap;
		hashmap = new HashMap<String, Object>();
		// Jill
		hashmap = new HashMap<String, Object>();
		hashmap.put(Constant.KEY_FG_TITLE, getString(R.string.LBL_OT_TITLE_JILL));
		hashmap.put(Constant.KEY_FG_TYPE, Page_Type.Other_Jill.getValue());
		hashmap.put(Constant.KEY_FG_CLASS_NAME, OtherWebFragment.class);
		mListAdapter.add(hashmap);

		// Facebook
		hashmap = new HashMap<String, Object>();
		hashmap.put(Constant.KEY_FG_TITLE, getString(R.string.LBL_OT_TITLE_FACEBOOK));
		hashmap.put(Constant.KEY_FG_TYPE, Page_Type.Other_Facebook.getValue());
		hashmap.put(Constant.KEY_FG_CLASS_NAME, OtherWebFragment.class);
		mListAdapter.add(hashmap);

		// Instagram
		hashmap = new HashMap<String, Object>();
		hashmap.put(Constant.KEY_FG_TITLE, getString(R.string.LBL_OT_TITLE_INSTAGRAM));
		hashmap.put(Constant.KEY_FG_TYPE, Page_Type.Other_Instagram.getValue());
		hashmap.put(Constant.KEY_FG_CLASS_NAME, OtherWebFragment.class);
		mListAdapter.add(hashmap);

		if (Config.USER_IS_CAN_RESET) {
			// リセット
			hashmap = new HashMap<String, Object>();
			hashmap.put(Constant.KEY_FG_TITLE, getString(R.string.LBL_BTN_RESET));
			mListAdapter.add(hashmap);

			// プッシュ通知テスト
			hashmap = new HashMap<String, Object>();
			hashmap.put(Constant.KEY_FG_TITLE, getString(R.string.LBL_BTN_PUSH));
			mListAdapter.add(hashmap);
		}

	}

	/**
	 * 子画面すべて消す
	 */
	public void popAllSubs() {
		FragmentManager fm = getSupportFragmentManager();
		for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {
			// スタック除去
			getSupportFragmentManager().popBackStack(fm.getBackStackEntryAt(entry).getId(),
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}

	private boolean isHasSub() {
		FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void refreshActionBar() {

		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			mActionBar.setHomeAsUpIndicator(R.drawable.navigationbar_icon_back);
		} else {
			mActionBar.setHomeAsUpIndicator(R.drawable.navigationbar_icon_others);
		}

	}

	private void setBadge() {

		// edit by tll 20160318
		// 未読取得
		// ローカルに取り込んでから未読取得
		OffersDataControll.getData(Data_Type.Offer_Coupon_List, null, MainActivity.this, new LeoDataListListener() {

			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {
				Integer couponCount = OffersKit.getInstance().unreadCouponsCount();
				String couponText = couponCount > 0 ? Constant.SHOW_OFFER_BADGE_TEXT : Constant.STRING_EMPTY;
				setCouponBadegCount(Constant.TAG_TAB_COUPON, couponText);
			}

		});

		OffersDataControll.getData(Data_Type.Offer_Recom_List, null, MainActivity.this, new LeoDataListListener() {

			@Override
			public <T> void onRequstDataComplete(Request_Status status, List<T> datas) {
				Integer recomCount = OffersKit.getInstance().unreadRecommendationsCount();
				String recomText = recomCount > 0 ? Constant.SHOW_OFFER_BADGE_TEXT : Constant.STRING_EMPTY;
				setCouponBadegCount(Constant.TAG_TAB_NEWS, recomText);
			}

		});
	}

	private void clearBadge(Object tag) {
		if (CheckHelper.isStringEmpty(tag)) {
			return;
		}
		String tagString = tag.toString();
		if (Constant.TAG_TAB_NEWS.equals(tagString) || Constant.TAG_TAB_COUPON.equals(tagString)) {
			this.setCouponBadegCount(tagString, Constant.STRING_EMPTY);
		}
	}

	public void setCouponBadegCount(String tag, String text) {

		try {
			TabWidget mTabContent = (TabWidget) mTabHost.findViewById(android.R.id.tabs);
			View mTabIndicator = mTabContent.findViewWithTag(tag);
			View tagView = mTabIndicator.findViewWithTag(Constant.TAG_TAB_BAG);
			if (tagView == null) {
				return;
			}
			TextView mTabIndicatorBadgeText = (TextView) tagView;

			if (CheckHelper.isStringEmpty(text) == false) {
				mTabIndicatorBadgeText.setText(text);
				mTabIndicatorBadgeText.setVisibility(View.VISIBLE);
			} else {
				mTabIndicatorBadgeText.setText(Constant.STRING_EMPTY);
				mTabIndicatorBadgeText.setVisibility(View.GONE);
			}
		} catch (Exception e) {
		}

	}

	@Override
	public void onBackStackChanged() {
		super.onBackStackChanged();

		refreshActionBar();
	}

	public int getTabHostHeight() {
		return mTabHost.getTabWidget().getHeight();
	}

	private void reset() {

		DialogHelper.showAlertView(Alert_Type.Msg_Confirm, MainActivity.this,
				getString(R.string.MSG_ASK_COUPON_REST_TITLE), getString(R.string.MSG_ASK_COUPON_REST_MSG),
				new LeoDialogConfirmListener() {

					@Override
					public void onOkClick() {
						doReset();
					}

					@Override
					public void OnCancelClick() {

					}
				});
	}

	@SuppressLint({ "TrulyRandom", "RtlHardcoded" })
	private void doReset() {

		Leonis.getInstance().resetAll();
		OffersKit.getInstance().resetAll();
		OffersKit.getInstance().requestsCancel();

		// 擬似UID
		String newUid = new BigInteger(64, new SecureRandom()).toString(16);
		OffersKit.getInstance().setUid(newUid);

		OffersKit.getInstance().authenticationToken(new OffersListener() {
			@Override
			public void onDone(Map<String, Object> arg0) {
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}

			@Override
			public void onFail(Integer arg0) {
				showErrorMessage(null, getString(R.string.MSG_RESET_FAILURE));
			}
		});
	}

	public void openTabByTag(String tag) {
		// 初期化tab index 選択
		mTabHost.setCurrentTabByTag(tag);
	}

	public void openTabByTagForPointCoupon() {
		// 初期化tab index 選択
		DataSaveHelper.saveLocal(getApplicationContext(), Constant.KEY_LOCAL_SAVE_OFFER_LIST_IS_POINT_COUPON, true,
				Java_Data_Type.Boolean);
		mTabHost.setCurrentTabByTag(Constant.TAG_TAB_COUPON);
	}

	public void connectPointProgramIdToOffer() {
		((MainApplication) MainActivity.this.getApplication()).initSDK();
	}

	// *********** exception ************
	protected void detectException() {

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				((MainApplication) getApplication()).handleUncaughtException(thread, e);
			}
		});
	}

	// ******** プッシュ通知 ********

	private void detectPush() {

		if (mNotiIntent == null || mNotiIntent.getExtras() == null) {
			return;
		}

		if (mNotiIntent.getExtras().getBoolean(SdkCupidoHelper.CUPIDO_KEY_FLG) == false) {
			return;
		}

		int cupidoId = mNotiIntent.getExtras().getInt(Constant.KEY_FG_ID);
		// クリア
		mNotiIntent.removeExtra(SdkCupidoHelper.CUPIDO_KEY_FLG);

		SdkCupidoHelper.detectPush(cupidoId, new LeoDataSingleListener() {

			@Override
			public void onRequstDataComplete(Request_Status status, Object data) {
				if (status == Request_Status.Success) {
					if (CheckHelper.isStringEmpty(data) == false) {
						openTabByTag(ConvertHelper.toString(data));
					}
				}
			}
		});

	}

	// ***********バージョンチェック*************
	private void checkVersionUpdate() {

		// チェックしない
		if (Config.IS_CHECK_MARKET_VERSION_VALID == false) {
			return;
		}
		// バージョンチェック
		try {
			String packageName = getPackageName();
			PackageInfo packageInfo = getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);

			new CheckHelper().checkVersionUpdate(packageName, packageInfo.versionName, MainActivity.this);

		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

	}

	//*********permission ***************
	public void checkPermission(int permissionType,LeoPersmissionListener listener){
		this.permissionListener = listener;
		
		PermissionsDispatcher.checkPerssion(MainActivity.this, permissionType,true);
	}
	
	public void permissionReslut(PermissionsDispatcher.Permission_Status status){
		if (this.permissionListener != null) {
			this.permissionListener.onResult(status);
		}
	}
	
	@Override
	 public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
	            @NonNull int[] grantResults) {
		PermissionsDispatcher.checkRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	
	private void askPermissions(){
		//フラグ
		boolean isCheck = DataSaveHelper.getLocalBoolean(mContext, Constant.KEY_LOCAL_SAVE_IS_CHECKED_PERMISSON, false);
		if (isCheck) {
			return;
		}
		
		//位置
		PermissionsDispatcher.checkPerssion(MainActivity.this, PermissionsDispatcher.REQUEST_LOCATION,false);
		//フラグ
		DataSaveHelper.saveLocal(mContext, Constant.KEY_LOCAL_SAVE_IS_CHECKED_PERMISSON, true, Java_Data_Type.Boolean);
	}
}