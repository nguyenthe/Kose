package com.jillstuart.beauty.app.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.jillstuart.beauty.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Movie> moviesList;
    private Context context;
    Animation slideUpAnimation, slideDownAnimation;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            imageView = (ImageView) view.findViewById(R.id.image);
        }
    }


    public MoviesAdapter(List<Movie> moviesList,Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Movie movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        Picasso.with(context)
                .load(movie.getImage())
                .resize(200, 200)
                .transform(new RoundedTransformation())
                .into(holder.imageView);

       /* Animation animation = AnimationUtils.loadAnimation(context,
                (position > 4) ? R.anim.up_from_bottom
                        : R.anim.slide_down);
        holder.itemView.startAnimation(animation);*/
       // 4 = position;

       /* slideDownAnimation = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imageView.startAnimation(slideDownAnimation);
            }
        });*/
    }

   /* @Override
    public void onViewAttachedToWindow(MyViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.imageView.clearAnimation();
    }*/


    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
