package com.jillstuart.beauty.app.common.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseWebViewNormalFragment extends BaseWebViewFragment {

	
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
	}

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {

		super.isHasBrowseBar = true;
		return super.onCreateView(layoutinflater, viewgroup, bundle);
	}

	@Override
	protected void setWebView() {
		
	}


}
