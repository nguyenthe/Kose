package com.jillstuart.beauty.app.offer.view;

import co.leonisand.leonis.Image.ImageListener;
import co.leonisand.offers.OffersCoupon;
import co.leonisand.offers.OffersStatus;
import co.leonisand.offers.OffersKit.OffersKitStatusCode;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.CouponCategroy;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.DateHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponDetailUsedView extends CouponDetailBaseSubView {

	private TextView mUsedMessage;
	private ImageView mSuccessImage;
	private TextView mUsedDate;
	private TextView mSuccessLbl;

	// data
	private OffersCoupon mCoupon;
	private Bitmap mSuccessBitmap;

	public CouponDetailUsedView(Context context) {
		super(context);
		initialize();
	}

	public CouponDetailUsedView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public CouponDetailUsedView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs,
				R.styleable.coupon_used_view);

		attributes.recycle();

		inflate(getContext(), R.layout.offer_coupon_view_detail_used, this);

		mUsedMessage = (TextView) findViewById(R.id.coupon_detail_used_message);
		mUsedDate = (TextView) findViewById(R.id.coupon_detail_used_time);

		mSuccessImage = (ImageView) findViewById(R.id.coupon_detail_used_image);
		mSuccessLbl = (TextView) findViewById(R.id.coupon_detail_used_thankyou_lbl);
	}

	public void setData(OffersCoupon coupon, OffersStatus offersstatus) {
		mCoupon = coupon;

		setUsedMsg(offersstatus);
		setUsedDate();
		setUsedImage(offersstatus);

		if (categroy == CouponCategroy.Change) {
			mSuccessBitmap = null;
			return;
		}
		//TODO image download 順番
		coupon.applySuccessImageBitmap(mSuccessImage, new ImageListener() {
			public void onDone(View view, Bitmap bitmap) {
				mSuccessBitmap = bitmap;
			}
		});
	}

	/**
	 * クーポン利用後メッセージ
	 * 
	 * @param offersstatus
	 */
	private void setUsedMsg(OffersStatus offersstatus) {
		String msg = Constant.STRING_EMPTY;

		if (offersstatus == null) {
			mUsedMessage.setText(msg);
			return;
		}

		// 成功
		if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusSuccess)) {
			msg = getSuccessMsg();
			// 無効化されたクーポン
		} else if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusInvalidCoupon)) {
			msg = mCoupon.getStatusInvalidMsg();
			// 上限超過
		} else if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusAvailableLimitOver)) {
			msg = mCoupon.getOverAvailableMsg();
			// 使用期間外
		} else if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusOutOfService)) {
			msg = mCoupon.getUnavailableTermMsg();
			// 再利用期間未満
		} else if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusLockedCoupon)
				&& mCoupon.getReusable() == true) {
			String dateString = DateHelper
					.dateFormatOfferDateStringToFormatString(
							mCoupon.getScheduledReusableTime(),
							"yyyy年MM月dd日 HH:mm:ss");
			msg = String.format(
					getContext().getString(
							R.string.MSG_ERR_COUPON_USE_REUSE_UNLOCKED),
					dateString);
		} else {
			msg = offersstatus.getCodeMessage();
		}

		if (CheckHelper.isStringEmpty(msg)) {
			String.format(getContext().getString(R.string.MSG_ERR_CP_OTHER), offersstatus.getCode());
		}

		mUsedMessage.setText(msg);
	}

	private String getSuccessMsg() {
		String msg = mCoupon.getAfterUseMsg();

		if (isPointShow()) {
			if (this.point > 0) {
				msg = getContext().getString(
						R.string.LBL_CP_AFTER_USE_POINT_PLUS);
			} else {
				msg = getContext().getString(
						R.string.LBL_CP_AFTER_USE_POINT_MIUS);
			}
		}

		return msg;
	}

	private void setUsedDate() {
		mUsedDate.setText(DateHelper.dateFormatOfferDateStringToFormatString(
				mCoupon.getUsedDate(),
				getContext().getString(R.string.format_coupon_use_date)));
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("DefaultLocale")
	private void setUsedImage(OffersStatus offersstatus) {
		if (offersstatus
				.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusSuccess)) {
			if (mSuccessBitmap != null) {
				mSuccessImage.setImageBitmap(mSuccessBitmap);
				mSuccessImage.setVisibility(View.VISIBLE);
				mSuccessLbl.setVisibility(View.GONE);
			} else {
				mSuccessImage.setVisibility(View.GONE);
				mSuccessLbl.setVisibility(View.VISIBLE);
				
				//通常クーポン以外：Thank you
				if (isPointShow() == false) {
					mSuccessLbl.setText(getContext().getString(
							R.string.LBL_CP_AFTER_USE_MSG));
					mSuccessLbl.setTextColor(getContext().getResources().getColor(R.color.sys_color));
				} else {
					//交換クーポン、ポイントクーポン：point
					int p = this.point > 0 ? this.point : this.point * -1;
					String msg = String.format("%d pt", p);
					mSuccessLbl.setText(msg);
					mSuccessLbl.setTextColor(getContext().getResources().getColor(R.color.sys_font_color_text));
				}
			}
		} else {
			mSuccessImage
					.setImageResource(R.drawable.coupon_used_icon_failed_2x);
			mSuccessImage.setVisibility(View.VISIBLE);
			mSuccessLbl.setVisibility(View.GONE);
		}
	}

	public void setActivity(BaseActionBarActivity activity) {
		this.activity = activity;
		 Common.setCustomFont(activity, mSuccessLbl);
	}
}
