package com.jillstuart.beauty.app.application;

import java.util.Map;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.KoseHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import co.leonisand.cupido.CupidoKit;
import co.leonisand.offers.OffersKit;
import co.leonisand.offers.OffersKit.OffersListener;
import co.leonisand.cupido.CupidoKit.GCMRegisterListener;
import co.leonisand.cupido.CupidoKit.CupidoListener;
import co.leonisand.cupido.CupidoStatus;
import co.leonisand.platform.Leonis;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.util.SparseArray;

public class MainApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				handleUncaughtException(thread, e);
			}
		});

		// init
		this.initSDK();

		// add by tll 20141001
		// set image download option
		this.initImageLoader();


	}
	/*@Override
	protected void attachBaseContext(Context context) {
		super.attachBaseContext(context);
		MultiDex.install(this);
	}*/

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	public void initSDK() {
		// SDKの初期化
		// Leonisの初期化
		String uid = KoseHelper.getProgramId(getApplicationContext());
		int timeOut = 10;
		if (CheckHelper.isStringEmpty(uid)) {
			Leonis.getInstance().init(getApplicationContext(), true, timeOut);
		} else {
			Leonis.getInstance().init(getApplicationContext(), uid, true,
					timeOut);
		}
		// System.out.println(Leonis.getInstance().uid());

		// OffersKitの初期化
		OffersKit.getInstance().init(getApplicationContext());
		OffersKit.getInstance().setRequestTimeoutInterval(10);
		//TODO offerskit local max count
		
		// メッセージ設定
		this.setOffersKitMsg();

		// // StampKitの初期化
		// StampKit.getInstance().init(getApplicationContext());

		// CupidoKitの初期化
		//initCupidoSdk();

		// チェーンクーポン
		initCupidoChainCoupon();

	}

	private void initCupidoSdk() {

		CupidoKit.getInstance().init(getApplicationContext());

		// デバイストークン未取得
		if (CheckHelper.isStringEmpty(CupidoKit.getInstance()
				.getRegistrationId())) {
			// デバイストークン取得
			CupidoKit.getInstance().gcmRegister(new GCMRegisterListener() {
				@Override
				public void onRegistered(String registrationId) {
					// System.out.println("gcmRegister.onRegistered:"+registrationId);

					// 認証トークン確認
					if (CupidoKit.getInstance().validateAuthenticationToken()) {
						// デバイストークン更新
						CupidoKit.getInstance().deviceUpdate(
								new CupidoListener() {
									@Override
									public void onDone(
											Map<String, Object> result) {
										CupidoStatus status = (CupidoStatus) result
												.get("status");
										if (status == null) {
											System.out
													.println("deviceUpdate.onDone:"
															+ "取得できませんでした。");
										}
									}

									@Override
									public void onFail(String message) {
										System.out
												.println("deviceUpdate.onFail:"
														+ message);
									}
								});
					} else {
						// デバイストークン登録
						CupidoKit.getInstance().deviceRegist(
								new CupidoListener() {
									@Override
									public void onDone(
											Map<String, Object> result) {
										CupidoStatus status = (CupidoStatus) result
												.get("status");
										if (status == null) {
											System.out
													.println("deviceRegist.onDone:"
															+ "取得できませんでした。");
										}
									}

									@Override
									public void onFail(String message) {
										System.out
												.println("deviceRegist.onFail:"
														+ message);
									}
								});
					}
				}

				@Override
				public void onError(String errorId) {
					System.out
							.println("setGCMRegistListener.onError" + errorId);
				}

				@Override
				public void onRecoverableError(String errorId) {
					System.out
							.println("setGCMRegistListener.ononRecoverableErrorError"
									+ errorId);
				}
			});
			// 認証トークンの妥当性検証
		} else if (!CupidoKit.getInstance().validateAuthenticationToken()) {
			// デバイストークン登録
			CupidoKit.getInstance().deviceRegist(new CupidoListener() {
				@Override
				public void onDone(Map<String, Object> result) {
					CupidoStatus status = (CupidoStatus) result.get("status");
					if (status == null) {

						System.out.println("deviceRegist.onDone:"
								+ "取得できませんでした。");
					}
				}

				@Override
				public void onFail(String message) {
					System.out.println("deviceRegist.onFail:" + message);
				}
			});
		}
	}

	private void initCupidoChainCoupon() {
		final CupidoKit mCupidoInstance = CupidoKit.getInstance();

		OffersKit.getInstance().linkUserWithCupido(mCupidoInstance,
				new OffersListener() {

					@Override
					public void onDone(Map<String, Object> map) {

					}

					@Override
					public void onFail(Integer code) {
						Log.e("チェーンクーポン", "チェーンクーポン失敗");
					}

				});
	}

	/**
	 * 画像ダウンロードラブラリ　download option 設定 option
	 * 参考：:https://github.com/nostra13/Android-Universal-Image-Loader
	 */
	/*
	 * private DisplayImageOptions initImageDownloadOptions() {
	 * DisplayImageOptions options = new DisplayImageOptions.Builder() //
	 * .showImageForEmptyUri(R.drawable.ic_empty)　//既定値利用 //
	 * .showImageOnFail(R.drawable.ic_error)　//既定値利用
	 * .resetViewBeforeLoading(true).cacheOnDisk(true) //
	 * .bitmapConfig(Bitmap.Config.RGB_565)　//既定値利用
	 * .considerExifParams(true).build();
	 * 
	 * return options; }
	 */

	/**
	 * 画像ダウンロードラブラリ　instance option 設定 option
	 * 参考：:https://github.com/nostra13/Android-Universal-Image-Loader
	 */
	private void initImageLoader() {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		// すべてデフォルト値
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this.getApplicationContext())
		// .diskCache(new UnlimitedDiscCache(cacheDir)) // default
		// .diskCacheFileCount(100)
		// .diskCacheSize(50 * 1024 * 1024) // 50MB
		// .defaultDisplayImageOptions(initImageDownloadOptions())
				.build();

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

	/**
	 * OffersKit メッセージ設定
	 */
	private void setOffersKitMsg() {
		SparseArray<String> STATUS_CODE_MESSAGES = new SparseArray<String>();

		// 0,701,710 管理画面のメッセージ

		// このクーポンは既に使用済みです。
		STATUS_CODE_MESSAGES.put(700,
				getString(R.string.MSG_ERR_CP_ALREADLY_USED));

		// このクーポンは現在ご利用になれません。\n申し訳ございません。
		STATUS_CODE_MESSAGES.put(720, getString(R.string.MSG_ERR_CP_INVALID));

		// このクーポンは一時的にご利用できなくなっています。
		STATUS_CODE_MESSAGES.put(721, getString(R.string.MSG_ERR_CP_SUSPENDED));

		// 暗証番号が正しくありません。もう一度入力をご確認ください。
		STATUS_CODE_MESSAGES.put(410,
				getString(R.string.MSG_ERR_CP_PASSWORD_NOT_CORRECT));

		// このキャンペーンコードは無効になっています。
		STATUS_CODE_MESSAGES.put(751,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_INVALID));

		// このキャンペーンコードは既にご利用済みです。
		STATUS_CODE_MESSAGES.put(752,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_ALREDALY_USED));

		// キャンペーンコードを確認できません。\n有効期限内かご確認ください。
		STATUS_CODE_MESSAGES.put(753,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_OUT_OF_PERIOD));

		// キャンペーンコード入力失敗回数が上限を超えたため、ロックアウトされました。\n翌日以降にあらためて操作してください。
		STATUS_CODE_MESSAGES.put(754,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_BLOCKED));

		// キャンペーンコードを確認できません。\nキャンペーンコードに誤りがないかご確認ください。
		STATUS_CODE_MESSAGES.put(151,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_NOT_CORRECT));

		// このキャンペーンコードは失効になっています。
		STATUS_CODE_MESSAGES.put(761,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_DISPOSE));

		// このキャンペーンコードは一時的にご利用できなくなっています。
		STATUS_CODE_MESSAGES.put(762,
				getString(R.string.MSG_ERR_CP_CAMPAIGN_CODE_SUSPENDED));

		// データがありません。(クーポンとレコメンデーション別々で画面のことろで設定)
		STATUS_CODE_MESSAGES.put(100, getString(R.string.MSG_ERR_NO_DATA));

		// 50?
		// ほかのエラー
		STATUS_CODE_MESSAGES.put(101,String.format(getString(R.string.MSG_ERR_CP_OTHER), "101"));
		STATUS_CODE_MESSAGES.put(300,String.format(getString(R.string.MSG_ERR_CP_OTHER), "300"));
		STATUS_CODE_MESSAGES.put(400,String.format(getString(R.string.MSG_ERR_CP_OTHER), "400"));
		STATUS_CODE_MESSAGES.put(420,String.format(getString(R.string.MSG_ERR_CP_OTHER), "420"));
		STATUS_CODE_MESSAGES.put(500,String.format(getString(R.string.MSG_ERR_CP_OTHER), "500"));
		STATUS_CODE_MESSAGES.put(800,String.format(getString(R.string.MSG_ERR_CP_OTHER), "800"));
		STATUS_CODE_MESSAGES.put(950,String.format(getString(R.string.MSG_ERR_CP_OTHER), "950"));
		STATUS_CODE_MESSAGES.put(999,String.format(getString(R.string.MSG_ERR_CP_OTHER), "999"));
		STATUS_CODE_MESSAGES.put(-100,String.format(getString(R.string.MSG_ERR_CP_OTHER), "-100"));
		STATUS_CODE_MESSAGES.put(-200,String.format(getString(R.string.MSG_ERR_CP_OTHER), "-200"));
		STATUS_CODE_MESSAGES.put(-256,String.format(getString(R.string.MSG_ERR_CP_OTHER), "-256"));
		STATUS_CODE_MESSAGES.put(-512,String.format(getString(R.string.MSG_ERR_CP_OTHER), "-512"));
		STATUS_CODE_MESSAGES.put(-1024,String.format(getString(R.string.MSG_ERR_CP_OTHER), "-1024"));

		OffersKit.STATUS_CODE_MESSAGES = STATUS_CODE_MESSAGES;
	}

	public void handleUncaughtException(Thread thread, Throwable e) {
		e.printStackTrace(); // not all Android versions will print the stack
								// trace automatically

//		//
//		 Intent intent = new Intent ();
////		 intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
//		 intent.setFlags (Intent.FLAG_ACTIVITY_NEW_TASK); // required when
////		 starting from Application 
//		 startActivity (intent);
//
//		 System.exit(1); // kill off the crashed app
		
		
	}

	public synchronized Tracker getTracker() {

		try {
			final GoogleAnalytics googleAnalytics = GoogleAnalytics
					.getInstance(this);
			googleAnalytics.setLocalDispatchPeriod(60);
//			return googleAnalytics.newTracker(Config.GOOGLE_ANALYTICS_KEY);
			return googleAnalytics.newTracker(R.xml.ga_global_tracker);

		} catch (final Exception e) {
		}

		return null;
	}
	
	
}
