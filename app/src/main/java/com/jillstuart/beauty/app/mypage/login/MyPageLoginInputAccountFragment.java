package com.jillstuart.beauty.app.mypage.login;

import java.util.HashSet;
import java.util.Set;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Data_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.EnumConstant.Date_Type;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.common.listener.LeoDataListener.LeoDataAPIListener;
import com.jillstuart.beauty.app.data.JillDataControll;
import com.jillstuart.beauty.app.data.model.UserInfo;
import com.jillstuart.beauty.app.data.model.UserRegistInfo;
import com.jillstuart.beauty.app.mypage.MyPageBaseFragment;

public class MyPageLoginInputAccountFragment extends MyPageBaseFragment {

	private EditText mEditTextBirthday;
	private EditText mEditTextMemoMonth;
	private EditText mEditTextMemoDay;

	// data
	private UserRegistInfo info;

	public View onCreateView(LayoutInflater layoutinflater,
			ViewGroup viewgroup, Bundle bundle) {
		
		super.onCreateView(layoutinflater, viewgroup, bundle);
		
		View view = layoutinflater.inflate(R.layout.mypage_login_input_account,
				viewgroup, false);

		info = getArguments().getParcelable(Constant.KEY_FG_DATA);

		initViews(view);

		return view;
	}

	private void initViews(View view) {
		Button btnNextButton = (Button) view
				.findViewById(R.id.mypage_login_input_account_next);
		btnNextButton.setOnClickListener(new LeoViewOnClickListener() {

			@Override
			public void onSingleClick(View v) {
				btnNextClick();
			}
		});

		setOnFoucsChange(view);

	}

	private void setOnFoucsChange(View view) {

		mEditTextBirthday = (EditText) view
				.findViewById(R.id.mypage_login_input_account_birthday);
		mEditTextMemoMonth = (EditText) view
				.findViewById(R.id.mypage_login_input_account_memo_month);
		mEditTextMemoDay = (EditText) view
				.findViewById(R.id.mypage_login_input_account_memo_day);

		setEditTextOnFoucsChangeKeyBoard(mEditTextBirthday);
		setEditTextOnFoucsChangeKeyBoard(mEditTextMemoMonth);
		setEditTextOnFoucsChangeKeyBoard(mEditTextMemoDay);

		setEditTextMaxLength(mEditTextBirthday,
				Constant.MYPAGE_DATE_INPUT_MAX_LENGTH);
		setEditTextMaxLength(mEditTextMemoMonth,
				Constant.MYPAGE_DATE_INPUT_MAX_LENGTH);
		setEditTextMaxLength(mEditTextMemoDay,
				Constant.MYPAGE_DATE_INPUT_MAX_LENGTH);
	}

	private boolean checkIsInputValid() {

		resetEditText();

		boolean isValid = true;
		StringBuffer msg = new StringBuffer();
		Set<View> targetsSet = new HashSet<View>();

		// 誕生月
		// 必須
		String birthdayString = ConvertHelper.toString(mEditTextBirthday
				.getText());
		if (CheckHelper.isStringEmpty(birthdayString)) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),
					"誕生月"));
			targetsSet.add(mEditTextBirthday);
		} else if (CheckHelper.isDate(birthdayString, Date_Type.Month) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_IN_ERR),
					"誕生月"));
			targetsSet.add(mEditTextBirthday);
		}

		// 記念日　月
		String monthString = ConvertHelper.toString(mEditTextMemoMonth.getText());
		if (CheckHelper.isStringEmpty(monthString)) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),
					"記念日"));
			targetsSet.add(mEditTextMemoMonth);
		} else if (CheckHelper.isDate(monthString, Date_Type.Month) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_IN_ERR),
					"記念日"));
			targetsSet.add(mEditTextMemoMonth);
		}

		// 記念日 日
		String dayString = ConvertHelper.toString(mEditTextMemoDay.getText());
		if (CheckHelper.isStringEmpty(dayString)) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_NOT),
					"記念日"));
			targetsSet.add(mEditTextMemoDay);
		} else if (CheckHelper.isDate(dayString, Date_Type.Day) == false) {
			isValid = false;
			msg.append(Constant.STRING_ENTER);
			msg.append(String.format(getString(R.string.MSG_ERR_MP_INPUT_IN_ERR),
					"記念日"));
			targetsSet.add(mEditTextMemoDay);
		}

		if (isValid == false) {
			for (View view : targetsSet) {
				setEditTextValid(view, isValid);
			}

			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose,
					mActivity, null, msg.toString(), null);

			return false;
		}
		return true;
	}

	private void resetEditText() {
		setEditTextValid(mEditTextBirthday, true);
		setEditTextValid(mEditTextMemoMonth, true);
		setEditTextValid(mEditTextMemoDay, true);
	}

	private void btnNextClick() {
		// check input
		if (checkIsInputValid() == false) {
			return;
		}

		info.setBirthday(convertDate(mEditTextBirthday.getText()));
		String month = convertDate(mEditTextMemoMonth.getText());
		String day = convertDate(mEditTextMemoDay.getText());
		info.setMemoryday(month + day);

		openNext();
	}

	@SuppressLint("DefaultLocale") private String convertDate(Object date){
		return String.format("%02d",ConvertHelper.toInteger(date));
	}
	
	private void openNext() {

		Data_Type dataType = null;
		if (info.getType() == Page_Type.MyPage_Login_Card.getValue()) {
			dataType = Data_Type.API_Post_Regist_Card;
		}else if (info.getType() == Page_Type.MyPage_Login_Ec.getValue()) {
			dataType = Data_Type.API_Post_Regist_Ec;
		}
	
		JillDataControll.requestData(dataType, info,
				mActivity, new LeoDataAPIListener() {

					@Override
					public void onRequstDataComplete(UserInfo data) {
						afterApi(data,getString(R.string.MSG_SHOW_MP_IN_SUCCESS_TITLE),getString(R.string.MSG_SHOW_MP_IN_SUCCESS_TEXT),true);
					}

				});
	}

}
