package com.jillstuart.beauty.app.common;

public class EnumConstant {

	public enum Page_Type {
		MyPage_Login_Card(10), MyPage_Login_Ec(11), Other_Jill(1), Other_Facebook(
				2), Other_Instagram(3);

		private final int value;

		private Page_Type(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Alert_Type {
		Number_Input(1), List_Select(2), Msg_Show_WithOk(3), Msg_Show_WithClose(
				4), Msg_Confirm(5);

		private final int value;

		private Alert_Type(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Request_Status {
		Success(0), Failure(-10), Error_Other(-1),Error_Net_Connect(-2), Unknow(1000);

		private final int value;

		private Request_Status(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Social_Type {
		Facebook_Open(1), Instagram_Open(5);

		private final int value;

		private Social_Type(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}
	
	
	/**
	 * メッセージ表示用、データ取得用(SDKに関係ない)
	 *
	 */
	public enum Data_Type {
		None,
		Offer_Coupon_List,Offer_Coupon_List_Point,Offer_Coupon_List_Normal,Offer_Coupon_Detail, Offer_Recom_List,Offer_Recom_Detail,
		Offer_Category_List,
		Kose_Shop_Css,
		API_Get_Json_Recovery_Ques,API_Get_Json_Coupon_Delivery_Shops,API_Get_Json_App_Version_Check,
		API_Get_User_Point,
		API_Post_Regist_Ec,API_Post_Regist_Card,API_Post_Add_Ec,API_Post_Add_Card,API_Post_Recovery,API_Post_Recovery_Edit,API_Post_ChangePoint;
		
	}
	
	public enum Date_Type {
		Year, Month, Day;
	}

	public enum CouponCategroy {
		Nomoral, Point, Change
	}
	
	public enum Java_Data_Type{
		Boolean,Integer,String
	}
}
