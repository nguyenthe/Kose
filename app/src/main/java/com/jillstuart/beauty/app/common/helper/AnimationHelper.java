package com.jillstuart.beauty.app.common.helper;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

public class AnimationHelper {
	public static void startRotateAnimation(View view) {
		RotateAnimation rotate = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);

		rotate.setDuration(1000);
		rotate.setRepeatCount(-1);
		rotate.setInterpolator(new LinearInterpolator());// make smoothly
		view.setAnimation(rotate);

	}

	public static void stopAnimation(View view) {
		view.setAnimation(null);
	}

	public static void startFadeOutAnimation(final View view,
			AnimationListener listener) {
		Animation fadeOut = new AlphaAnimation(0, 1);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // add this
		fadeOut.setDuration(5000);

		if (listener != null) {
			fadeOut.setAnimationListener(listener);
		}
		view.setAnimation(fadeOut);
		fadeOut.start();

	}

	public static void startFadeInAnimation(final View view,AnimationListener listener) {
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(5000);

		if (listener != null) {
			fadeIn.setAnimationListener(listener);
		}
		
		view.setAnimation(fadeIn);
		fadeIn.start();
		
	}

}
