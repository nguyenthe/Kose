package com.jillstuart.beauty.app.common.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.jillstuart.beauty.app.common.EnumConstant.Request_Status;
import com.jillstuart.beauty.app.data.model.UserInfo;

public interface LeoDataListener {

	public interface LeoDataListListener extends LeoDataListener {

		public <T> void onRequstDataComplete(Request_Status status, List<T> datas);

	}

	public interface LeoDataSingleListener extends LeoDataListener {

		public void onRequstDataComplete(Request_Status status, Object data);

	}

	public interface LeoDataSingleWithParameterListener extends LeoDataListener {

		public void onRequstDataComplete(Request_Status status, Map<String, Object> result);

	}
	
	public interface LeoDataCountListener extends LeoDataListener {
		public void onRequstDataCountComplete(int count);

	}

	public interface LeoDataJsonListListener extends LeoDataListener {

		public void onRequstDataComplete(Request_Status status, List<HashMap<String, String>> datas);

	}

	public interface LeoDataAPIListener extends LeoDataListener {

		public void onRequstDataComplete(UserInfo data);

	}

	public interface LeoDataAPIJsonListener extends LeoDataListener {

		public void onRequstDataComplete(JSONObject data);

	}
}
