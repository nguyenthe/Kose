package com.jillstuart.beauty.app.common;

public class JillApiConstant {

	// 結果ステータス
	public static final String API_STATUS_SUCCESS = "0000001";

	public static final String API_STATUS_ERR_KEY = "9000001";
	public static final String API_STATUS_ERR_PARA = "9000002";
	public static final String API_STATUS_ERR_SYSTEM = "9999999";
	public static final String API_STATUS_ERR_EC_EXIST = "8000002";
	public static final String API_STATUS_ERR_EC = "8000003";
	public static final String API_STATUS_ERR_CARD_COUNT_OVER = "8000004";
	public static final String API_STATUS_ERR_CARD_NUMBER_EXIST = "8000005";
	public static final String API_STATUS_ERR_USER = "8000006";
	public static final String API_STATUS_ERR_POINT_NOT_LACK = "8000007";
	public static final String API_STATUS_ERR_CARD_NUMBER_INVAILD = "8000008";

	public static final String API_STATUS_ERR_NET_CONNECT = "-99";

	public static final int API_CONNECT_TIME_OUT_MILLI = 20*1000;
	public static final int API_READ_TIME_OUT_MILLI = 20*1000;
	
	// API TEST
//	public static final String API_URL_KEY = "743732GO948P718327O6Z6HVO2KDQI4L";
//	public static final String API_URL_BASE = "https://point-test.jillstuart-beauty.jp/api/";
//	public static final String KEY_MY_PAGE_IS_LOGINED = "my_page_is_logined_test";
//	public static final String KEY_MY_PAGE_IS_LOGINED_EC = "my_page_is_logined_ec_test";
//	public static final String KEY_MY_PAGE_PROGRAM_ID = "my_page_program_id_test";
	
	// API　本番
	public static String API_URL_KEY = "743732GO948P718327O6Z6HVO2KDQI4L";
	public static String API_URL_BASE = "https://point.jillstuart-beauty.jp/api/";
	public static final String KEY_MY_PAGE_IS_LOGINED = "my_page_is_logined";
	public static final String KEY_MY_PAGE_IS_LOGINED_EC = "my_page_is_logined_ec";
	public static final String KEY_MY_PAGE_PROGRAM_ID = "my_page_program_id";
	
	// アプリ会員登録
	public static final String API_URL_LOGIN = "signup?";
	// 顧客コード登録
	public static final String API_URL_ADD_EC_OR_CARD = "/addcustomer";
	// アプリ会員認証情報変更
	public static final String API_URL_RECOVRY_EDIT = "/changeauth";
	// アプリ会員認証
	public static final String API_URL_RECOVERY = "/auth";
	// 顧客コード・ポイント照会
	public static final String API_URL_GET_POINT = "/showpoint";
	// ポイント加減算通知
	public static final String API_URL_CHANGE_POINT = "/point";

	// API PARAMETER
	// IN
	public static final String API_KEY_IN_ACKEY = "activationKey";
	public static final String API_KEY_IN_EC_MAIL = "ecMailAddress";
	public static final String API_KEY_IN_EC_CODE = "ecMemberCode";
	public static final String API_KEY_IN_CARD = "customerCode";
	public static final String API_KEY_IN_QUES_CODE = "questionCode";
	public static final String API_KEY_IN_QUES_CODE_NEW = "newQuestionCode";
	public static final String API_KEY_IN_QUES_ANSWER = "questionAnswer";
	public static final String API_KEY_IN_QUES_ANSWER_NEW = "newQuestionAnswer";
	public static final String API_KEY_IN_DAY_BIRTHDAY = "birthMonth";
	public static final String API_KEY_IN_DAY_MEMORYDAY = "memorialDay";
	public static final String API_KEY_IN_POINT = "point";
	public static final String API_KEY_IN_PROGRAM_ID = "appMemberCode";
	// OUT
	public static final String API_KEY_OUT_RESULT_CODE = "resultCode";
	public static final String API_KEY_OUT_PROGRAM_ID = "appMemberCode";
	public static final String API_KEY_OUT_EC_CODE = "ecMemberCode";
	public static final String API_KEY_OUT_CARD_CODES = "customerCode";
	public static final String API_KEY_OUT_POINT = "point";
	public static final String API_KEY_OUT_POINT_CLASS_NAME = "className";
	

}
