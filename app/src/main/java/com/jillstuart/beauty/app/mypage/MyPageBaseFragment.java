package com.jillstuart.beauty.app.mypage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.common.Common;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.EnumConstant.Alert_Type;
import com.jillstuart.beauty.app.common.fragment.BaseFragment;
import com.jillstuart.beauty.app.common.helper.DialogHelper;
import com.jillstuart.beauty.app.common.listener.LeoDialogCompleteListener.LeoDialogDismissListener;
import com.jillstuart.beauty.app.data.model.UserInfo;

public class MyPageBaseFragment extends BaseFragment {

	protected static boolean isLogined;

	public MyPageBaseFragment() {
		
	}
	
	/**
	 * 結果ページ開く
	 */
	protected void openReslutPage() {

		mActivity.dismissOverlayProgress();

		MyPageResultFragment rs = new MyPageResultFragment();
		openFragment(rs);
	}

	/**
	 * ポイント画面へ戻る
	 */
	protected void backToPointPage() {
		((MainActivity) mActivity).popAllSubs();
	}

	/**
	 * EditTextキーボード表示設定
	 * 
	 * @param view
	 */
	protected void setEditTextOnFoucsChangeKeyBoard(View view) {

		view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				Common.hiddenShowKeyBoard(mActivity, v, hasFocus);
			}
		});
	}

	/**
	 * EditText max length
	 * 
	 * @param editText
	 * @param maxLength
	 */
	protected void setEditTextMaxLength(EditText editText, int maxLength) {
		editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				maxLength) });

	}

	/**
	 * EditText 不正時のUI設定
	 * 
	 * @param view
	 * @param isValid
	 */
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	protected void setEditTextValid(View view, boolean isValid) {
		int styleId = isValid ? R.drawable.shape_edittext_border
				: R.drawable.shape_edittext_border_red;
		view.setBackgroundDrawable(getResources().getDrawable(styleId));
	}

	/**
	 * 
	 * @param info
	 * @param titleSuccess
	 *            成功した時
	 * @param textSuccess
	 *            　成功した時
	 */
	protected void afterApi(final UserInfo info, final String titleSuccess,
			final String textSuccess, final boolean isRefreshPoint) {
		openReslutPage();

		if (info.isResultSuccess()) {
			afterSuccess();

			// show result msg
			DialogHelper.showAlertView(Alert_Type.Msg_Show_WithOk, mActivity,
					titleSuccess, textSuccess,
					new LeoDialogDismissListener() {

						@Override
						public void OnDismiss() {
							backToPointPage();

							if (isRefreshPoint) {
								// refresh
								Intent intent = new Intent(
										Constant.KEY_MY_PAGE_EVENT_LOGIN);
								LocalBroadcastManager.getInstance(mActivity)
										.sendBroadcast(intent);
							}
						}

					});
		} else {
			afterNotSuccess(info);
		}

	}

	private void afterNotSuccess(UserInfo info) {
		DialogHelper.showAlertView(Alert_Type.Msg_Show_WithClose, mActivity,
				null, info.getResultMsg(), new LeoDialogDismissListener() {

					@Override
					public void OnDismiss() {
						backFragment();
					}

				});
	}

	// overrid
	protected void afterSuccess() {

	}
}
