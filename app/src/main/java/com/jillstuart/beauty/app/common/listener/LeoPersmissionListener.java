package com.jillstuart.beauty.app.common.listener;

import permissions.dispatcher.PermissionsDispatcher.Permission_Status;

public interface LeoPersmissionListener {
	public void onResult(Permission_Status status);
}
