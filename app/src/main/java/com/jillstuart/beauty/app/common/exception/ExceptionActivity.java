package com.jillstuart.beauty.app.common.exception;
import com.jillstuart.beauty.app.R;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class ExceptionActivity extends Activity {

	TextView error;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		
		setContentView(R.layout.err_page);
		error = (TextView) findViewById(R.id.error);
		error.setText(getIntent().getStringExtra("error"));
	}
}
