package com.jillstuart.beauty.app.firebase;

/**
 * Created by HongNgoc on 4/13/2017.
 */

public class ObjectReceiverData {
    private String couponID;
    private String title;
    private String image;
    private String category;
    private String couponType;

    public ObjectReceiverData() {
    }

    public String getCouponID() {
        return couponID;
    }

    public void setCouponID(String couponID) {
        this.couponID = couponID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    @Override
    public String toString() {
        return "ObjectReceiverData{" +
                "couponID='" + couponID + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", category='" + category + '\'' +
                ", couponType='" + couponType + '\'' +
                '}';
    }
}
