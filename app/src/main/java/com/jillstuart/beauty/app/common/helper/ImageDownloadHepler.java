package com.jillstuart.beauty.app.common.helper;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

	/**
	 * 画像ダウンロード (cache管理ライブラリ利用)
	 * :https://github.com/nostra13/Android-Universal-Image-Loader
	 * 
	 * @histroy created by tll 20140813
	 * 
	 */

	public class ImageDownloadHepler {
		
		public enum IMAGE_SIZE{
			AUTO(-1);

	        private final int value;

	        private IMAGE_SIZE(final int newValue) {
	            value = newValue;
	        }

	        public int getValue() { return value; }
		}
		
		//デファクト値
		private ImageView imageView = null;
		private String imageUrl = null;
		private Point imageSize = null;
		private OnLeonisImageDownloadCompleteListener imageDownListener = null;
		private ProgressBar loadingBar = null;
		/**
		 * コンストラクタ
		 */
		public ImageDownloadHepler() {

		}

		/**
		 * コンストラクタ
		 * 
		 * @param imageUrl 画像ダウンロードURL
		 * @param imageView  画像表示ImageView
		 * @param imageDownListener callback
		 */
		public ImageDownloadHepler(String imageUrl, ImageView imageView, ProgressBar loading,
				OnLeonisImageDownloadCompleteListener imageDownListener) {

			this.setParameters(imageUrl, null, imageView, imageDownListener, loading);
			this.downloadImage();
		}
		
		/**
		 * コンストラクタ with loading bar
		 * @param imageUrl
		 * @param imageView
		 * @param loading
		 */
		public ImageDownloadHepler(String imageUrl, ImageView imageView,ProgressBar loading) {
			
			this.setParameters(imageUrl, null, imageView, null, loading);
			this.downloadImage();
		}
		
		
		private void setParameters(String imageUrl, Point size,ImageView imageView,
				OnLeonisImageDownloadCompleteListener imageDownListener,ProgressBar loading){
			
			this.imageUrl = imageUrl;
			this.imageSize = size;
			this.imageView = imageView;
			this.imageDownListener = imageDownListener;
			this.loadingBar = loading;
			
		}
		
		/**
		 * Listener
		 */
		public interface OnLeonisImageDownloadCompleteListener {
			void OnImageDownloadComplete(Bitmap bitmap, ImageView imageView);
		}

		// ----------------
		/**
		 * ダウンロード
		 */
		private void downloadImage() {
			// https://github.com/nostra13/Android-Universal-Image-Loader
			// ImageLoaderいろいろなメソッドがある。sizeに関するのはtargetSize で、
			// targetSize < 元サイズの時 scale OK;
			// targetSize > 元サイズの時、NG。so カスタマイズする。
			// ImageLoader.getInstance().loadImage(imageUrl, targetSize, new
			// SimpleImageLoadingListener()

			if(CheckHelper.isStringEmpty(this.imageUrl)){
				finish(null);
				return;
			}
			
			ImageLoader.getInstance().displayImage(this.imageUrl, this.imageView, new ImageLoadingListener(){

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					starting();
				}

				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					finish(null);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view,
						Bitmap loadedImage) {
					
					afterSuccess(loadedImage);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					finish(null);
				}});

		}
		
		private void starting(){
			//show loading
			if(loadingBar != null){
				loadingBar.setVisibility(View.VISIBLE);
			}
		}
		
		private void afterSuccess(Bitmap loadedImage){
			
			if (loadedImage == null) {
				finish(null);
				
				return;
			}
			
			Bitmap img = ScaleBitmap(loadedImage);

			if (imageView != null) {
				imageView.setImageBitmap(img);
			}
			
			finish(img);
		}
		
		private void finish(Bitmap img){

			if (imageDownListener != null) {
				imageDownListener.OnImageDownloadComplete(img,imageView);
			}
			
			if(loadingBar != null){
				loadingBar.setVisibility(View.GONE);
			}
		}
		
		/**
		 * 画像サイズ調整
		 */
		private Bitmap ScaleBitmap(Bitmap bm) {

			// 画像サイズ調整なし
			if (this.imageSize == null) {
				return bm;
			}

			// 画像サイズscale計算
			float scalingFactor = this.getBitmapScalingFactor(bm);

			int scaleHeight = (int) (bm.getHeight() * scalingFactor);
			int scaleWidth = (int) (bm.getWidth() * scalingFactor);

			// 画像サイズ調整
			return Bitmap.createScaledBitmap(bm, scaleWidth, scaleHeight, true);
		}

		/**
		 * 画像サイズscale計算
		 */
		private float getBitmapScalingFactor(Bitmap bm) {
			int width = bm.getWidth();
			int height = bm.getHeight();
			float scale = 1.0f;
			
			//制限サイズなし
			if(this.imageSize.y == IMAGE_SIZE.AUTO.getValue() && this.imageSize.x == IMAGE_SIZE.AUTO.getValue()){
				return scale;
			}
			
			//高さ、幅両方制限サイズあり
			if(this.imageSize.y != IMAGE_SIZE.AUTO.getValue() && this.imageSize.x != IMAGE_SIZE.AUTO.getValue()){
				if (width > height) {
					scale = ((float) this.imageSize.x) / width;
				} else {
					scale = ((float) this.imageSize.y) / height;
				}
				
				return scale;
			}
			
			//幅制限サイズあり
			if(this.imageSize.y == IMAGE_SIZE.AUTO.getValue() && this.imageSize.x != IMAGE_SIZE.AUTO.getValue()){
				scale = ((float) this.imageSize.x) / width;
			}else 
			//高さ制限サイズあり
			if(this.imageSize.y != IMAGE_SIZE.AUTO.getValue() && this.imageSize.x == IMAGE_SIZE.AUTO.getValue()){
				scale = ((float) this.imageSize.y) / height;
			}
			
			return scale;
		}
		
}
