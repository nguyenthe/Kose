package com.jillstuart.beauty.app.data.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.JillApiConstant;
import com.jillstuart.beauty.app.common.helper.CheckHelper;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;

public class UserPointInfo extends UserInfo {
	private Integer point;
	private Integer pointChange;
	private String pointClassName;

	private List<String> cardNumbers;
	private List<String> ecNumbers;

	@SuppressWarnings({ "serial" })
	public void convertJsonToObject(final JSONObject json) {
		super.convertJsonToObject(json);

		if (isResultSuccess() == false) {
			return;
		}

		try {
			
			if (json.has(JillApiConstant.API_KEY_OUT_POINT)) {
				this.point = ConvertHelper.toInteger(json
						.getString(JillApiConstant.API_KEY_OUT_POINT));
			} else {
				this.point = 0;
			}

			this.pointClassName = null;
			if (json.has(JillApiConstant.API_KEY_OUT_POINT_CLASS_NAME)) {
				String className = json
						.getString(JillApiConstant.API_KEY_OUT_POINT_CLASS_NAME);
				if (CheckHelper.isJsonStringNull(className) == false) {
					this.pointClassName = className;
				}
			}
			
			this.ecNumbers = null;
			if (json.has(JillApiConstant.API_KEY_OUT_EC_CODE)) {

				final String value = json
						.getString(JillApiConstant.API_KEY_OUT_EC_CODE);
				if (CheckHelper.isJsonStringNull(value) == false) {
					this.ecNumbers = new ArrayList<String>() {
						{
							add(value);
						}
					};
				}
			}

			this.cardNumbers = null;
			if (json.has(JillApiConstant.API_KEY_OUT_CARD_CODES)) {
				JSONArray jArray = json
						.getJSONArray(JillApiConstant.API_KEY_OUT_CARD_CODES);
				List<String> list = ConvertHelper.JsonArrayToList(jArray);
				if (CheckHelper.isJsonListNull(list) == false) {
					this.cardNumbers = list;
				}
			}

			//for test
//			List<String> cards = new ArrayList<String>();
//			for (int i = 0; i < 9; i++) {
//				cards.add("041400002355");
//			}
//			this.cardNumbers = cards;
//			
//			List<String> ecs = new ArrayList<String>();
//			ecs.add("890909008080");
//			this.ecNumbers = ecs;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Integer getPoint() {
		return point;
	}

	public String getPointString() {
		return ConvertHelper.toString(point);
	}

	public Integer getPointChange() {
		return pointChange;
	}

	public String getPointChangeString() {
		return ConvertHelper.toString(pointChange);
	}

	public void setPointChange(Integer pointChange) {
		this.pointChange = pointChange;
	}

	public String getPointClassName() {
		return pointClassName;
	}

	public boolean isHasEcNumber() {
		return CheckHelper.isHasContent(this.ecNumbers);
	}

	public boolean isHasCardNumber() {
		return CheckHelper.isHasContent(this.cardNumbers);
	}

	public boolean isOverCardNumber() {
		if (this.cardNumbers == null) {
			return false;
		}
		if (this.cardNumbers.size() < Constant.MYPAGE_CARD_SHOW_MAX) {
			return false;
		}

		return true;
	}

	public String getCardNumbersString() {
		return ConvertHelper.ListToString(this.cardNumbers,
				Constant.STRING_ENTER);
	}

	public String getEcNumbersString() {
		return ConvertHelper
				.ListToString(this.ecNumbers, Constant.STRING_ENTER);
	}

}
