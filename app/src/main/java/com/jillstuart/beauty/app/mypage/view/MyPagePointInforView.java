package com.jillstuart.beauty.app.mypage.view;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.helper.ConvertHelper;
import com.jillstuart.beauty.app.common.listener.LeoViewOnClickListener;
import com.jillstuart.beauty.app.data.model.UserPointInfo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyPagePointInforView extends FrameLayout {


	private Button mBtnEc;
	private Button mBtnCard;

	private LinearLayout layout;

	private LeoViewOnClickListener btnEcClickListener;
	private LeoViewOnClickListener btnCardClickListener;

	// data
	private UserPointInfo pointInfo;

	public MyPagePointInforView(Context context) {
		super(context);
		initialize();
	}

	public MyPagePointInforView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(attrs);
	}

	public MyPagePointInforView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize(attrs);
	}

	/**
	 * Initialize with no custom attributes.
	 */
	private void initialize() {
		initialize(null);
	}

	private void initialize(AttributeSet attrs) {

		TypedArray attributes = getContext().obtainStyledAttributes(attrs,
				R.styleable.mypage_question_point_info_view);

		attributes.recycle();

		inflate(getContext(), R.layout.mypage_view_point_info, this);

		layout = (LinearLayout) findViewById(R.id.point_layout);

	}

	private void setPointInfo() {
		
		layout.removeAllViews();
		
		initInfoView();
		initBtn();

	}

	private void initInfoView() {
		if (pointInfo.isHasCardNumber()) {
			initInfoView(Page_Type.MyPage_Login_Card,
					pointInfo.getCardNumbersString());
		}
		
		if (pointInfo.isHasEcNumber()) {
			initInfoView(Page_Type.MyPage_Login_Ec,
					pointInfo.getEcNumbersString());
		}
		
	}

	@SuppressWarnings("deprecation")
	private void initInfoView(Page_Type type, String textString) {
		String titleString = null;

		switch (type) {
		case MyPage_Login_Ec:
			titleString = getContext().getString(
					R.string.LBL_MP_HM_LOGINED_EC_TITLE);
			break;
		case MyPage_Login_Card:
			titleString = getContext().getString(
					R.string.LBL_MP_HM_LOGINED_CARD_TITLE);
			break;
		default:
			break;
		}

		TextView title = new TextView(getContext());
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(0, (int) ConvertHelper.toDIP(getContext(), 20),
				0, (int) ConvertHelper.toDIP(getContext(), 3));
		title.setLayoutParams(params);
		title.setTextColor(getContext().getResources().getColor(
				R.color.sys_font_color_title));
		title.setTypeface(null, Typeface.BOLD);
		title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		title.setText(titleString);
		layout.addView(title);

		TextView text = new TextView(getContext());
		LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//		paramstext.setMargins(0, 0, 0,
//				(int) ConvertHelper.lengthSize(getContext(), 10));
		text.setLayoutParams(paramstext);
		text.setTextColor(getContext().getResources().getColor(
				R.color.sys_font_color_text));
		title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
		text.setText(textString);
		layout.addView(text);
	}

	private void initBtn() {

		if (isShowBtn(Page_Type.MyPage_Login_Card)) {
			addBtn(Page_Type.MyPage_Login_Card);
		}
		if (isShowBtn(Page_Type.MyPage_Login_Ec)) {
			addBtn(Page_Type.MyPage_Login_Ec);
		}

	}

	private boolean isShowBtn(Page_Type type) {

		boolean isShow = false;
		switch (type) {
		case MyPage_Login_Ec:
			isShow = !pointInfo.isHasEcNumber();
			break;
		case MyPage_Login_Card:
			isShow = !pointInfo.isOverCardNumber();

			break;
		default:
			break;
		}

		return isShow;
	}

	@SuppressWarnings("deprecation")
	private void addBtn(Page_Type type) {

		Button btn = new Button(getContext());

		btn.setTextColor(getContext().getResources().getColor(
				R.color.btn_enable_text_color));
		btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
		btn.setTypeface(null, Typeface.BOLD);
		btn.setBackgroundDrawable(getContext().getResources().getDrawable(
				R.drawable.shape_btn_enable));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, (int) getContext().getResources()
						.getDimension(R.dimen.sys_btn_size_height),
				Gravity.CENTER);
		params.setMargins(0, (int) ConvertHelper.toDIP(getContext(), 20),
				0, 0);
		btn.setLayoutParams(params);
		layout.addView(btn);

		String btnTitle = null;
		switch (type) {
		case MyPage_Login_Ec:
			btnTitle = getContext()
					.getString(R.string.LBL_MP_HM_UNLOGIN_BTN_EC);
			mBtnEc = btn;
			mBtnEc.setOnClickListener(this.btnEcClickListener);
			break;
		case MyPage_Login_Card:
			btnTitle = getContext().getString(
					R.string.LBL_MP_HM_UNLOGIN_BTN_CARD);
			mBtnCard = btn;
			mBtnCard.setOnClickListener(this.btnCardClickListener);
			break;
		default:
			break;
		}

		btn.setText(btnTitle);
	}

	public void setBtnEcClickListener(
			LeoViewOnClickListener btnEcClickListener) {
		this.btnEcClickListener = btnEcClickListener;

	}

	public void setBtnCardClickListener(
			LeoViewOnClickListener btnCardClickListener) {
		this.btnCardClickListener = btnCardClickListener;

	}

	public void setPointInfo(UserPointInfo pointInfo) {
		this.pointInfo = pointInfo;
		setPointInfo();
	}

}
