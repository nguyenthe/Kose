package com.jillstuart.beauty.app.common.helper;


import android.app.Activity;
import android.content.Context;

import com.jillstuart.beauty.app.common.Config;
import com.jillstuart.beauty.app.common.Constant;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.JillApiConstant;

public class KoseHelper {

	private static final String URL_TAG = "m=1";
	private static final String URL_MARK_AND = "&";
	private static final String URL_MARK_QUES = "?";


	@SuppressWarnings("unused")
	public static boolean isNeedAddParameterForWebViewLoadUrl(String urlString,
			Activity activity) {

		if (Config.IS_ADD_URL_PARA == false) {
			return false;
		}

		if (CheckHelper.isStringEmpty(urlString)) {
			return false;
		}

		// 会員登録済みのユーザーのみ、m=1を付ける
		if (KoseHelper.userIsLogined(activity.getApplicationContext()) == false) {
			return false;
		}

		boolean isCanAdd = false;
		for (String vaild_url : UrlConstant.URLS_CAN_ADD_PARA) {
			if (urlString.startsWith(vaild_url)) {
				isCanAdd = true;
				break;
			}
		}

		if (isCanAdd == false) {
			return false;
		}

		// もう付けた
		if (urlString.contains(URL_TAG)) {
			return false;
		}

		return true;
	}

	public static String addParameterForWebViewLoadUrl(String urlString) {
		if (CheckHelper.isStringEmpty(urlString)) {
			return Constant.STRING_EMPTY;
		}

		String connectString = null;
		if (urlString.contains(URL_MARK_QUES)) {
			connectString = URL_MARK_AND;
		} else {
			connectString = URL_MARK_QUES;
		}

		return urlString + connectString + URL_TAG;
	}

	/**
	 * ホワイトリスト
	 * 
	 * @param urlString
	 * @return
	 */
	@SuppressWarnings("unused")
	public static boolean isCanAccessUrlAtApp(String urlString) {

		if (Config.IS_WHITE_LIST_VALID == false) {
			return true;
		}

		if (CheckHelper.isStringEmpty(urlString)) {
			return false;
		}
		
		//android　これはいらない
//		//not http.//www https://
//		if (urlString.startsWith(UrlConstant.URLS_CAN_ACCESS_PASS_HTTP) && urlString.startsWith(UrlConstant.URLS_CAN_ACCESS_PASS_WWW) == false) {
//			return true;
//		}
		
		//blank pass
		if (urlString.startsWith(UrlConstant.URLS_CAN_ACCESS_PASS_BLANK)) {
			return true;
		}
		
		for (String vaild_url : UrlConstant.URLS_CAN_ACCESS) {
			if (urlString.startsWith(vaild_url)) {
				return true;
			}
		}

		return false;

	}
	
	public static boolean userIsLogined(Context context) {
		
		return DataSaveHelper.getLocalBoolean(context, JillApiConstant.KEY_MY_PAGE_IS_LOGINED, false);
	}

	public static boolean userIsLoginedEc(Context context) {
		
		return DataSaveHelper.getLocalBoolean(context, JillApiConstant.KEY_MY_PAGE_IS_LOGINED_EC, false);
	}

	public static String getProgramId(Context context) {

		return DataSaveHelper.getLocalString(context, JillApiConstant.KEY_MY_PAGE_PROGRAM_ID, Constant.STRING_EMPTY);
	}

	public static boolean isHasTimeSep(long lastTime,long sepTimeMsc){
		
		if (lastTime == Constant.NO_SET) {
			return true;
		}
		
		long nowTime = System.currentTimeMillis();
		
		if (nowTime - lastTime > sepTimeMsc) {
			return true;
		}
		
		return false;
	}
}
