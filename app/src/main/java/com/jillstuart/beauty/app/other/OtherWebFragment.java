package com.jillstuart.beauty.app.other;

import android.content.Intent;
import android.os.Bundle;

import com.jillstuart.beauty.app.common.EnumConstant.Page_Type;
import com.jillstuart.beauty.app.common.UrlConstant;
import com.jillstuart.beauty.app.common.fragment.BaseWebViewNormalFragment;
import com.jillstuart.beauty.app.common.Constant;


public class OtherWebFragment extends BaseWebViewNormalFragment {
	
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		
		Intent intent = mActivity.getIntent();
		if (intent != null) {
			int type = intent.getIntExtra(Constant.KEY_FG_TYPE, Constant.NO_SET);
			if (type == Page_Type.Other_Jill.getValue()) {
				loadUrl(UrlConstant.URL_OTHER_JILL);
			}else if(type == Page_Type.Other_Facebook.getValue()){
				loadUrl(UrlConstant.URL_OTHER_FACEBOOK);
			}else if(type == Page_Type.Other_Instagram.getValue()){
				loadUrl(UrlConstant.URL_OTHER_INSTAGRAM);
			}
			
		}
	}
}
