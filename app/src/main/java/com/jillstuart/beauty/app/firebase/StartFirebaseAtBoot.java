package com.jillstuart.beauty.app.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by HongNgoc on 4/9/2017.
 */

public class StartFirebaseAtBoot extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("Hello StartFirebase ");
        context.startService(new Intent(FirebaseMessagingService.class.getName()));
    }
}
