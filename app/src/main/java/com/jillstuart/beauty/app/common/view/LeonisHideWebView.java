package com.jillstuart.beauty.app.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class LeonisHideWebView extends WebView {

	public LeonisHideWebView(Context context) {
		super(context);
	}

	public LeonisHideWebView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	public LeonisHideWebView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
	}

	private OnScrollChangedCallback mOnScrollChangedCallback;

	@Override
	protected void onScrollChanged(final int l, final int t, final int oldl,
			final int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);

		if (this.mOnScrollChangedCallback != null) {

			this.mOnScrollChangedCallback.onScroll(l, t,oldl,oldt);
		}

	}

	public OnScrollChangedCallback getOnScrollChangedCallback() {
		return this.mOnScrollChangedCallback;
	}

	public void setOnScrollChangedCallback(
			final OnScrollChangedCallback onScrollChangedCallback) {
		this.mOnScrollChangedCallback = onScrollChangedCallback;
	}

	/**
	 * Impliment in the activity/fragment/view that you want to listen to the
	 * webview
	 */
	public static interface OnScrollChangedCallback {
		public void onScroll(final int l, final int t, final int oldl,
							 final int oldt);
	}
}
