package com.jillstuart.beauty.app.common.fragment;

import co.leonisand.offers.OffersKit.OffersKitStatusCode;
import co.leonisand.offers.OffersStatus;
import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.jillstuart.beauty.app.R;
import com.jillstuart.beauty.app.activity.MainActivity;
import com.jillstuart.beauty.app.application.MainApplication;
import com.jillstuart.beauty.app.common.activity.BaseActionBarActivity;
import com.jillstuart.beauty.app.common.helper.SdkOfferHelper;

@SuppressLint("SetJavaScriptEnabled")
public abstract class BaseFragment extends Fragment {
	protected BaseActionBarActivity mActivity;
	protected FragmentManager mFragmentManager;

	public static boolean isLoadData = true;

	public BaseFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);

		mActivity = (BaseActionBarActivity) getActivity();
		mFragmentManager = getFragmentManager();
		
		detectException();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuinflater) {
		super.onCreateOptionsMenu(menu, menuinflater);
		menu.clear();
	}

	@Override
	public void onPause() {
		super.onPause();
		dismissOverlayProgress();
	}

	public boolean onBackKeyEvent() {
		return false;
	}

	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		if (FragmentTransaction.TRANSIT_FRAGMENT_OPEN == transit) {
			if (enter) {
				return AnimationUtils.loadAnimation(mActivity,
						R.anim.slide_in_right);
			} else {
				// return AnimationUtils.loadAnimation(mActivity,
				// R.anim.slide_out_right);
			}
		} else if (FragmentTransaction.TRANSIT_FRAGMENT_CLOSE == transit) {
			if (enter) {
				// return AnimationUtils.loadAnimation(mActivity,
				// R.anim.slide_in_right);
			} else {
				return AnimationUtils.loadAnimation(mActivity,
						R.anim.slide_out_right);
			}
		}
		return super.onCreateAnimation(transit, enter, nextAnim);
	}

	/**
	 * fragment開く
	 * 
	 * @param fragment
	 */
	protected void openFragment(Fragment fragment) {
		mFragmentManager.beginTransaction().add(R.id.container, fragment, null)
				.addToBackStack(null).commitAllowingStateLoss();
	}
	/**
	 * fragment戻る
	 * 
	 */
	protected void backFragment() {
		mFragmentManager.popBackStack();
	}
	
	/**
	 * タブバー開く
	 * @param tag　タブバータグ
	 */
	protected void openTabByTag(String tag){
		((MainActivity)mActivity).openTabByTag(tag);
	}
	
	protected void hideActionBar() {
		mActivity.hideActionBar();
	}

	protected void showActionBar() {
		mActivity.showActionBar();
	}

	protected void setActionBarTitle(String title) {
		mActivity.setActionBarTitle(title);
	}

	protected boolean isConnectedOrConnecting() {
		return mActivity.isConnectedOrConnecting();
	}

	
	protected void showOverlayProgress() {
		mActivity.showOverlayProgress();
	}

	protected void dismissOverlayProgress() {
		mActivity.dismissOverlayProgress();
	}

	protected void processMsg(OffersStatus offersstatus, int offerType) {

		if (offersstatus == null) {
			mActivity.showErrorNoNetworkConnection();
			return;
		}

		String msg = offersstatus.getCodeMessage();
		// no data code
		if (offersstatus.isEqualStatusCode(OffersKitStatusCode.OffersKitStatusNotFound)) {
			if (SdkOfferHelper.Offer_Type.OfferCoupon.getValue() == offerType) {
				msg = getString(R.string.MSG_ERR_NO_DATA_COUPON);
			} else if (SdkOfferHelper.Offer_Type.OfferRecom.getValue()  == offerType) {
				msg = getString(R.string.MSG_ERR_NO_DATA_RECOM);
			}
			
			return;
		}
		mActivity.showErrorMessage(null, msg);
	}

	protected void processMsg(OffersStatus offersstatus) {
		if (offersstatus != null) {
			mActivity.showErrorMessage(null, offersstatus.getCodeMessage());
		} else {
			mActivity.showErrorNoNetworkConnection();
		}
	}

	protected final boolean isSuccess(OffersStatus offersstatus) {
		if (offersstatus != null && offersstatus.getCode() == 0) {
			return true;
		}

		return false;
	}
	
	@SuppressWarnings("deprecation")
	 protected void setWebViewSetting(WebView mWebView) {
		mWebView.setInitialScale(1); // デフォルト拡大率が変なことがある

		WebSettings mWebSettings = mWebView.getSettings();
		mWebSettings = mWebView.getSettings();
		mWebSettings.setJavaScriptEnabled(true);
//		mWebSettings.setDomStorageEnabled(true);
		mWebSettings.setAppCacheEnabled(true);
		mWebSettings.setAppCacheMaxSize(32 * 1024 * 1024);
		mWebSettings.setBuiltInZoomControls(true);// //+-表示
		
		mWebSettings.setUseWideViewPort(true);// html設定されたviewportを有効にする
		mWebSettings.setLoadWithOverviewMode(true);
		
		//画像最初表示しない。画面が表示した後にダウンロード
		if (Build.VERSION.SDK_INT >= 19) {
			mWebSettings.setLoadsImagesAutomatically(true);
		} else {
			mWebSettings.setLoadsImagesAutomatically(false);
		}

		mWebView.setWebChromeClient(new WebChromeClient()); // html5など有効
		
//		mWebView.setVerticalScrollBarEnabled(false);
		mWebView.setHorizontalScrollBarEnabled(false);
	}
	
	// *********** exception ************
		protected void detectException() {

			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread thread, Throwable e) {
					((MainApplication)mActivity.getApplication())
							.handleUncaughtException(thread, e);
				}
			});
		}
}